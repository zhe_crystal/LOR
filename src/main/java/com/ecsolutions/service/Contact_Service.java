package com.ecsolutions.service;

import com.ecsolutions.entity.Contact_Entity;
import com.ecsolutions.entity.Pledge_Entity;

import java.util.List;

/**
 * Created by Administrator on 2017/7/24.
 */
public interface Contact_Service {

    public List<Contact_Entity> getContactInfo(String custcode,String bpm_no);

    public Integer getContactId(String custcode,String bpm_no);

    public void saveContactInfo(Contact_Entity contact_entity);

    public void updateContactInfo(Contact_Entity contact_entity);

    public void deleteContactInfo(Integer id);


}

