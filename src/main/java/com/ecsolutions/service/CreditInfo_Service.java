package com.ecsolutions.service;

import com.ecsolutions.entity.CreditInfo_Entity;

/**
 * Created by Administrator on 2017/7/28.
 */
public interface CreditInfo_Service {
    CreditInfo_Entity getPopupInfo(String custcode);
    void saveCreditInfoEntity(String custcode, CreditInfo_Entity creditInfo_entity);
    void updateCreditInfoEntity(CreditInfo_Entity updataCreditInfo_entity);
    void deleteCreditInfoEntity(String deleteId);
}
