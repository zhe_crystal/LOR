package com.ecsolutions.Validators;

import com.ecsolutions.entity.Contact_Entity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Administrator on 2017/7/26.
 * 抵押信息校验
 */
@Component
public class ContactValidate implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Contact_Entity.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Contact_Entity contact_entity = (Contact_Entity) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "clientname", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contactreason", "contactreason", "不能为空");
    }
}
