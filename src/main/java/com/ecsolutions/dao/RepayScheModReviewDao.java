package com.ecsolutions.dao;

import com.ecsolutions.dao.SqlProvider.RepayScheModReviewProvider;
import com.ecsolutions.dao.SqlProvider.repaymentScheModify_Provider;
import com.ecsolutions.entity.repaymentScheModify_entity;
import com.ecsolutions.entity.repaymentSchedule_entity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017-09-07.
 */
@Repository
public interface RepayScheModReviewDao {
    @SelectProvider(type = RepayScheModReviewProvider.class, method = "getResultList")
    @ResultType(repaymentScheModify_entity.class)
    List<repaymentScheModify_entity> getResultList(@Param("refno")String refno);

    @Select("SELECT intrate FROM adsinmf4 Where TRIM(appref) = TRIM(#{refno}) and rownum = 1")
    @ResultType(String.class)
    String getIntrate(String refno);

    @Select("SELECT strdate FROM hkbinmf4 Where TRIM(refno) = TRIM(#{arg0}) and instno = #{arg1}")
    @ResultType(String.class)
    String getStrdate(String refno,String curinno);


    @Select("SELECT s.refno as refno,s.custcod as custcod,s.custnam as custnm,s.loandbd as dwndate,s.tenor as instno,l.instno as curinno,p.paymeth as prymth,p.payfreq as rpyfeq,p.payper as rpyper ,s.applccy as lnccy,s.applamt as lnamt ,l.lnosamt as lnosamt,p.repayda as fpaydat FROM hkbppmas s,hkbpptmp p ,hkbindtl l Where  s.refno=p.refno and p.refno=l.refno and l.refno= TRIM(#{refno})")
    @ResultType(repaymentScheModify_entity.class)
    repaymentScheModify_entity getLoanInfo(String refno);

    //通过refno查询出还款计划表的信息
    @Select("select appref as refno,verno,instno,func,schetyp,subtype,custcod,strdate,duedate,intrate,ratetyp,spread,prntier,inttier,prnamt,intamt,instamt,lnosamt,lnosccy,appref from adsinmf4 where TRIM(appref) = TRIM(#{refno})")
    @ResultType(repaymentSchedule_entity.class)
    List<repaymentSchedule_entity> getRepayScheReviewList(@Param("refno") String refno);



}
