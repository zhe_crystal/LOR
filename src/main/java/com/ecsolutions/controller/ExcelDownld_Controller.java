package com.ecsolutions.controller;

import com.ecsolutions.entity.ExcelDownld_Entity;
import com.ecsolutions.service.ExcelDownld_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by Administrator on 2017/7/31.
 */
@Controller
public class ExcelDownld_Controller {
    @Autowired
    private ExcelDownld_Service excelDownld_service;
    public ExcelDownld_Service getExcelDownld_service() {
        return excelDownld_service;
    }

    @GetMapping("/excelDownld")
    public String getApplyPage(Model model) {
        model.addAttribute("excelDownld_entity",new ExcelDownld_Entity());
        return "excelDownld";
    }

    @PostMapping("/excelDownld")
    public String saveGuaranteeInfo( Model model,@ModelAttribute ExcelDownld_Entity excelDownld_entity) {

        excelDownld_service.ExcelDownld(excelDownld_entity);
        model.addAttribute("excelDownld_entity",new ExcelDownld_Entity());
        return "excelDownld";
    }
}
