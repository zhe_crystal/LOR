package com.ecsolutions.service;

import com.ecsolutions.dao.Facility_Dao;
import com.ecsolutions.dao.Route_DAO;
import com.ecsolutions.entity.Route_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/7/24.
 */
@Service("Route_Service")
public class Route_ServiceImpl implements Route_Service {
    private Route_DAO route_dao;

    @Autowired
    public Route_ServiceImpl(Route_DAO route_dao) {
        this.route_dao = route_dao;
    }

    @Override
    public List<Route_Entity> getByloanid(String loanid) {
        return route_dao.getByloanid(loanid);
    }
}

