package com.ecsolutions.service;

import com.ecsolutions.entity.finance_body_entity;
import com.ecsolutions.entity.finance_header_entity;

import java.util.List;

/**
 * Created by ecs on 2017/8/18.
 */
public interface finance_Service {
    finance_header_entity getPopupInfo();
    void saveFinanceHeaderEntity(finance_header_entity finance_header_entity);
    void saveFinanceBodyEntity(finance_body_entity finance_body_entity);
    String insertCheck(String custcode);
    List<finance_header_entity> financeHeaderInfoSearch(String custcode);
    List<String> getReportno(String custcode);
    List<finance_body_entity> financeBodyInfoSearch(String reportno);
}
