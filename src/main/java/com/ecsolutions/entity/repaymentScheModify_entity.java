package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ecs on 2017/9/4.
 */
public class repaymentScheModify_entity {
    private String userid;
    private String refno;
    private String status = "待修改";
    private String custcod;
    private String fllcycd;
    private String flrcsts;
    private String cusadd1;
    private String cusadd2;
    private String cusadd3;
    private String cusnm;
    private String dwndate;
    private String intrate;
    private String instno;
    private String curinno;
    private String prymth;
    private String paymth;
    private String rpyfeq;
    private String rpypey;
    private String matdate;
    private String rpyper;
    private String lnccy;
    private String schetyp;
    private String subtype;
    private String tb_subtype;
    private String rpymth;
    private String bankent;
    private String branch;
    private String idno;
    private BigDecimal lnamt;
    private BigDecimal lnosamt;
    private String strdate;
    private String duedate;
    private String fpaydat;
    private BigDecimal hanchrg;
    private String appref;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPaymth() {
        return paymth;
    }

    public void setPaymth(String paymth) {
        this.paymth = paymth;
    }

    public String getRpypey() {
        return rpypey;
    }

    public void setRpypey(String rpypey) {
        this.rpypey = rpypey;
    }

    public String getMatdate() {
        return matdate;
    }

    public void setMatdate(String matdate) {
        this.matdate = matdate;
    }

    public String getFllcycd() {
        return fllcycd;
    }

    public void setFllcycd(String fllcycd) {
        this.fllcycd = fllcycd;
    }

    public String getFlrcsts() {
        return flrcsts;
    }

    public void setFlrcsts(String flrcsts) {
        this.flrcsts = flrcsts;
    }

    public String getCusadd1() {
        return cusadd1;
    }

    public void setCusadd1(String cusadd1) {
        this.cusadd1 = cusadd1;
    }

    public String getCusadd2() {
        return cusadd2;
    }

    public void setCusadd2(String cusadd2) {
        this.cusadd2 = cusadd2;
    }

    public String getCusadd3() {
        return cusadd3;
    }

    public void setCusadd3(String cusadd3) {
        this.cusadd3 = cusadd3;
    }

    public String getSchetyp() {
        return schetyp;
    }

    public void setSchetyp(String schetyp) {
        this.schetyp = schetyp;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
        this.tb_subtype = subtype;
    }

    public String getTb_subtype() {
        return tb_subtype;
    }

    public void setTb_subtype(String tb_subtype) {
        this.tb_subtype = tb_subtype;
        this.subtype = tb_subtype;
    }

    public String getRpymth() {
        return rpymth;
    }

    public void setRpymth(String rpymth) {
        this.rpymth = rpymth;
    }

    public String getBankent() {
        return bankent;
    }

    public void setBankent(String bankent) {
        this.bankent = bankent;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustcod() {
        return custcod;
    }

    public void setCustcod(String custcod) {
        this.custcod = custcod;
    }

    public String getCusnm() {
        return cusnm;
    }

    public void setCusnm(String cusnm) {
        this.cusnm = cusnm;
    }

    public String getDwndate() {
        return dwndate;
    }

    public void setDwndate(String dwndate) {
        this.dwndate = dwndate;
    }

    public String getIntrate() {
        return intrate;
    }

    public void setIntrate(String intrate) {
        this.intrate = intrate;
    }

    public String getInstno() {
        return instno;
    }

    public void setInstno(String instno) {
        this.instno = instno;
    }

    public String getCurinno() {
        return curinno;
    }

    public void setCurinno(String curinno) {
        this.curinno = curinno;
    }

    public String getPrymth() {
        return prymth;
    }

    public void setPrymth(String prymth) {
        this.prymth = prymth;
    }

    public String getRpyfeq() {
        return rpyfeq;
    }

    public void setRpyfeq(String rpyfeq) {
        this.rpyfeq = rpyfeq;
    }

    public String getRpyper() {
        return rpyper;
    }

    public void setRpyper(String rpyper) {
        this.rpyper = rpyper;
    }

    public String getLnccy() {
        return lnccy;
    }

    public void setLnccy(String lnccy) {
        this.lnccy = lnccy;
    }

    public BigDecimal getLnamt() {
        return lnamt;
    }

    public void setLnamt(BigDecimal lnamt) {
        this.lnamt = lnamt;
    }

    public BigDecimal getLnosamt() {
        return lnosamt;
    }

    public void setLnosamt(BigDecimal lnosamt) {
        this.lnosamt = lnosamt;
    }

    public String getStrdate() {
        return strdate;
    }

    public void setStrdate(String strdate) {
        this.strdate = strdate;
    }

    public String getFpaydat() {
        return fpaydat;
    }

    public void setFpaydat(String fpaydat) {
        this.fpaydat = fpaydat;
    }

    public BigDecimal getHanchrg() {
        return hanchrg;
    }

    public void setHanchrg(BigDecimal hanchrg) {
        this.hanchrg = hanchrg;
    }

    public String getAppref() {
        return appref;
    }

    public void setAppref(String appref) {
        this.appref = appref;
    }
}
