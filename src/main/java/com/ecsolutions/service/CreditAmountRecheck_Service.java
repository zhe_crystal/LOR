package com.ecsolutions.service;

import com.ecsolutions.entity.CreditAmountRecheck_Entity;

import java.util.List;

public interface CreditAmountRecheck_Service {
    List<CreditAmountRecheck_Entity> getSearchInfoResultList(String obkgid, String refnum, String startDate, String endDate);

    Integer countSearchInfoResultList(String obkgid, String refnum, String startDate, String endDate);

    String getFirstApproverName(String userId);

    String getCustnm(String refnum);

    boolean saveRecord(CreditAmountRecheck_Entity entity);
}
