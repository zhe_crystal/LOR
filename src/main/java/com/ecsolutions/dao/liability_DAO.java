package com.ecsolutions.dao;

import com.ecsolutions.entity.liability_entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/7/28.
 */
@Repository
public interface liability_DAO{

    // select by custcode
    @Select("SELECT * FROM loanApplicantLiabilitiesinfo WHERE TRIM(custcode) = TRIM(#{custcode})")
    @ResultType(liability_entity.class)
    List<liability_entity> getLiabilityInfo(String custcode);

    //check insert
    @Select("SELECT count(*) FROM loanApplicantLiabilitiesinfo WHERE id = #{id}")
    @ResultType(String.class)
    String checkinsert(@Param("id") String id);

    //根据id删除负债信息
    @Delete("delete from loanApplicantLiabilitiesinfo where id=#{id}")
    void delete(String id);

    @Insert("insert into loanApplicantLiabilitiesinfo(custcode,loanamount,balance,creditorname,loandate,duedate,paymentmethod,installmentamount,isoverdue,overdueamount,memo1) "
            +"values(#{custcode,jdbcType=VARCHAR},#{loanamount,jdbcType=VARCHAR},#{balance,jdbcType=VARCHAR},#{creditorname,jdbcType=VARCHAR},#{loandate,jdbcType=TIMESTAMP},"
            +"#{duedate,jdbcType=TIMESTAMP},#{paymentmethod,jdbcType=VARCHAR},#{installmentamount,jdbcType=VARCHAR},#{isoverdue,jdbcType=VARCHAR},"
            +"#{overdueamount,jdbcType=VARCHAR},#{memo1,jdbcType=VARCHAR})")
    void insert(liability_entity liability_entity);

    @Update("update loanApplicantLiabilitiesinfo set custcode=#{custcode,jdbcType=VARCHAR},loanamount=#{loanamount,jdbcType=VARCHAR},balance=#{balance,jdbcType=VARCHAR},"
            +"creditorname=#{creditorname,jdbcType=VARCHAR},loandate=#{loandate,jdbcType=VARCHAR},duedate=#{duedate,jdbcType=VARCHAR},paymentmethod=#{paymentmethod,jdbcType=VARCHAR},"
            +"installmentamount=#{installmentamount,jdbcType=VARCHAR},isoverdue=#{isoverdue,jdbcType=VARCHAR},overdueamount=#{overdueamount,jdbcType=VARCHAR},memo1=#{memo1,jdbcType=VARCHAR} "
            +"where id=#{id}")
    void update(liability_entity liability_entity);
}
