package com.ecsolutions.entity;

/**
 * Created by ecs on 2017/8/29.
 */
public class CompDepApproval_entity {
    private String userid;
    private String loanid;
    private String bankcode;
    private String branch;
    private double drawamt;
    private String drawccy;
    private String ActionUserName;
    private String WorkItemTitle;
    private String AuditContext;
    private String Result;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLoanid() {
        return loanid;
    }

    public void setLoanid(String loanid) {
        this.loanid = loanid;
    }

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public double getDrawamt() {
        return drawamt;
    }

    public void setDrawamt(double drawamt) {
        this.drawamt = drawamt;
    }

    public String getDrawccy() {
        return drawccy;
    }

    public void setDrawccy(String drawccy) {
        this.drawccy = drawccy;
    }

    public String getActionUserName() {
        return ActionUserName;
    }

    public void setActionUserName(String actionUserName) {
        ActionUserName = actionUserName;
    }

    public String getWorkItemTitle() {
        return WorkItemTitle;
    }

    public void setWorkItemTitle(String workItemTitle) {
        WorkItemTitle = workItemTitle;
    }

    public String getAuditContext() {
        return AuditContext;
    }

    public void setAuditContext(String auditContext) {
        AuditContext = auditContext;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }
}
