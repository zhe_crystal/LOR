package com.ecsolutions.dao;

import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017-10-13.
 */
@Repository
public interface ApproveDao {
    @Select("SELECT loanid FROM Loanroutehistory where workitemid = #{TaskId} and rownum = 1")
    String getBpmNoByTaskId(String TaskId);
}
