package com.ecsolutions.service;

import com.ecsolutions.entity.*;

import java.util.List;

/**
 * Created by Administrator on 2017/8/25.
 */
public interface ExaminationService {
    List<Examination> getUsersInfo();
    String getPuid(String loanid);
    void savaExamination(String loanid,String userid,Examination examination);
    String getCustcod(String loanid);
    String getCustomerType(String custcod);
    Acpy_Customer_Entity getPersonal(String custcode);
    Acpy_Enterprise_Entity getEnterprise(String custcode);
}
