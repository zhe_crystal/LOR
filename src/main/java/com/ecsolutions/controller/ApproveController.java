package com.ecsolutions.controller;

import com.ecsolutions.service.ApproveService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017-10-12.
 */
//主界面审批controller
@Controller
public class ApproveController {
    @Autowired
    public ApproveService approveService;

    @GetMapping("/approveMain")
    public String getApprovePage(Model model, @RequestParam("userid") String userid){
        List<String> listStr1 = approveService.getApproveList("1", "11", "1", "true","提交贷款申请");
        List<String> listStr2 = approveService.getApproveList("1", "11", "1", "true","支行审贷会秘书审批");
        List<String> listStr3 = approveService.getApproveList("1", "11", "1", "true","合规");
        model.addAttribute("listStr1",listStr1);
        model.addAttribute("listStr2",listStr2);
        model.addAttribute("listStr3",listStr3);
        model.addAttribute("userid",userid);




        return "Approve/approveMain";
    }
}
