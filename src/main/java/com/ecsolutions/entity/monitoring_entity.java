package com.ecsolutions.entity;

/**
 * Created by ecs on 2017/10/10.
 */
public class monitoring_entity {
    private String bpm_no;
    private String processid;
    private String taskname;
    private String initiatorname;
    private String templatename;
    private String state;
    private String createtime;

    public String getBpm_no() {
        return bpm_no;
    }

    public void setBpm_no(String bpm_no) {
        this.bpm_no = bpm_no;
    }

    public String getProcessid() {
        return processid;
    }

    public void setProcessid(String processid) {
        this.processid = processid;
    }

    public String getTaskname() {
        return taskname;
    }

    public void setTaskname(String taskname) {
        this.taskname = taskname;
    }

    public String getInitiatorname() {
        return initiatorname;
    }

    public void setInitiatorname(String initiatorname) {
        this.initiatorname = initiatorname;
    }

    public String getTemplatename() {
        return templatename;
    }

    public void setTemplatename(String templatename) {
        this.templatename = templatename;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }
}
