package com.ecsolutions.entity;

/**
 * Created by ecs on 2017/8/18.
 */
public class Tblusers_Entity {
    private String userid;
    private String username;
    private String passwrd;
    private String email;
    private String branch;
    private String businesamoun;
    private String considerationamount;
    private String auditamount;
    private String organizationcode;
    private String pauditamt;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswrd() {
        return passwrd;
    }

    public void setPasswrd(String passwrd) {
        this.passwrd = passwrd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBusinesamoun() {
        return businesamoun;
    }

    public void setBusinesamoun(String businesamoun) {
        this.businesamoun = businesamoun;
    }

    public String getConsiderationamount() {
        return considerationamount;
    }

    public void setConsiderationamount(String considerationamount) {
        this.considerationamount = considerationamount;
    }

    public String getAuditamount() {
        return auditamount;
    }

    public void setAuditamount(String auditamount) {
        this.auditamount = auditamount;
    }

    public String getOrganizationcode() {
        return organizationcode;
    }

    public void setOrganizationcode(String organizationcode) {
        this.organizationcode = organizationcode;
    }

    public String getPauditamt() {
        return pauditamt;
    }

    public void setPauditamt(String pauditamt) {
        this.pauditamt = pauditamt;
    }
}
