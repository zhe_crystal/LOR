package com.ecsolutions.entity;

/**
 * Created by ecs on 2017/8/29.
 */
public class finance_body_entity {
    private String reportno;
    private Integer seqno;
    private String typeid;
    private double origvalue;
    private double endvalue;

    public String getReportno() {
        return reportno;
    }

    public void setReportno(String reportno) {
        this.reportno = reportno;
    }

    public Integer getSeqno() {
        return seqno;
    }

    public void setSeqno(Integer seqno) {
        this.seqno = seqno;
    }

    public String getTypeid() {
        return typeid;
    }

    public void setTypeid(String typeid) {
        this.typeid = typeid;
    }

    public double getOrigvalue() {
        return origvalue;
    }

    public void setOrigvalue(double origvalue) {
        this.origvalue = origvalue;
    }

    public double getEndvalue() {
        return endvalue;
    }

    public void setEndvalue(double endvalue) {
        this.endvalue = endvalue;
    }
}
