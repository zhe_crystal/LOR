package com.ecsolutions.service;

import com.ecsolutions.dao.repaymentInput_DAO;
import com.ecsolutions.entity.repaymentInput_entity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/8/30.
 */
@Service("repaymentInput_Service")
public class repaymentInput_ServiceImpl implements repaymentInput_Service{
    private repaymentInput_DAO repaymentinput_dao;

    public repaymentInput_ServiceImpl(repaymentInput_DAO repaymentinput_dao) {
        this.repaymentinput_dao = repaymentinput_dao;
    }

    @Override
    public List<repaymentInput_entity> getResultList(String dwndate, String matdate, String custcod, String refno){
        List<repaymentInput_entity> result = repaymentinput_dao.getResultList(dwndate,matdate,custcod,refno);
        return result;
    }

    @Override
    public void saveRepaymentInfo(repaymentInput_entity repaymentinput_entity){
        if(repaymentinput_entity.isMargflag()){
            repaymentinput_entity.setMargflagString("Y");
        }else {
            repaymentinput_entity.setMargflagString("N");
        }
        repaymentinput_dao.insert(repaymentinput_entity);
    }

    @Override
    public void updateRepaymentInfo(repaymentInput_entity repaymentinput_entity){
        if(repaymentinput_entity.isMargflag()){
            repaymentinput_entity.setMargflagString("Y");
        }else {
            repaymentinput_entity.setMargflagString("N");
        }
        repaymentinput_dao.update(repaymentinput_entity);
    }
    @Override
    public String insertCheck(String refno,String custcod,String instnof,String instnot,String status){
        String result = repaymentinput_dao.checkinsert(refno,custcod,instnof,instnot,status);
        return result;
    }

    @Override
    public int countResultList(String dwndate,String matdate,String custcod,String refno){
        int result = repaymentinput_dao.countResultList(dwndate,matdate,custcod,refno);
        return result;
    }

    @Override
    public repaymentInput_entity getRepaymentInfo(String refno){
        repaymentInput_entity result = repaymentinput_dao.getInfoByRefno(refno);
        return result;
    }

    @Override
    public String QueryBySchetyp(String schetyp){
        String result = repaymentinput_dao.getSchedes(schetyp);
        return result;
    }
    @Override
    public String getPayamt(String refno){
        String result = repaymentinput_dao.countPayment(refno);
        return  result;
    }
}
