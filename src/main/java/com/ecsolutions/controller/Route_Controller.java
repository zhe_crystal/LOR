package com.ecsolutions.controller;

import com.ecsolutions.entity.Contact_Entity;
import com.ecsolutions.entity.Route_Entity;
import com.ecsolutions.service.Route_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/24.
 */
@SuppressWarnings("all")
@Controller
public class Route_Controller {

    public Route_Controller(){}

    @Autowired
    private Route_Service route_service;

    /*进入审核记录页面*/
    @GetMapping("/RouteList")
    public String getRouteList(Model model){
        String loanid = "BP1Y10000128";
        model.addAttribute("loanid",loanid);
        return "Route/route";
    }

    /*通过loanid查询信息到Table表格中*/
    @ResponseBody
    @RequestMapping(value = "/Route_EntityList",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public List<Route_Entity> getRouteListInfo(@RequestBody Map<String, Object> map ){
        String loanid = map.get("loanid").toString();
        List<Route_Entity> route_entityList = route_service.getByloanid(loanid);
        return route_entityList;
    }

}