function Format(value){
    if (value != null) {
        var date = new Date(parseInt(value));
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        return date.getFullYear()  + month  + currentDate;
    }
}

$(function() {

    $('#btn_delete').click(function () {
        if(confirm("确定要删除记录?")) {
            var id = $('#id').val();

            $.ajax({
                type: "POST",
                url: "/deleteAppointmentInfo",
                dataType: "json",
                data: {id: id},
                async: false,
                success: function (result) {
                    $('#AppointmentTable').bootstrapTable("refresh", {
                        silent: true
                    });
                    alert(result.message);
                    $('#btn_add').trigger("click");
                },
                error: function (result) {
                    alert(result.message);
                }
            });
        }
    });

    //当其采取后台分页的时候，在这里把所需要的参数传给后台
    function queryParams(params) { // 配置参数
        var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            // limit : params.limit, // 页面大小
            // offset : params.offset, // 页码
            // sortname : params.sort, // 排序列名
            // sortorder : params.order// 排序规则
            custcode : $("#custcode").val(),
            bpm_no : $("#bpm_no").val()
            // pageWhere2 : $("#searchAcName").val()

        };
        return temp;
    }

    $("#AppointmentTable").bootstrapTable({
        method : "post",// 请求方式（*）
        url: "/AppointmentListInfo",// 请求后台的URL（*）
        queryParams : queryParams,// 传递参数（*)
        uniqueId : "id",// 唯一标识
        sidePagination : 'client',// 分页方式：client客户端分页，server服务端分页（*）
        sortName : "id",// 排序字段
        sortOrder : "desc",// 排序方式
        pageList : [ 5, 10, 15, 20, 25, 30 ],// 可供选择的每页的行数（*）
        pageNumber : 1,// 初始化加载第一页，默认第一页
        pagination : true,// 是否显示分页（*）
        pageSize : 5,// 每页的记录行数（*）
        showColumns : true, // 是否显示所有的列
        toolbar : "#toolbar",// 工具按钮用哪个容器
        clickToSelect : true,// 是否启用点击选中行
        cache : false,// 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped : false,// 是否显示行间隔色
        showRefresh : true,// 是否显示刷新按钮
        showToggle : true,// 是否显示详细视图和列表视图的切换按钮
        cardView : false,// 是否显示详细视图
        detailView : false,// 是否显示父子表
        paginationLoop : false,// 是否允许循环分页
        paginationPreText : "上一页",// 指定上一页按钮文字，不配置默认<
        paginationNextText : "下一页",
        columns : [{
            field: 'appointclient',
            title: '客户姓名'
        }, {
            field: 'appointuser',
            title: '预约人'
        }, {
            field : 'appointdate',
            title : '预约日期',
            formatter:function(value,row,index){
                if (value != null) {
                    var date = new Date(parseInt(value));
                    var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
                    var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                    return date.getFullYear()  + month  + currentDate;
                }
            }
        }, {
            field : 'appointtime',
            title : '预约时间',
            formatter:function(value,row,index){
                if (value != null) {
                    var date = new Date(parseInt(value));
                    var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
                    var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                    return date.getFullYear()  + month  + currentDate;
                }
            }
        }, {
            field : 'appointplace',
            title : '预约地点'
        }, {
            field : 'remarks',
            title : '备注'
        }],

        onClickRow : function(row) {
            //通用信息
            $("#id").val(row.id);
            $("#appointclient").val(row.appointclient);
            $("#appointuser").val(row.appointuser);
            var appointdate = Format(row.appointdate);
            $("#appointdate").val(appointdate);
            var appointtime = Format(row.appointtime);
            $("#appointtime").val(appointtime);
            $("#appointplace").val(row.appointplace);
            $("#remarks").val(row.remarks);
        }
    });

    $("#btn_add").click(function () {

        var custcode = $('#custcode').val();
        $.ajax({
            type: "POST",
            url: "/NewAppointmentInfo",
            data: {custcode : custcode},
            dataType: "json",
            async: false,
            success: function(row){
                //通用信息
                $("#id").val(row.id);
                $("#appointclient").val(row.appointclient);
                $("#appointuser").val(row.appointuser);
                $("#appointdate").val(row.appointdate);
                $("#appointtime").val(row.appointtime);
                $("#appointplace").val(row.appointplace);
                $("#remarks").val(row.remarks);
            }
        });
    });

    $('#appointdate').datepicker({
        format: 'yyyymmdd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN'
    }).on('changeDate',function(ev){

    });

    $('#appointtime').datepicker({
        format: 'yyyymmdd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN'
    }).on('changeDate',function(ev){

    });

});