package com.ecsolutions.entity;

import java.util.List;
/**
 * Created by ecs on 2017/8/4.
 */
public class Print_entity {
    private String bpm_no;
    private String lineno;
    private String loanref;
    private String startdate;
    private String anndate;
    private String dircdesc;
    private String custcod;
    private String lastname;
    private String CommunicationAddress;
    private String Businessscope;
    private String lnofemp;
    private String legalrepresentativename;
    private String drawdate;
    private String drawamt;
    private String osamt;
    private String expirydate;
    private String schemarate;
    private String order;

    public String getBpm_no() {
        return bpm_no;
    }

    public void setBpm_no(String bpm_no) {
        this.bpm_no = bpm_no;
    }

    public String getLineno() {
        return lineno;
    }

    public void setLineno(String lineno) {
        this.lineno = lineno;
    }

    public String getLoanref() {
        return loanref;
    }

    public void setLoanref(String loanref) {
        this.loanref = loanref;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getAnndate() {
        return anndate;
    }

    public void setAnndate(String anndate) {
        this.anndate = anndate;
    }

    public String getDircdesc() {
        return dircdesc;
    }

    public void setDircdesc(String dirdcdesc) {
        this.dircdesc = dirdcdesc;
    }

    public String getCustcod() {
        return custcod;
    }

    public void setCustcod(String custcod) {
        this.custcod = custcod;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCommunicationAddress() {
        return CommunicationAddress;
    }

    public void setCommunicationAddress(String communicationAddress) {
        CommunicationAddress = communicationAddress;
    }

    public String getBusinessscope() {
        return Businessscope;
    }

    public void setBusinessscope(String businessscope) {
        Businessscope = businessscope;
    }

    public String getLnofemp() {
        return lnofemp;
    }

    public void setLnofemp(String lnofemp) {
        this.lnofemp = lnofemp;
    }

    public String getLegalrepresentativename() {
        return legalrepresentativename;
    }

    public void setLegalrepresentativename(String legalrepresentativename) {
        this.legalrepresentativename = legalrepresentativename;
    }

    public String getDrawdate() {
        return drawdate;
    }

    public void setDrawdate(String drawdate) {
        this.drawdate = drawdate;
    }

    public String getDrawamt() {
        return drawamt;
    }

    public void setDrawamt(String drawamt) {
        this.drawamt = drawamt;
    }

    public String getOsamt() {
        return osamt;
    }

    public void setOsamt(String osamt) {
        this.osamt = osamt;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getSchemarate() {
        return schemarate;
    }

    public void setSchemarate(String schemarate) {
        this.schemarate = schemarate;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
