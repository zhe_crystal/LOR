package com.ecsolutions.service;

public interface AmountDeal_Service {
    String dealWithMoneyAmount(String Amount,int digit1,int digit2);
    String dealWithInterestRate(String Rate,int digit1,int digit2);
}
