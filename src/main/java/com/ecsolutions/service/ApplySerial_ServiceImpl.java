package com.ecsolutions.service;

import com.ecsolutions.dao.ApplyInfo_DAO;
import com.ecsolutions.dao.ApplySerial_DAO;
import com.ecsolutions.dao.LoanApplicantFacilityInfo_DAO;
import com.ecsolutions.dao.MicroCreditAuditLog_DAO;
import com.ecsolutions.entity.ApplySerial_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("ApplySerial_Service")
public class ApplySerial_ServiceImpl implements ApplySerial_Service {

    ApplySerial_DAO applySerial_dao;
    ApplyInfo_DAO applyInfo_dao;
    MicroCreditAuditLog_DAO microCreditAuditLog_dao;
    LoanApplicantFacilityInfo_DAO loanApplicantFacilityInfoDAO;

    @Autowired
    public ApplySerial_ServiceImpl(ApplySerial_DAO applySerial_dao, ApplyInfo_DAO applyInfo_dao, MicroCreditAuditLog_DAO microCreditAuditLog_dao,LoanApplicantFacilityInfo_DAO loanApplicantFacilityInfoDAO) {
        this.applyInfo_dao = applyInfo_dao;
        this.applySerial_dao = applySerial_dao;
        this.microCreditAuditLog_dao = microCreditAuditLog_dao;
        this.loanApplicantFacilityInfoDAO=loanApplicantFacilityInfoDAO;
    }

    @Override
    public String getCustomerNum() {
        return applySerial_dao.getCustomerNum();
    }

    @Override
    public boolean insertRecord(ApplySerial_Entity entity) {
        return applySerial_dao.insertRecord(entity);
    }

    @Override
    public boolean updateRecord(ApplySerial_Entity entity) {
        return false;
    }

    @Override
    public ApplySerial_Entity checkIsRecordUncompleted(String custcode) {
        return applySerial_dao.checkIsRecordUncompleted(custcode);
    }

    @Override
    public String getPipelineNo() {
        return applySerial_dao.getPipelineNo();
    }

    @Override
    public boolean startOrUpdateWorkFlow(String userid, String loanid, String stepcode, String result, String taskid) {
        return applyInfo_dao.applyInfoInsert(userid, loanid, stepcode, result, taskid);
    }

    @Override
    public String getTaskId(String BPM_NO, String userid) {
        return applyInfo_dao.getTaskId(BPM_NO);
    }

    @Override
    public boolean insertMicroCreditAuditLogRecord(String WorkItemID, String custcod, String LoanID, String AuditNo, String AuditTime, String AuditUserBankCode, String WorkItemTitle, String AuditContext, String ActionUserName, String Result) {
        return microCreditAuditLog_dao.insertRecord(WorkItemID, custcod, LoanID, AuditNo, AuditTime, AuditUserBankCode, WorkItemTitle, AuditContext, ActionUserName, Result);
    }

    @Override
    public String getCustcodebyBPM_NO(String BPM_NO) {
        return applySerial_dao.getCustcodebyBPM_NO(BPM_NO);
    }

    @Override
    public String getAuditNo() {
        return microCreditAuditLog_dao.getAuditNo();
    }

    @Override
    public boolean updateAppFlag(String CustCode) {
        return loanApplicantFacilityInfoDAO.updateAppFlag(CustCode);
    }

    @Override
    public void updateAppFlagWithBPM_NO(String CustCode, String BPM_NO) {
        System.out.println(CustCode + BPM_NO);
        loanApplicantFacilityInfoDAO.updateAppFlagWithBPM_NO(CustCode, BPM_NO);
    }

    @Override
    public boolean updateStatus(String CustCode, String BPM_NO) {
        return applySerial_dao.updateStatus(CustCode, BPM_NO);
    }

    @Override
    public boolean updateAmountWithBPM_NO(String osamt, String holdamt, String usedamt, String CustCode, String BPM_NO) {
        return loanApplicantFacilityInfoDAO.updateAmountWithBPM_NO(osamt, holdamt, usedamt, CustCode, BPM_NO);
    }

    //获取工作流创建时间
    @Override
    public String getCreatetime(String bpm_no){
        return applySerial_dao.getCreatetimeInfo(bpm_no);
    }

    //保存工作流列表信息
    @Override
    public void saveWorkflowListInfo(String bpm_no,String processid,String taskname,String initiatorname,String templatename,String state,String createtime){
        applyInfo_dao.saveWorkflowList(bpm_no,processid,taskname,initiatorname,templatename,state,createtime);
    }

    //check insert
    @Override
    public String insertCheck(String bpm_no){
        return applyInfo_dao.checkinsert(bpm_no);
    }

    //更新工作流列表信息
    @Override
    public void updateWorkflowListInfo(String bpm_no,String processid,String taskname,String initiatorname,String templatename,String state,String createtime){
        applyInfo_dao.updateWorkflowList(bpm_no,processid,taskname,initiatorname,templatename,state,createtime);
    }
}
