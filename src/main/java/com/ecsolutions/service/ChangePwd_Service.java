package com.ecsolutions.service;

import com.ecsolutions.entity.ChangePwd_Entity;

/**
 * Created by Administrator on 2017/8/11.
 */
public interface ChangePwd_Service {
    String getPassword(String id);
    void changePwd(ChangePwd_Entity changePwd_entity);
}
