package com.ecsolutions.dao;

import com.ecsolutions.entity.CreditInfo_Entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/7/28.
 */
@Repository
public interface CreditInfo_DAO {
    @Select("SELECT CCY FROM TACCYMAS ")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getCCY();

    @Select("SELECT id,custcod,line_no,Rating,OTHERORGNAME,OTHERORGERATING,HaveBadRecord,TotalSecurityCCY,TotalSecurity FROM LoanApplicantCredit WHERE custcod= #{custcode}")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getCreditInfoList(@Param("custcode") String custcode);

    @Insert("insert into LoanApplicantCredit(" +
            "Custcod," +
            "line_no," +
            "Rating," +
            "OtherOrgName," +
            "Otherorgerating," +
            "Havebadrecord," +
            "Totalsecurityccy," +
            "Totalsecurity) " +
            "values(" +
            "#{custcode},"+
            "#{param2.line_no}," +
            "#{param2.Rating}," +
            "#{param2.OtherOrgName}," +
            "#{param2.OtherOrgeRating}," +
            "#{param2.HaveBadRecord}," +
            "#{param2.TotalSecurityCCY}," +
            "#{param2.TotalSecurity}" +
            ")")
    void insert(@Param("custcode") String custcode, CreditInfo_Entity creditInfo_entity);

    @Update("update LoanApplicantCredit " +
            "set "+
            "Rating=#{Rating}," +
            "OtherOrgName=#{OtherOrgName}," +
            "Otherorgerating=#{OtherOrgeRating}," +
            "Havebadrecord=#{HaveBadRecord}," +
            "Totalsecurityccy=#{TotalSecurityCCY}," +
            "Totalsecurity =#{TotalSecurity}" +
            "WHERE" +
            " ID=#{ID}")
    void update(CreditInfo_Entity creditInfo_entity);

    @Delete("delete from LoanApplicantCredit where ID= #{deleteId}")
    void delete(String deleteId);

}
