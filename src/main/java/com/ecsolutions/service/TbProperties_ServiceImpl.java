package com.ecsolutions.service;

import com.ecsolutions.dao.TbProperties_DAO;
import com.ecsolutions.entity.TbProperties_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("TbProperties_Service")
public class TbProperties_ServiceImpl implements TbProperties_Service {
    private TbProperties_DAO tbProperties_dao;
    @Autowired
    public TbProperties_ServiceImpl(TbProperties_DAO tbProperties_dao) {
        this.tbProperties_dao = tbProperties_dao;
    }

    @Override
    public boolean save(TbProperties_Entity entity) {
        return tbProperties_dao.saveNewProperty(entity);
    }

    @Override
    public boolean update(TbProperties_Entity entity) {
        return tbProperties_dao.update(entity);
    }

    @Override
    public List<TbProperties_Entity> findOne(String propertiesid) {
        return tbProperties_dao.findOne(propertiesid);
    }

    @Override
    public List<TbProperties_Entity> findAll() {
        return tbProperties_dao.findAll();
    }

    @Override
    public Integer countOne(String propertiesid) {
        return tbProperties_dao.countOne(propertiesid);
    }

    @Override
    public Integer countAll() {
        return tbProperties_dao.countAll();
    }

    @Override
    public boolean delete(String propertiesid) {
        return tbProperties_dao.delete(propertiesid);
    }
}
