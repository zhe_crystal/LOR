package com.ecsolutions.service;

import com.ecsolutions.entity.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/7/31.
 */
public interface Utilization_Service {
    public Utilization_Entity getPopupInfo();

    public List<Utilization_Entity> getByCustcodInfo(String custcod);

    public List<HashMap<String,String>> getByMenleiInfo(String dircflag1);

    public List<HashMap<String,String>> getByMenleiAndDaleiInfo(String dircflag1, String dircflag2);

    public List<HashMap<String,String>> getByMenleiAndDaleiAndZhongleiInfo(String dircflag1, String dircflag2, String dircflag3);

    public Facility_Entity getFacilityInfoByCustcodAndLineno(String custcod, String lineno);

    public Utilization_Entity getUtilizationinfo(String bpm_no);

    public Facility_Entity getFacilityInfo(String bpm_no);

    public List<HashMap<String,Object>> getByLiLvInfo(String drawccy);

    public String getNianlilvInfo(String drawccy, String ratetype);

    public String getJingLiMingChenInfo(String accoffic);

    public Utilization_Entity getUtilizationByCustcodAndLineno(String custcod, String lineno, String loanref);

    public void saveUtilizationInfo(Utilization_Entity utilization_entity);

    public List<HashMap<String,Object>> getRatetypeList(String drawccy);

    public List<HashMap<String,String>> getDaleiList(String dorcflag1);

    public List<HashMap<String,String>> getZhongleiList(String dorcflag1, String dorcflag2);

    public List<HashMap<String,String>> getXiaoleiList(String dorcflag1, String dorcflag2, String dorcflag3);

    public void updateUtilizationInfo(Utilization_Entity utilization_entity);

    public void deleteUtilizationInfo(String custcod,String lineno,String loanref);

    public ApplicationInfo_Entity getByLoanrefInfo(String loanref);

    public void saveApplicationInfo(ApplicationInfo_Entity applicationInfo_entity);

    public void updateApplicationInfo(ApplicationInfo_Entity applicationInfo_entity);

    public Loanpaymentinfo_Entity getLoanpaymentinfo(String loanref);

    public Loanpaymentinfo_det_Entity getLoanpaymentdetinfo(String loanref);

    public String getFenZhiHang(String custcode);

    public String getLastName(String custcode);

    public HashMap getApplicationInfo(String custcode);

    public Integer getMaxId();

    public List<Loanpaymentinfo_det_Entity> getLoanpaymentinfoList(String custcode,String loanref);

    public void saveLoanpaymentInfo(Loanpaymentinfo_Entity loanpaymentinfo_entity);

    public void updateLoanpaymentInfo(Loanpaymentinfo_Entity loanpaymentinfo_entity);

    public void saveLoanpaymentInfodet(Loanpaymentinfo_det_Entity loanpaymentinfo_det_entity);

    public void updateLoanpaymentInfodet(Loanpaymentinfo_det_Entity loanpaymentinfo_det_entity);

    public void deleteLoanpaymentInfo(Integer id);

    public List<Pay_Entity> getPayList(String custcod,String loanref);

    public Boolean updateFacilityInfo(Integer osamt,Integer avliamt,String linestatus,String custcod,String lineno);

    public Boolean updateFacilityInfoTwo(Integer avliamt,Integer holdamt,String linestatus,String custcod,String lineno);

    public Date getDuedateByUtilizationInfo(Utilization_Entity utilization_entity);

    public String derivarExcel(List<Pay_Entity> payList, Utilization_Entity utilization_entity);

    public void export(HSSFWorkbook wb, HttpServletResponse response, HttpServletRequest request);

}
