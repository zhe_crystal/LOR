package com.ecsolutions.common;

import java.math.BigDecimal;

/**
 * 银行还款计算
 * @author cuiran
 * @version TODO
 */
public class Lixijisuan {

    /**
     * 等额本金还款法【利息少，但前期还的多】
     *
     * @param totalMoeny 贷款总额
     * @param rate       贷款商业利率
     * @param year       贷款年限
     */
    public static void principal(int totalMoney, double rate, int year) {
        /**
         * 每月本金
         */
        int totalMonth = year * 12;
        double monthPri = totalMoney / totalMonth;
        /**
         * 获取月利率
         */
        double monRate = resMonthRate(rate);
        BigDecimal b = new BigDecimal(monRate);
        monRate = b.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
        for (int i = 1; i <= totalMonth; i++) {
            double monthRes = monthPri + (totalMoney - monthPri * (i - 1)) * monRate;
            BigDecimal b1 = new BigDecimal(monthRes);
            monthRes = b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            System.out.println("第" + i + "月，还款为：" + monthRes);
        }
    }

    /**
     *
     * 等额本息还款【利息多】
     * @param totalMoeny 贷款总额
     * @param rate  贷款商业利率
     * @param year  贷款年限
     */
    public static void interest(int totalMoney,double rate,int year){
        /**
         * 获取月利率
         */
        double monRate=resMonthRate(rate);
        /**
         * 月还款本息
         */
        double monInterest=totalMoney*monRate*Math.pow((1+monRate),year*12)/(Math.pow((1+monRate),year*12)-1);
        BigDecimal b = new BigDecimal(monInterest);
        monInterest = b.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
        System.out.println("月供本息和："+monInterest);

    }

    /**
     *
     * 转换为月利率
     * @param rate
     * @return
     */
    public static double resMonthRate(double rate){

        return rate/12;
    }
}
