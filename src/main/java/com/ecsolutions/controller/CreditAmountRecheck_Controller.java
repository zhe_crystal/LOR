package com.ecsolutions.controller;

import com.ecsolutions.entity.CreditAmountRecheck_Entity;
import com.ecsolutions.service.CreditAmountRecheck_Service;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
public class CreditAmountRecheck_Controller {

    @Autowired
    private CreditAmountRecheck_Service creditAmountRecheck_service;

    @GetMapping("/CreditAmountRecheck")
    public String getCreditAmountRecheck() {
        return "ClientInformation/CreditAmountRecheck";
    }

    @PostMapping("/CreditAmountRecheck")
    public String postCreditAmountRecheck() {
        return "ClientInformation/CreditAmountRecheck";
    }

    @RequestMapping(value = "/CreditAmountRecheck/Search", method = RequestMethod.GET)
    @ResponseBody
    public String searchResult(@RequestParam String obkgid, @RequestParam String refnum, @RequestParam String startDate, @RequestParam String endDate, int pageIndex, int pageSize) {
        Gson gson = new Gson();
        int total = creditAmountRecheck_service.countSearchInfoResultList(obkgid, refnum, startDate, endDate);
        PageHelper.startPage(pageIndex, pageSize);
        List<CreditAmountRecheck_Entity> list = creditAmountRecheck_service.getSearchInfoResultList(obkgid, refnum, startDate, endDate);
        String historyList = gson.toJson(list);
        String errorCode = "0";
        System.out.println("{\"rowDatas\":" + historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}");
        return "{\"rowDatas\":" + historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}";
    }
}
