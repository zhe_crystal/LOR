package com.ecsolutions.controller;

import com.ecsolutions.entity.Print_entity;
import com.ecsolutions.service.Print_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Administrator on 2017/6/27.
 */
@Controller
public class Print_Controller {

    @Autowired
    private Print_Service Print_service;
    public Print_Service getPrint_service() {
        return Print_service;
    }

    @GetMapping("/Print/print")
    public String getPrintPage(Model model) {
        model.addAttribute("print_entity", new Print_entity());
        return "/Print/print";
    }

    @PostMapping("/Print/print")
    public String printApplyInfo(@ModelAttribute Print_entity print_entity, Model model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("print_entity", print_entity);
        Print_service.printApplyEntity(print_entity);
        return "/Print/print";
    }
}
