package com.ecsolutions.controller;

import com.ecsolutions.common.ObjectHelp;
import com.ecsolutions.entity.Paymenttemp;
import com.ecsolutions.entity.repaymentInput_entity;
import com.ecsolutions.service.RepaymentReviewService;
import com.ecsolutions.service.repaymentInput_Service;
import com.ecsolutions.soaClient.TransferClient;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Administrator on 2017-09-05.
 */
@Controller
public class repaymentReviewInputController {
    @Autowired
    private RepaymentReviewService repaymentReviewService;

    @Autowired
    private TransferClient client;

    public RepaymentReviewService getRepaymentinput_service() {
        return repaymentReviewService;
    }

    @GetMapping("/repaymentReviewInput")
    public String getRepaymentInputPage(Model model, @RequestParam String refno) {
        List<Paymenttemp> result_list = repaymentReviewService.getResultList("", "", "", refno);
        Paymenttemp paymenttemp = result_list.get(0);
        model.addAttribute("paymenttemp_info", paymenttemp);
        return "repaymentReviewInput";
    }

    @PostMapping("/repaymentReviewInput")
    public String postRepaymentInputPage( @ModelAttribute("paymenttemp_info") Paymenttemp paymenttemp_info, BindingResult result, Model model) {
        repaymentReviewService.saveRepaymentReviewInfo(paymenttemp_info);
        //调用soa C00008接口
        try {
            System.out.println("call TransferClient.transfer");
            String mess = ObjectHelp.InitTransferData("RepaymentTx", paymenttemp_info);

            String recieveMsg = client.transfer(mess);
            System.out.println(recieveMsg);
            if (recieveMsg != null || recieveMsg.length() > 0) {
                JSONObject receiveJson= JSONObject.fromObject(recieveMsg);
                String prspcod = receiveJson.getString("PRSPCOD");

                if (prspcod.equals("0000")) {
                    System.out.println("Soa RepaymentTx success");
                } else {
                    System.out.println("Cannot get RepaymentTx From Soa");
                }

            }

        }catch(Exception e){
            e.printStackTrace();
        }
        //调用soa C00008接口 结束

        model.addAttribute("paymenttemp_info", new Paymenttemp());
        return "repaymentReviewInput";
    }
}
