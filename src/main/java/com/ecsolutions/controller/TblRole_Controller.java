package com.ecsolutions.controller;

import com.ecsolutions.Validators.TblRole_Validate;
import com.ecsolutions.Validators.Tbluser_role_Validate;
import com.ecsolutions.entity.TblRole_Entity;
import com.ecsolutions.service.TblRole_Service;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@Controller
public class TblRole_Controller {
    @Autowired
    private TblRole_Service tblRole_service;

    @InitBinder("tblRole_entity")
    public void initBuilder(WebDataBinder binder) {
        binder.setValidator(new TblRole_Validate());
    }

    public TblRole_Service getTblRole_service() {
        return tblRole_service;
    }

    @GetMapping("/TblRole")
    public String getTblRole(Model model) {
        model.addAttribute("tblRole_entity", new TblRole_Entity());
        return "ClientInformation/TblRole";
    }

    @PostMapping("/TblRole")
    public String postTblRole(Model model, @Valid @ModelAttribute("tblRole_entity") TblRole_Entity tblRole_entity, BindingResult result) {
        model.addAttribute("tblRole_entity", tblRole_entity);
        if (!result.hasErrors())
            if (tblRole_service.isRecord(tblRole_entity.getRoleid())) {
                tblRole_service.updateTblRole(tblRole_entity);
            } else {
                tblRole_service.saveTblRole(tblRole_entity);
            }
        return "ClientInformation/TblRole";
    }

    @ResponseBody
    @RequestMapping(value = "/TblRole/getHistoryList", method = RequestMethod.GET)
    public String getHistoryList(int pageSize, int pageIndex) {
        Gson gson = new Gson();
        int total = tblRole_service.countTblRole();
        PageHelper.startPage(pageIndex, pageSize);
        List<TblRole_Entity> list = tblRole_service.getHistoryList();
        String historyList = gson.toJson(list);
        String errorCode = "0";
        return "{\"rowDatas\":" + historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}";
    }

    @ResponseBody
    @RequestMapping(value = "/TblRole/getAllUser", method = RequestMethod.GET)
    public String getAllUser() {
        Gson gson = new Gson();
        List<HashMap<String, String>> userIdList = tblRole_service.getAllUsers();
        String userIdListInfo = gson.toJson(userIdList);
        return userIdListInfo;
    }

    @ResponseBody
    @RequestMapping(value = "/TblRole/getUserinCurrentRole", method = RequestMethod.GET)
    public String getUserinCurrentRole(@RequestParam String roleid) {
        Gson gson = new Gson();
        List<HashMap<String, String>> userIdList = tblRole_service.getUserinOneRole(roleid);
        String userIdListInfo = gson.toJson(userIdList);
        return userIdListInfo;
    }

    @ResponseBody
    @RequestMapping(value = "/TblRole/deleteTblRole", method = RequestMethod.GET)
    public String deleteTblRole(@RequestParam String roleid) {
        if (tblRole_service.deleteTblRole(roleid))
            return "success";
        else
            return "fail";
    }

    @ResponseBody
    @RequestMapping(value = "/TblRole/getCurrentUserInfo", consumes = "application/json", method = RequestMethod.POST)
    public String getCurrentUserInfo(@RequestBody List<String> htmlList, @RequestParam String roleid) {
        List<HashMap<String, String>> SqlList = tblRole_service.getUserinOneRole(roleid);
        //去除相同元素
        for (int i = 0; i < SqlList.size(); ) {
            boolean Istate = false;
            for (int j = 0; j < htmlList.size(); ) {
                if (SqlList.get(i).get("USERID").toString().trim().equals(htmlList.get(j).toString().trim())) {
                    Istate = true;
                    htmlList.remove(j);
                } else {
                    j++;
                }
            }
            if (Istate) {
                SqlList.remove(i);
            } else {
                i++;
            }
        }
        //SqlList该删的删 htmlList该增的增
        for (HashMap<String, String> each : SqlList) {
            tblRole_service.deleteTblUser_Role(each.get("USERID"), roleid);
        }
        for (String item : htmlList) {
            tblRole_service.saveTblUserRole(item, roleid);
        }

        return "success";
    }
}
