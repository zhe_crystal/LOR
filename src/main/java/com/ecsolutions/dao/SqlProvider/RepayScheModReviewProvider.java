package com.ecsolutions.dao.SqlProvider;

import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * Created by Administrator on 2017-09-07.
 */
public class RepayScheModReviewProvider {
    public String getResultList(Map<String, Object> map) {
        String refno = (String) map.get("param1");
        return new SQL() {{
            SELECT("refno,custnam as custnm,tenor as instno,apprlmt as lnamt ");
            FROM("hkbppmas ");
            if (!refno.equals("") && refno != null) {
                WHERE("TRIM(hkbppmas.refno)=#{param1}");
            }
            ORDER_BY("refno ");
        }}.toString();
    }
}
