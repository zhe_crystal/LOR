package com.ecsolutions.service;

import com.ecsolutions.entity.Loan;

/**
 * Created by Administrator on 2017/8/22.
 */
public interface LoanService {
    Loan getPopupInfo(String userid,String bpm_no);
    void saveLoan(Loan loan);
    void paymentSchedule(Loan loanAfter,Loan loanBefore);
    Loan caulMatdate(Loan calLoan);
    Loan getLoan(String bpm_no);
    void saveLoanReview(String bpm_no,String uesrid);
}
