$(function() {

    var schetype = $('#schetype').html();
    if(schetype=='WN '){
        $('#schetype').html("回租设备融资");
    }else if(schetype=='WY1'){
        $('#schetype').html("TEST");
    }else if(schetype=='X09'){
        $('#schetype').html("旅游贷款");
    }else if(schetype=='X08'){
        $('#schetype').html("大件耐用消费品贷款");
    }else if(schetype=='X07'){
        $('#schetype').html("助学贷款");
    }else if(schetype=='J03'){
        $('#schetype').html("商户担保贷款");
    }else if(schetype=='X02'){
        $('#schetype').html("个人汽车消费贷款");
    }else if(schetype=='WD '){
        $('#schetype').html("直租设备融资");
    }else if(schetype=='XD '){
        $('#schetype').html("动产融资");
    }else if(schetype=='WP '){
        $('#schetype').html("微小企业贷款批量业务");
    }

    var collflag = $('#collflag').html();
    if(collflag=='1'){
        $('#collflag').html("信用");
    }else if(collflag=='2'){
        $('#collflag').html("保证");
    }else if(collflag=='3'){
        $('#collflag').html("抵押");
    }else if(collflag=='4'){
        $('#collflag').html("质押");
    }else if(collflag=='5'){
        $('#collflag').html("抵押+质押");
    }else if(collflag=='6'){
        $('#collflag').html("抵押+保证");
    }else if(collflag=='7'){
        $('#collflag').html("质押+保证");
    }else if(collflag=='8'){
        $('#collflag').html("抵押+质押+保证");
    }else if(collflag=='9'){
        $('#collflag').html("全额保证金");
    }

    var paymentmethod = $('#paymentmethod').html();
    if(paymentmethod=='1'){
        $('#paymentmethod').html("等额本息");
    }else if(paymentmethod=='2'){
        $('#paymentmethod').html("等额本金");
    }else if(paymentmethod=='3'){
        $('#paymentmethod').html("一次还本分次付息");
    }else if(paymentmethod=='4'){
        $('#paymentmethod').html("一次性还本付息");
    }

});
