package com.ecsolutions.entity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/7/31.
 */
public class ApplyInfo_Entity {
    private String loanid;
    private String actiondate;
    private String actionuser;
    private String stepcode;
    private String workitemid;

    public List<HashMap<String, String>> getWorkflow() {
        return Workflow;
    }

    public void setWorkflow(List<HashMap<String, String>> workflow) {
        Workflow = workflow;
    }

    public List<HashMap<String,String>> Workflow;

    public String getLoanid() {
        return loanid;
    }

    public void setLoanid(String loanid) {
        this.loanid = loanid;
    }

    public String getActiondate() {
        return actiondate;
    }

    public void setActiondate(String actiondate) {
        this.actiondate = actiondate;
    }

    public String getActionuser() {
        return actionuser;
    }

    public void setActionuser(String actionuser) {
        this.actionuser = actionuser;
    }

    public String getStepcode() {
        return stepcode;
    }

    public void setStepcode(String stepcode) {
        this.stepcode = stepcode;
    }

    public String getWorkitemid() {
        return workitemid;
    }

    public void setWorkitemid(String workitemid) {
        this.workitemid = workitemid;
    }


}
