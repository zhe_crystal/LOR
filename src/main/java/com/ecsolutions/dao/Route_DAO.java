package com.ecsolutions.dao;

import com.ecsolutions.entity.Route_Entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/7/24.
 */
@Repository
public interface Route_DAO {

    // loanid查询出的信息
    @Select("select a.puid,a.stepcode,b.audittime,b.workitemtitle,b.audituserbankcode,b.actionusername,b.result from loanroutehistory a,microcreditauditlog b where TRIM(a.loanid)=TRIM(#{loanid}) and TRIM(a.workitemid) = TRIM(b.workitemid) and TRIM(a.puid) = TRIM(b.auditno) order by a.puid asc")
    @ResultType(Route_Entity.class)
    List<Route_Entity> getByloanid(@Param("loanid") String loanid);
}