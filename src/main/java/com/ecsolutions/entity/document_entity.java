package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * Created by ecs on 2017/7/28.
 */
public class document_entity {
    private String custcode = "188688000007";
    private String line_no;
    private String lineDesc = "小额";
    private String docNo;
    private Integer doctype;
    private String descripe;
    private String docName;
    private String imageTag;
    private String physicalAddr;
    private String recieveInd;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date recivedate;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date effectivedate;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date expirydate;
    private Integer requesttype;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date approvaldate;
    private String docTypeId;
    private Integer loanId;
    private String applicantId;
    private String docDirName;
    private String fileName;
    private Integer isScanUploadId;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date receiveddate;

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public Integer getDoctype() {
        return doctype;
    }

    public void setDoctype(Integer doctype) {
        this.doctype = doctype;
    }

    public String getDescripe() {
        return descripe;
    }

    public void setDescripe(String descripe) {
        this.descripe = descripe;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public String getImageTag() {
        return imageTag;
    }

    public void setImageTag(String imageTag) {
        this.imageTag = imageTag;
    }

    public String getPhysicalAddr() {
        return physicalAddr;
    }

    public void setPhysicalAddr(String physicalAddr) {
        this.physicalAddr = physicalAddr;
    }

    public String getRecieveInd() {
        return recieveInd;
    }

    public void setRecieveInd(String recieveInd) {
        this.recieveInd = recieveInd;
    }

    public Date getRecivedate() {
        return recivedate;
    }

    public void setRecivedate(Date recivedate) {
        this.recivedate = recivedate;
    }

    public Date getEffectivedate() {
        return effectivedate;
    }

    public void setEffectivedate(Date effectivedate) {
        this.effectivedate = effectivedate;
    }

    public Date getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(Date expirydate) {
        this.expirydate = expirydate;
    }

    public Integer getRequesttype() {
        return requesttype;
    }

    public void setRequesttype(Integer requesttype) {
        this.requesttype = requesttype;
    }

    public Date getApprovaldate() {
        return approvaldate;
    }

    public void setApprovaldate(Date approvaldate) {
        this.approvaldate = approvaldate;
    }

    public Date getReceiveddate() {
        return receiveddate;
    }

    public void setReceiveddate(Date receiveddate) {
        this.receiveddate = receiveddate;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public String getLine_no() {
        return line_no;
    }

    public void setLine_no(String line_no) {
        this.line_no = line_no;
    }

    public String getLineDesc() {
        return lineDesc;
    }

    public void setLineDesc(String lineDesc) {
        this.lineDesc = lineDesc;
    }

    public String getDocTypeId() {
        return docTypeId;
    }

    public void setDocTypeId(String docTypeId) {
        this.docTypeId = docTypeId;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public String getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(String applicantId) {
        this.applicantId = applicantId;
    }

    public String getDocDirName() {
        return docDirName;
    }

    public void setDocDirName(String docDirName) {
        this.docDirName = docDirName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getIsScanUploadId() {
        return isScanUploadId;
    }

    public void setIsScanUploadId(Integer isScanUploadId) {
        this.isScanUploadId = isScanUploadId;
    }

}
