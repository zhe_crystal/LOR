package com.ecsolutions.controller;

import com.ecsolutions.common.ObjectHelp;
import com.ecsolutions.entity.Loan;
import com.ecsolutions.service.ApplyInfo_Service;
import com.ecsolutions.service.LoanService;
import com.ecsolutions.soaClient.TransferClient;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by Administrator on 2017/8/21.
 */
@Controller
public class LoanController {

    @Autowired
    private TransferClient client;
    @Autowired
    private LoanService loanService;
    @Autowired
    private ApplyInfo_Service applyInfo_service;

    public LoanService getLoanService() {
        return loanService;
    }

    public ApplyInfo_Service getApplyInfo_Service() {
        return applyInfo_service;
    }



    @GetMapping("/loanMain")
    public String getLoanMainPage(@RequestParam("userid") String userid,@RequestParam("bpm_no") String bpm_no,Model model) {
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loanMain";
    }
    @GetMapping("/loanReviewMain")
    public String getLoanReviewMainPage(@RequestParam("userid") String userid,@RequestParam("bpm_no") String bpm_no,Model model) {
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loanReviewMain";
    }

    @GetMapping("/loanCondition")
    public String getLoanConditionPage(@RequestParam("userid") String userid,@RequestParam("bpm_no") String bpm_no,Model model) {
        model.addAttribute("applyInfo_info", applyInfo_service.getPopupInfo(bpm_no));
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loanCondition";
    }
    @PostMapping("/loanCondition")
    public String PostLoanConditionPage(@RequestParam("userid") String userid,@RequestParam("bpm_no") String bpm_no,Model model) {
        model.addAttribute("applyInfo_info", applyInfo_service.getPopupInfo(bpm_no));
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loanCondition";
    }

    @GetMapping("/loanReviewCondition")
    public String getLoanReviewConditionPage(@RequestParam("userid") String userid,@RequestParam("bpm_no") String bpm_no,Model model) {
        model.addAttribute("applyInfo_info", applyInfo_service.getPopupInfo(bpm_no));
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loanReviewCondition";
    }
    @PostMapping("/loanReviewCondition")
    public String PostLoanReviewConditionPage(@RequestParam("userid") String userid,@RequestParam("bpm_no") String bpm_no,Model model) {
        model.addAttribute("applyInfo_info", applyInfo_service.getPopupInfo(bpm_no));
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loanReviewCondition";
    }

    @ResponseBody
    @RequestMapping(value = "/loanConditionSubmit",method = RequestMethod.POST,consumes="application/json")
    public String loanConditionSubmit(@RequestBody Map<String, String> map){
        applyInfo_service.applyInfoSubmit( map.get("userid").toString(), map.get("bpm_no").toString(), map.get("stepcode").toString(),null);

        return "success";
    }

    @ResponseBody
    @RequestMapping(value = "/loanReviewConditionSubmit",method = RequestMethod.POST,consumes="application/json")
    public String loanReviewConditionSubmit(@RequestBody Map<String, String> map){
        applyInfo_service.applyInfoSubmit( map.get("userid").toString(), map.get("bpm_no").toString(), map.get("stepcode").toString(),map.get("result").toString());
        return "success";
    }

    @GetMapping("/loan")
    public String getLoanPage(@RequestParam("userid") String userid,@RequestParam("bpm_no") String bpm_no,Model model) {
        Loan popup_Loan=loanService.getPopupInfo(userid,bpm_no);
        popup_Loan=loanService.caulMatdate(popup_Loan);
        model.addAttribute("popup_Loan",popup_Loan);
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loan";
    }

    @PostMapping("/loan")
    public String PostLoanPage(@ModelAttribute Loan popup_Loan, @RequestParam("userid") String userid, @RequestParam("bpm_no") String bpm_no, Model model) {
//        loanService.saveLoan(popup_Loan);
        //C00010
        System.out.println("call TransferClient.transfer(LimitApprovalTx)");
        try {
            JSONObject map = new JSONObject();
            map.put("QREFNO", popup_Loan.getRefno()+"");
            map.put("QCUSTCD", popup_Loan.getCustcod()+"");
            map.put("QLNTYPE", popup_Loan.getLnccy() + "");
            map.put("QLINENO", popup_Loan.getCrline()+"");
            map.put("QFUNC", "HKBINCAN");
            map.put("QPRODUT", "HKBINMAS");
            map.put("QPRODAT", popup_Loan.getDwndate().replace("-","")+"");
            map.put("QLNAMT", popup_Loan.getLnamt()+"");
            String mess= ObjectHelp.InitTransferData2("LimitApprovalTx",map.toString());
            System.out.println("----send-----" + map.toString() + "----");
            String recieveMsg = client.transfer(mess);
            System.out.println("----customer_code-----" + recieveMsg+"----");
            System.out.println("----customer_code-----");
            if (recieveMsg != null && recieveMsg.length() > 0) {
                JSONObject jsonObject = JSONObject.fromObject(recieveMsg);
                String PRSPCOD = jsonObject.getString("PRSPCOD");
                String PRSPMSG = jsonObject.getString("PRSPMSG");
                String PRECODE = jsonObject.getString("PRECODE");
                if (PRSPCOD.equals("0000000") && PRSPMSG.equals("0000000")) {

                    loanService.saveLoan(popup_Loan);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //END

        model.addAttribute("popup_Loan",loanService.getPopupInfo(userid,bpm_no));
        model.addAttribute("info_Loan",new Loan());
        return "Loan/loan";
    }
}
