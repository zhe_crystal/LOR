package com.ecsolutions.service;

import com.ecsolutions.dao.RepayScheModReviewDao;
import com.ecsolutions.dao.repaymentScheModify_DAO;
import com.ecsolutions.entity.repaymentScheModify_entity;
import com.ecsolutions.entity.repaymentSchedule_entity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017-09-07.
 */
@Service("RepayScheModReviewService")
public class RepayScheModReviewServiceImpl implements RepayScheModReviewService{

    private RepayScheModReviewDao repayScheModReviewDao;
    public RepayScheModReviewServiceImpl(RepayScheModReviewDao repayScheModReviewDao) {
        this.repayScheModReviewDao = repayScheModReviewDao;
    }

    @Override
    public List<repaymentScheModify_entity> getResultList(String refno){
        List<repaymentScheModify_entity> result = repayScheModReviewDao.getResultList(refno);
        return result;
    };
    @Override
    public String getIntrate(String refno){
        return repayScheModReviewDao.getIntrate(refno);
    };
    @Override
    public String getStrdate(String refno,String curinno){
        return repayScheModReviewDao.getStrdate(refno,curinno);
    };
    @Override
    public repaymentScheModify_entity getLoanInfo(String refno){
         return repayScheModReviewDao.getLoanInfo(refno);
    };

    @Override
    public List<repaymentSchedule_entity> getRepaymentScheduleList(String refno){
        return repayScheModReviewDao.getRepayScheReviewList(refno);
    };
}
