package com.ecsolutions.service;

import com.ecsolutions.dao.ApplySearch_DAO;
import com.ecsolutions.dao.LoanDao;
import com.ecsolutions.entity.ApplySearch_Entity;
import com.ecsolutions.entity.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2017/8/22.
 */
@Service("LoanService")
public class LoanServiceImpl implements LoanService{
    private LoanDao loanDao ;
    @Autowired
    public LoanServiceImpl(LoanDao loanDao) {
        this.loanDao= loanDao;
    }


    @Override
    public Loan getPopupInfo(String userid,String bpm_no) {//popup信息来自多张表，在此进行整合
        Loan popupLoan = new Loan();
        popupLoan =loanDao.getPopupupByLoanrefFromUtilizationdetail(bpm_no);

        String menlei =popupLoan.getMenlei();
        String dalei =popupLoan.getDalei();
        String zhonglei =popupLoan.getZhonglei();
        String xiaolei =popupLoan.getXiaolei();

        //门类的编号信息转换成中文说明
        popupLoan.setMenlei(menlei+loanDao.getMenlei(menlei));
        popupLoan.setDalei(dalei+loanDao.getDalei(menlei,dalei));
        popupLoan.setZhonglei(zhonglei+loanDao.getZhonglei(menlei,dalei,zhonglei));
        popupLoan.setXiaolei(xiaolei+loanDao.getXiaolei(menlei,dalei,zhonglei,xiaolei));

        Loan popupLoan1 =loanDao.getPopupupByLoanrefFromHKBBIMAS(bpm_no);
        if(!(popupLoan1==null)) {
                popupLoan.setCufulnm(popupLoan1.getCufulnm());
                popupLoan.setWkcuad1(popupLoan1.getWkcuad1());
                popupLoan.setWkcuad2(popupLoan1.getWkcuad2());
                popupLoan.setWkcuad3(popupLoan1.getWkcuad3());
        }
        Loan popupLoan2 =loanDao.getPopupupByUseridFromTBLUESRS(userid);
        if(!(popupLoan2==null)) {
            popupLoan.setBranch(popupLoan2.getBranch());
        }

        Loan popupLoan3 =loanDao.getPopupupByLoanrefFrompaymentSchedule(bpm_no);
        if(!(popupLoan3==null)) {
                popupLoan.setInstamt(popupLoan3.getInstamt());
                popupLoan.setWtotins(popupLoan3.getWtotins());
        }
        Loan popupLoan4 =loanDao.getPopupupByloanreffFromHKBINSCH(bpm_no);
        if(!(popupLoan4==null)) {
                popupLoan.setDueonhl(popupLoan4.getDueonhl());
                popupLoan.setStlord1(popupLoan4.getStlord1());
                popupLoan.setStlord2(popupLoan4.getStlord2());
                popupLoan.setStlord3(popupLoan4.getStlord3());
                popupLoan.setStlord4(popupLoan4.getStlord4());
                popupLoan.setStlord5(popupLoan4.getStlord5());
                popupLoan.setLatchgs(popupLoan4.getLatchgs());
                popupLoan.setPenchgs(popupLoan4.getPenchgs());
                popupLoan.setGrace(popupLoan4.getGrace());
                popupLoan.setIntbase(popupLoan4.getIntbase());

                popupLoan.setPrtrps(popupLoan4.getPrtrps());
        }
        Loan popupLoan5 =loanDao.getPopupupByloanIdFromContractinformation(bpm_no);
        if(!(popupLoan5==null)) {
                popupLoan.setMargin(popupLoan5.getMargin());
                popupLoan.setLgamt(popupLoan5.getLgamt());
        }
        Loan popupLoan6 =loanDao.getPopupupByloanIdFromloanapplicantfacilityinfo(bpm_no);
        if(!(popupLoan6==null)) {
            popupLoan.setCountry(popupLoan6.getCountry());
        }
        return  popupLoan;
    }

    @Override
    public  void saveLoan(Loan loan){
        loanDao.saveLoan(loan);
    };

    @Override
    public void paymentSchedule(Loan loanAfter,Loan loanBefore){
        if( (!loanAfter.getWpaydat().equals(loanBefore.getWpaydat())) || (!loanAfter.getDwndate().equals(loanBefore.getDwndate())) ){


        }
    };

    @Override
    public Loan caulMatdate(Loan calLoan){//MATDATE
        String s1 =calLoan.getWpaydat();//首次还款日
        String s2 =calLoan.getRpyfeq();//还款周期频率
        String s3 =calLoan.getRpyper();//还款周期类型
        String s4 =calLoan.getInstno();//期数

        if((s1!=null)&&(s2!=null)&&(s3!=null)&&(s4!=null)) {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//小写的mm表示的是分钟
            String dstr = calLoan.getWpaydat();
            Date date = null;
            try {
                date = sdf.parse(dstr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            c.setTime(date);


            if(s3.equals("M")||s3.equals("m")) {
                c.add(Calendar.MONTH, (Integer.parseInt(s4)*Integer.parseInt(s2)-1));
            }
            if(s3.equals("D")||s3.equals("d")) {
                c.add(Calendar.DATE, (Integer.parseInt(s4)*Integer.parseInt(s2)-1));
            }
            if(s3.equals("Y")||s3.equals("y")) {
                c.add(Calendar.YEAR, (Integer.parseInt(s4)*Integer.parseInt(s2)-1));
            }
            String matDate=sdf.format(c.getTime());
            calLoan.setMatdate(matDate);
        }
        return calLoan;

    };

    @Override
    public Loan getLoan(String bpm_no){
        return loanDao.getLoanByBpm_no(bpm_no);
    };

    @Override
    public void saveLoanReview(String bpm_no,String userid){
        loanDao.savaLoanReview(bpm_no,userid);
    };

}
