package com.ecsolutions.controller;

import com.ecsolutions.Validators.CustomerValidate;
import com.ecsolutions.common.ObjectHelp;
import com.ecsolutions.entity.Customer_entity;
import com.ecsolutions.service.Customer_Service;
import com.ecsolutions.soaClient.TransferClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/6/27.
 */
@Controller
public class Customer_Controller {

    @Autowired
    private Customer_Service customer_service;

    @Autowired
    private TransferClient client;

    public Customer_Service getCustomer_service() {
        return customer_service;
    }

    @InitBinder("customer_entity")
    public void initBinder(WebDataBinder binder){
        binder.setValidator(new CustomerValidate());
    }

    //个人用户申请界面
    @GetMapping("/customerInfo")
    public String getApplyPage(Model model) {
        Customer_entity customer_entity=new Customer_entity();
//        System.out.println("call TransferClient.transfer(GenerateCustcodTx)");
//        String pcustcd = "";
//        String prspcod = "";
//        try {
//            String mess = ObjectHelp.InitTransferData("GenerateCustcodTx", null);
////        System.out.println("****customer_code:"+client.transfer(mess));
//            String recieveMsg = client.transfer(mess);
//            if (recieveMsg != null || recieveMsg.length() > 0) {
//                recieveMsg = recieveMsg.replace("{", "").replace("}", "");
//                String arr[] = recieveMsg.split(",");
//                int pcustcdIndex = arr[1].lastIndexOf(":");
//                int prspcodIndex = arr[2].lastIndexOf(":");
//                pcustcd = (arr[1].substring(pcustcdIndex + 1)).replace("\"", "");
//                prspcod = arr[2].substring(prspcodIndex + 1).replace("\"", "");
////                Map<String,String> map=new HashMap<String,String>();
////                map.put("pcustcd",pcustcd);
////                map.put("prspcod",prspcod);
//                if (prspcod.equals("0000000")) {
//                    System.out.println("get CustomerCode success");
////                    model.addAttribute("generateCustcd",map);
//                } else {
//                    System.out.println("Cannot get CustomerCode From Soa");
//                }
//                customer_entity.setSoacustcode(pcustcd);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        model.addAttribute("customer_entity", customer_entity);
        model.addAttribute("customer_info", customer_service.getPopupInfo());
        return "customer";
    }

    //个人用户信息查看修改界面
    @GetMapping("/PersonalUserInfo")
    public String getApplyPage(@RequestParam String custcode, Model model) {
        //这里填充数据
        Customer_entity customer_entity = new Customer_entity();
        model.addAttribute("customer_entity", customer_entity);
        model.addAttribute("customer_info", customer_service.getPopupInfo());
        return "customer";
    }

    @PostMapping("/customerInfo")
    public String saveApplyInfo(@Valid @ModelAttribute("customer_entity") Customer_entity customer_entity, BindingResult result, Model model) {
        model.addAttribute("customer_entity", customer_entity);
        model.addAttribute("customer_info", customer_service.getPopupInfo());
        if (!(result.hasErrors())){
            System.out.println("call TransferClient.transfer(GenerateCustcodTx)");
            String pcustcd = "";
            String prspcod = "";
            try {
                String mess = ObjectHelp.InitTransferData("GenerateCustcodTx", null);
//        System.out.println("****customer_code:"+client.transfer(mess));
                String recieveMsg = client.transfer(mess);
                if (recieveMsg != null || recieveMsg.length() > 0) {
                    recieveMsg = recieveMsg.replace("{", "").replace("}", "");
                    String arr[] = recieveMsg.split(",");
                    int pcustcdIndex = arr[1].lastIndexOf(":");
                    int prspcodIndex = arr[2].lastIndexOf(":");
                    pcustcd = (arr[1].substring(pcustcdIndex + 1)).replace("\"", "");
                    prspcod = arr[2].substring(prspcodIndex + 1).replace("\"", "");
//                Map<String,String> map=new HashMap<String,String>();
//                map.put("pcustcd",pcustcd);
//                map.put("prspcod",prspcod);
                    if (prspcod.equals("0000000")) {
                        System.out.println("get CustomerCode success");
//                    model.addAttribute("generateCustcd",map);
                    } else {
                        System.out.println("Cannot get CustomerCode From Soa");
                    }
                    customer_entity.setSoacustcode(pcustcd);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            customer_service.saveCustomerEntity(customer_entity);
        }
        return "customer";
    }

    @ResponseBody
    @RequestMapping(value = "/customer_PidnoCheck",method = RequestMethod.POST)
    public String findByPidno(String pidno){
        //Pledge_Entity pledge_entity = pledgeNew_service.getByEthnic(ethnic);
        //return pledge_entity;
        String result = customer_service.pidnoCheck(pidno);
        return result;
    }

    /*通过门类值查询信息到大类下拉框*/
    @ResponseBody
    @RequestMapping(value = "/customer_getDaleiInfo",method = RequestMethod.POST)
    public List<HashMap<String,String>> getDaleiList(String industrialClass){
        List<HashMap<String,String>> daleiList = customer_service.getByMenleiInfo(industrialClass);
        return daleiList;
    }

    /*通过门类和大类查询信息到中类下拉框*/
    @ResponseBody
    @RequestMapping(value = "/customer_getZhongleiInfo",method = RequestMethod.POST)
    public List<HashMap<String,String>> getZhongleiList(String industrialClass,String industrialPrimaryCategory){
        List<HashMap<String,String>> zhongleiList = customer_service.getByMenleiAndDaleiInfo(industrialClass,industrialPrimaryCategory);
        return zhongleiList;
    }
}
