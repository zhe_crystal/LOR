package com.ecsolutions.entity;

import java.math.BigDecimal;

/**
 * Created by ecs on 2017/9/6.
 */
public class repaymentSchedule_entity {
    private String userid;
    private String refno;
    private Integer verno;
    private String intrate;
    private String instno;
    private String func;
    private String schetyp;
    private String subtype;
    private String custcod;
    private String strdate;
    private String duedate;
    private BigDecimal instamt;
    private String ratetyp;
    private BigDecimal spread;
    private Integer prntier;
    private Integer inttier;
    private BigDecimal begprinamt;
    private BigDecimal prnamt;
    private BigDecimal intamt;
    private BigDecimal lnosamt;
    private String lnosccy;

    private BigDecimal sum_instamt;
    private BigDecimal sum_intamt;
    private BigDecimal sum_prnamt;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getIntrate() {
        return intrate;
    }

    public void setIntrate(String intrate) {
        this.intrate = intrate;
    }

    public BigDecimal getLnosamt() {
        return lnosamt;
    }

    public void setLnosamt(BigDecimal lnosamt) {
        this.lnosamt = lnosamt;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }

    public BigDecimal getBegprinamt() {
        return begprinamt;
    }

    public void setBegprinamt(BigDecimal begprinamt) {
        this.begprinamt = begprinamt;
    }

    public String getInstno() {
        return instno;
    }

    public void setInstno(String instno) {
        this.instno = instno;
    }

    public String getStrdate() {
        return strdate;
    }

    public void setStrdate(String strdate) {
        this.strdate = strdate;
    }

    public BigDecimal getInstamt() {
        return instamt;
    }

    public void setInstamt(BigDecimal instamt) {
        this.instamt = instamt;
    }

    public BigDecimal getPrnamt() {
        return prnamt;
    }

    public void setPrnamt(BigDecimal prnamt) {
        this.prnamt = prnamt;
    }

    public BigDecimal getIntamt() {
        return intamt;
    }

    public void setIntamt(BigDecimal intamt) {
        this.intamt = intamt;
    }

    public Integer getVerno() {
        return verno;
    }

    public void setVerno(Integer verno) {
        this.verno = verno;
    }

    public String getFunc() {
        return func;
    }

    public void setFunc(String func) {
        this.func = func;
    }

    public String getSchetyp() {
        return schetyp;
    }

    public void setSchetyp(String schetyp) {
        this.schetyp = schetyp;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getCustcod() {
        return custcod;
    }

    public void setCustcod(String custcod) {
        this.custcod = custcod;
    }

    public String getRatetyp() {
        return ratetyp;
    }

    public void setRatetyp(String ratetyp) {
        this.ratetyp = ratetyp;
    }

    public BigDecimal getSpread() {
        return spread;
    }

    public void setSpread(BigDecimal spread) {
        this.spread = spread;
    }

    public Integer getPrntier() {
        return prntier;
    }

    public void setPrntier(Integer prntier) {
        this.prntier = prntier;
    }

    public Integer getInttier() {
        return inttier;
    }

    public void setInttier(Integer inttier) {
        this.inttier = inttier;
    }

    public String getLnosccy() {
        return lnosccy;
    }

    public void setLnosccy(String lnosccy) {
        this.lnosccy = lnosccy;
    }

    public BigDecimal getSum_instamt() {
        return sum_instamt;
    }

    public void setSum_instamt(BigDecimal sum_instamt) {
        this.sum_instamt = sum_instamt;
    }

    public BigDecimal getSum_intamt() {
        return sum_intamt;
    }

    public void setSum_intamt(BigDecimal sum_intamt) {
        this.sum_intamt = sum_intamt;
    }

    public BigDecimal getSum_prnamt() {
        return sum_prnamt;
    }

    public void setSum_prnamt(BigDecimal sum_prnamt) {
        this.sum_prnamt = sum_prnamt;
    }
}
