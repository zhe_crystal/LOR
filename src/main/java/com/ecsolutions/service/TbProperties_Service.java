package com.ecsolutions.service;

import com.ecsolutions.entity.TbProperties_Entity;

import java.util.List;

public interface TbProperties_Service {

    boolean save(TbProperties_Entity entity);
    boolean update(TbProperties_Entity entity);
    List<TbProperties_Entity> findOne(String propertiesid);
    List<TbProperties_Entity> findAll();
    Integer countOne(String propertiesid);
    Integer countAll();
    boolean delete(String propertiesid);
}
