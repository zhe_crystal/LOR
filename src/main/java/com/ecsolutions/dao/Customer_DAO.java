package com.ecsolutions.dao;

import com.ecsolutions.dao.SqlProvider.ApplyDAO_Provider;
import com.ecsolutions.entity.Acpy_Customer_Entity;
import com.ecsolutions.entity.Customer_entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/6/28.
 */

@Repository
public interface Customer_DAO {
    // select bankcod
    @Select("SELECT distinct SUBORNO,BCHNAME FROM TABCHMAS")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getBanks();

//    // select custcod
//    @Select("SELECT custcod FROM LoanApplicant where rownum <= 1")
//    @ResultType(String.class)
//    String getCustcod();

    // select managername
    @Select("SELECT distinct ACCOFFE,DESCRIP FROM TAACOMAS")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getCustManagerNames();

//    //select custtype
//    @Select("SELECT (CODTYPE||CSTCOD2) AS CODE,CSTLDES FROM TACSTMAS WHERE CODTYPE = 'ID'")
//    @ResultType(HashMap.class)
//    List<HashMap<String,String>>  getpIdTypeId();

    // select idType
    @Select("SELECT CSTCOD2 AS CODE,CSTLDES FROM TACSTMAS WHERE CODTYPE = 'ID'")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getIdType();

    // select 婚姻状况
    @Select("SELECT distinct maritalStatusId,name FROM MaritalStatusText")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getMaritalStatus();

    //select 居住状况
    @Select("select distinct typeid,name from LivingConditionsText where localeid=1")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getJuzhuStatus();

    //select 职业
    @Select("select distinct typeid,name from VocationalText where localeid=1")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getZhiye();

    //select 职务
    @Select("select distinct typeid,name from DutyText where localeid=1")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getZhiWu();

    //select 职称
    @Select("select distinct typeid,name from DutyTitleText where localeid=1")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getZhiChen();

    //select 年收入币种
    @Select("select distinct CCY from TACCYMAS")
    @ResultType(HashMap.class)
    List<String> getCcy();

    //select 最高学历
    @Select("select distinct educationLevelId,name from EducationLevelText where localeid=1 order by educationLevelId desc")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getXueli();

    //select 最高学位
    @Select("select distinct TypeId,name from HighestDegreeText where localeid=1")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getXueWei();

    //select 国家
    @Select("select distinct COUNTRY,COUNNAM from TACTYMAS")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getGuoJia();

    //select 省市
    @Select("select distinct cityid,name from CityText where localeid=1")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getShengShi();

    //select 门类
    @Select("select distinct menlei,leibiemingcheng from Hangye WHERE DALEI is null AND ZHONGLEI is null")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getIndustryClassList();

    //大类
    @Select("SELECT DALEI,LEIBIEMINGCHENG FROM HANGYE WHERE TRIM(MENLEI) = TRIM(#{industrialClass}) AND DALEI is not null AND ZHONGLEI is null")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getDaleiList(@Param("industrialClass") String industrialClass);

    //中类
    @Select("SELECT ZHONGLEI,LEIBIEMINGCHENG FROM HANGYE WHERE TRIM(MENLEI) = TRIM(#{industrialClass}) AND TRIM(DALEI) = TRIM(#{industrialPrimaryCategory}) AND ZHONGLEI is not null")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getZhongleiList(@Param("industrialClass") String industrialClass,@Param("industrialPrimaryCategory") String industrialPrimaryCategory);

//    //select 大类
//    @Select("select distinct dalei from Hangye where rownum < 100")
//    @ResultType(HashMap.class)
//    List<String> getIndustryCategoryList();
//
//    //select 中类
//    @Select("select distinct zhonglei,shuoming from Hangye where rownum < 100 and ZHONGLEI is not null and SHUOMING is not null")
//    @ResultType(HashMap.class)
//    List<HashMap<String,String>> getSecIndustryCategoryList();

    //select pidno
    @Select("select count(*) from loanapplicant where pidno = #{pidno}")
    @ResultType(String.class)
    String isPidnoExist(String pidno);
//    @Select("SELECT DISTINCT  FROM  )")
//    @ResultType(String.class)
//    List<String> getFinancingList();

    @Insert("insert into LoanApplicant(personalflag,foreign,bankCode,ACCOFFIC,custcod,pIdTypeId,pidno,nationality,birthDate,gender,OrginalName,"
            +"CreditCardNumber,lastname,firstname,PoliticalStatus,residentIndicator,LEMAILAD,"
            +"NumberOfFamilyMembers,livingConditions,unitsName,startYear,vocational,duty,dutyTitle,revenueYearCCY,"
            +"revenueYear,wagesAccount,waDepositaryBank,"
            +"educationLevelId,highestDegree,ethnic,CreditCardPassword,isbankshareholder,ShareholderID,InvestmentCurrency,InvestmentAmount,InvestmentDate,InvestmentProportion,InvestmentMethod,"
            +"isbankstaff,BankDuty,Remarks,macountry,maCity,CommunicationAddress,maPostCode,LHOMETEL,LHPHUM,hjCountry,hjCity,domicileAddress,hjPostCode,HJSTREET,HJBUILDING,HJFLOOR,ResidenceCountry,"+
            "ResidenceCity,ResidenceAddress,CommunicationAddressPostalCode,applydate) "+
            "values("+"#{personalflag,jdbcType=VARCHAR},#{foreign,jdbcType=VARCHAR},#{bankCode,jdbcType=VARCHAR},#{ACCOFFIC,jdbcType=VARCHAR},#{custcod},#{pIdTypeId},#{pidno,jdbcType=VARCHAR},"+ "#{nationality},"
            +"to_number(#{birthDate,jdbcType=VARCHAR}),"+"#{gender,jdbcType=VARCHAR}," +"#{OrginalName,jdbcType=VARCHAR}," +"#{CreditCardNumber,jdbcType=VARCHAR},"
            +"#{lastname,jdbcType=VARCHAR},"+"#{firstname,jdbcType=VARCHAR},"+"#{PoliticalStatus}," +"#{residentIndicator,jdbcType=VARCHAR},"+"#{LEMAILAD,jdbcType=VARCHAR},"
            +"to_number(#{NumberOfFamilyMembers,jdbcType=VARCHAR}),"+"#{livingConditions},"+"#{unitsName},"
            +"#{startYear,jdbcType=NUMERIC},"+"#{vocational},"+"#{duty},"
            +"#{dutyTitle,jdbcType=VARCHAR},"+"#{revenueYearCCY},"+"to_number(#{revenueYear,jdbcType=VARCHAR}),"+"#{wagesAccount,jdbcType=VARCHAR},"
            +"#{waDepositaryBank,jdbcType=VARCHAR}," +"#{educationLevelId,jdbcType=VARCHAR},"+"#{highestDegree,jdbcType=VARCHAR},"+"#{ethnic,jdbcType=VARCHAR},"
            +"#{CreditCardPassword,jdbcType=VARCHAR},to_number(#{isbankshareholder,jdbcType=BIT}),"+"#{ShareholderID,jdbcType=VARCHAR},"
            +"#{InvestmentCurrency,jdbcType=VARCHAR},"+ "to_number(#{InvestmentAmount,jdbcType=VARCHAR})," +"to_number(#{InvestmentDate,jdbcType=VARCHAR}),"+"to_number(#{InvestmentProportion}),"
            +"#{InvestmentMethod,jdbcType=VARCHAR},"+"to_number(#{isbankstaff,jdbcType=BIT}),#{BankDuty,jdbcType=VARCHAR},"+"#{Remarks,jdbcType=VARCHAR},"
            +"#{macountry,jdbcType=VARCHAR}," +"#{maCity},"+"#{CommunicationAddress,jdbcType=VARCHAR},"+"#{maPostCode,jdbcType=VARCHAR},"+"#{LHOMETEL,jdbcType=VARCHAR},"
            + "#{LHPHUM,jdbcType=VARCHAR}," +"#{hjCountry,jdbcType=VARCHAR},"+"#{hjCity,jdbcType=VARCHAR},"+"#{domicileAddress,jdbcType=VARCHAR},"+"#{hjPostCode,jdbcType=VARCHAR},"
            + "#{HJSTREET,jdbcType=VARCHAR},"+"#{HJBUILDING,jdbcType=VARCHAR},"+"#{HJFLOOR,jdbcType=VARCHAR},"+"#{ResidenceCountry,jdbcType=VARCHAR},"+"#{ResidenceCity,jdbcType=VARCHAR},"
            +"#{ResidenceAddress,jdbcType=VARCHAR},"+"#{CommunicationAddressPostalCode,jdbcType=VARCHAR},#{applyDate,jdbcType=VARCHAR})")
    void insert(Customer_entity customer_entity);


    @SelectProvider(type = ApplyDAO_Provider.class, method = "getSearchInfoList")
    @ResultType(Customer_entity.class)
    List<Customer_entity> getSearchInfoList(String PersonalOrEnterprise,
                                         String startDate,
                                         String endDate,
                                         String chineseName,
                                         String englishName,
                                         String licenseNumber,
                                         String creditCardNumber);

    @SelectProvider(type = ApplyDAO_Provider.class, method = "countSearchInfoList")
    @ResultType(Integer.class)
    Integer countSearchInfoList(String PersonalOrEnterprise,
                                           String startDate,
                                           String endDate,
                                           String chineseName,
                                           String englishName,
                                           String licenseNumber,
                                           String creditCardNumber);

    @Select("SELECT * FROM LoanApplicant Where custcod=#{custcod,jdbcType=VARCHAR}")
    @ResultType(Acpy_Customer_Entity.class)
    Acpy_Customer_Entity searchCustomer(String custcod);

    @Select("SELECT personalflag FROM LoanApplicant Where custcod=#{custcod,jdbcType=VARCHAR}")
    @ResultType(String.class)
    String getCustomerType(String custcod);
}
