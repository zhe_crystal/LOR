package com.ecsolutions.entity;

public class CreditAmountRecheck_Entity {
    private String refnum;
    private String obkgid;
    private String custnm;
    private String obkglin;
    private String lntype;
    private String prdccy;
    private String obkgamt;
    private String bkuser;
    private String bokdate;
    private String boktime;
    private String pdgrsn;
    private String actcode;
    private String apprmk1;
    private String appid1;
    private String username;
    private String appdat1;
    private String apptim1;

    public String getRefnum() {
        return refnum;
    }

    public void setRefnum(String refnum) {
        this.refnum = refnum;
    }

    public String getObkgid() {
        return obkgid;
    }

    public void setObkgid(String obkgid) {
        this.obkgid = obkgid;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getObkglin() {
        return obkglin;
    }

    public void setObkglin(String obkglin) {
        this.obkglin = obkglin;
    }

    public String getLntype() {
        return lntype;
    }

    public void setLntype(String lntype) {
        this.lntype = lntype;
    }

    public String getPrdccy() {
        return prdccy;
    }

    public void setPrdccy(String prdccy) {
        this.prdccy = prdccy;
    }

    public String getObkgamt() {
        return obkgamt;
    }

    public void setObkgamt(String obkgamt) {
        this.obkgamt = obkgamt;
    }

    public String getBkuser() {
        return bkuser;
    }

    public void setBkuser(String bkuser) {
        this.bkuser = bkuser;
    }

    public String getBokdate() {
        return bokdate;
    }

    public void setBokdate(String bokdate) {
        this.bokdate = bokdate;
    }

    public String getBoktime() {
        return boktime;
    }

    public void setBoktime(String boktime) {
        this.boktime = boktime;
    }

    public String getPdgrsn() {
        return pdgrsn;
    }

    public void setPdgrsn(String pdgrsn) {
        this.pdgrsn = pdgrsn;
    }

    public String getActcode() {
        return actcode;
    }

    public void setActcode(String actcode) {
        this.actcode = actcode;
    }

    public String getApprmk1() {
        return apprmk1;
    }

    public void setApprmk1(String apprmk1) {
        this.apprmk1 = apprmk1;
    }

    public String getAppid1() {
        return appid1;
    }

    public void setAppid1(String appid1) {
        this.appid1 = appid1;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAppdat1() {
        return appdat1;
    }

    public void setAppdat1(String appdat1) {
        this.appdat1 = appdat1;
    }

    public String getApptim1() {
        return apptim1;
    }

    public void setApptim1(String apptim1) {
        this.apptim1 = apptim1;
    }
}
