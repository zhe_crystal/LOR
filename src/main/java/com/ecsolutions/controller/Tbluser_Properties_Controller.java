package com.ecsolutions.controller;

import com.ecsolutions.entity.Tbluser_Properties_Entity;
import com.ecsolutions.service.Tbluser_Properties_Service;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@Controller
public class Tbluser_Properties_Controller {
    private String UserId;

    @Autowired
    private Tbluser_Properties_Service tbluser_properties_service;

    public Tbluser_Properties_Service getTbluser_properties_service() {
        return tbluser_properties_service;
    }

    @GetMapping("/Tbluser_Properties")
    public String getTbluser_Properties(Model model, @RequestParam String UserId) {
        Tbluser_Properties_Entity tbluser_properties_entity = new Tbluser_Properties_Entity();
        tbluser_properties_entity.setUserId(UserId);
        this.UserId = UserId;
        model.addAttribute("tbluser_properties_entity", tbluser_properties_entity);
        return "ClientInformation/Tbluser_Properties";
    }

    @PostMapping("/Tbluser_Properties")
    public String postTbluser_Properties(Model model, @ModelAttribute Tbluser_Properties_Entity tbluser_properties_entity) {
        tbluser_properties_entity.setUserId(UserId);
        model.addAttribute("tbluser_properties_entity", tbluser_properties_entity);
        return "ClientInformation/Tbluser_Properties";
    }

    @ResponseBody
    @RequestMapping(value = "/Tbluser_Properties/loadBody", method = RequestMethod.GET)
    public String loadBody() {
        List<Tbluser_Properties_Entity> loadList = tbluser_properties_service.getAllByUserId(UserId);
        Gson gson = new Gson();
        String loadInfo = gson.toJson(loadList);
        return loadInfo;
    }

    @ResponseBody
    @RequestMapping(value = "/Tbluser_Properties/postCurrentUserInfo", consumes = "application/json", method = RequestMethod.POST)
    public String getCurrentUserInfo(@RequestBody List<String> htmlList, @RequestParam String UserId) {
        boolean state = true;
        for (int i = 0; i < htmlList.size() / 2; i++) {
            Tbluser_Properties_Entity entity = new Tbluser_Properties_Entity();
            entity.setUserId(UserId);
            entity.setPropertiesId(htmlList.get(2 * i));
            entity.setPropertiesValue(htmlList.get(2 * i + 1));
            state = tbluser_properties_service.updateOneByAll(entity) & true;
        }
        if (state)
            return "success";
        else
            return "fail";
    }
}
