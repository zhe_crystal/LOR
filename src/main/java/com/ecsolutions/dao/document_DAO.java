package com.ecsolutions.dao;

import com.ecsolutions.entity.document_entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/7/28.
 */
@Repository
public interface document_DAO{

        // select lineDesc
        @Select("SELECT distinct lineDesc FROM loanapplicantfacilityinfo where rownum = 1")
        @ResultType(String.class)
        String getLineDesc();

        //check line_no
        @Select("SELECT count(*) FROM loanapplicantfacilityinfo WHERE custcod = #{custcode} and lineno = #{line_no}")
        @ResultType(String.class)
        String checklineno(@Param("line_no") String line_no, @Param("custcode") String custcode);

        //check insert
        @Select("SELECT count(*) FROM LoanDocumentationDetailsInfo WHERE custcode = #{custcode} and line_no = #{line_no} and docNo = #{docNo}")
        @ResultType(String.class)
        String checkinsert(@Param("custcode") String custcode,@Param("line_no") String line_no,@Param("docNo") String docNo);

        // select by custcode
        @Select("SELECT * FROM LoanDocumentationDetailsInfo WHERE TRIM(custcode) = TRIM(#{custcode})")
        @ResultType(document_entity.class)
        List<document_entity> getDocInfo(String custcode);

        //根据文档号docNo删除文档信息
        @Delete("delete from LoanDocumentationDetailsInfo where docNo=#{docNo}")
        void delete(String docNo);

        @Insert("insert into LoanDocumentationDetailsInfo(custcode,line_no,lineDesc,docNo,doctype,descripe,docName,imageTag,physicalAddr,recieveInd,recivedate,effectivedate,expirydate,requesttype,approvaldate,loanId,docDirName,fileName,isScanUploadId,receiveddate) "
                +"values(#{custcode,jdbcType=VARCHAR},#{line_no,jdbcType=VARCHAR},#{lineDesc,jdbcType=VARCHAR},#{docNo,jdbcType=VARCHAR},#{doctype,jdbcType=NUMERIC},#{descripe,jdbcType=VARCHAR},#{docName,jdbcType=VARCHAR},#{imageTag,jdbcType=VARCHAR},#{physicalAddr,jdbcType=VARCHAR},"
                +"#{recieveInd,jdbcType=VARCHAR},#{recivedate,jdbcType=TIMESTAMP},#{effectivedate,jdbcType=TIMESTAMP},#{expirydate,jdbcType=TIMESTAMP},#{requesttype,jdbcType=NUMERIC},#{approvaldate,jdbcType=TIMESTAMP},#{loanId,jdbcType=NUMERIC},"
                +"#{docDirName,jdbcType=VARCHAR},#{fileName,jdbcType=VARCHAR},#{isScanUploadId,jdbcType=NUMERIC},#{receiveddate,jdbcType=TIMESTAMP})")
        void insert(document_entity document_entity);

        @Update("update LoanDocumentationDetailsInfo set custcode=#{custcode,jdbcType=VARCHAR},line_no=#{line_no,jdbcType=VARCHAR},lineDesc=#{lineDesc,jdbcType=VARCHAR},docNo=#{docNo,jdbcType=VARCHAR},doctype=#{doctype,jdbcType=NUMERIC},descripe=#{descripe,jdbcType=VARCHAR},"
                +"docName=#{docName,jdbcType=VARCHAR},imageTag=#{imageTag,jdbcType=VARCHAR},physicalAddr=#{physicalAddr,jdbcType=VARCHAR},recieveInd=#{recieveInd,jdbcType=VARCHAR},recivedate=#{recivedate,jdbcType=TIMESTAMP},effectivedate=#{effectivedate,jdbcType=TIMESTAMP},"
                +"expirydate=#{expirydate,jdbcType=TIMESTAMP},requesttype=#{requesttype,jdbcType=NUMERIC},approvaldate=#{approvaldate,jdbcType=TIMESTAMP},loanId=#{loanId,jdbcType=NUMERIC},docDirName=#{docDirName,jdbcType=VARCHAR},fileName=#{fileName,jdbcType=VARCHAR},"
                +"isScanUploadId=#{isScanUploadId,jdbcType=NUMERIC},receiveddate=#{receiveddate,jdbcType=TIMESTAMP} where custcode=#{custcode} and line_no=#{line_no} and docNo=#{docNo}")
        void update(document_entity document_entity);
}
