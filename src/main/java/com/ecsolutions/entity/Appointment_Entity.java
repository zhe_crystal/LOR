package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Administrator on 2017/7/31.
 */
public class Appointment_Entity {
    private Integer id;
    private String bpm_no;
    private String custcode;
    private Integer contactid;
    private String appointclient;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date appointdate;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date appointtime;
    private String appointplace;
    private String appointuser;
    private String remarks;
    private String createdby;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date createdon;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date modifiedon;
    private String modifiedby;
    private Integer appointclient_state;
    private Integer appointdate_state;
    private Integer appointtime_state;
    private Integer appointplace_state;
    private Integer appointuser_state;
    private Integer remarks_state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBpm_no() {
        return bpm_no;
    }

    public void setBpm_no(String bpm_no) {
        this.bpm_no = bpm_no;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public Integer getContactid() {
        return contactid;
    }

    public void setContactid(Integer contactid) {
        this.contactid = contactid;
    }

    public String getAppointclient() {
        return appointclient;
    }

    public void setAppointclient(String appointclient) {
        this.appointclient = appointclient;
    }

    public Date getAppointdate() {
        return appointdate;
    }

    public void setAppointdate(Date appointdate) {
        this.appointdate = appointdate;
    }

    public Date getAppointtime() {
        return appointtime;
    }

    public void setAppointtime(Date appointtime) {
        this.appointtime = appointtime;
    }

    public String getAppointplace() {
        return appointplace;
    }

    public void setAppointplace(String appointplace) {
        this.appointplace = appointplace;
    }

    public String getAppointuser() {
        return appointuser;
    }

    public void setAppointuser(String appointuser) {
        this.appointuser = appointuser;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Date getModifiedon() {
        return modifiedon;
    }

    public void setModifiedon(Date modifiedon) {
        this.modifiedon = modifiedon;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Integer getAppointclient_state() {
        return appointclient_state;
    }

    public void setAppointclient_state(Integer appointclient_state) {
        this.appointclient_state = appointclient_state;
    }

    public Integer getAppointdate_state() {
        return appointdate_state;
    }

    public void setAppointdate_state(Integer appointdate_state) {
        this.appointdate_state = appointdate_state;
    }

    public Integer getAppointtime_state() {
        return appointtime_state;
    }

    public void setAppointtime_state(Integer appointtime_state) {
        this.appointtime_state = appointtime_state;
    }

    public Integer getAppointplace_state() {
        return appointplace_state;
    }

    public void setAppointplace_state(Integer appointplace_state) {
        this.appointplace_state = appointplace_state;
    }

    public Integer getAppointuser_state() {
        return appointuser_state;
    }

    public void setAppointuser_state(Integer appointuser_state) {
        this.appointuser_state = appointuser_state;
    }

    public Integer getRemarks_state() {
        return remarks_state;
    }

    public void setRemarks_state(Integer remarks_state) {
        this.remarks_state = remarks_state;
    }
}