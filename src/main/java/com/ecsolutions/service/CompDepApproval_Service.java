package com.ecsolutions.service;

import com.ecsolutions.entity.CompDepApproval_entity;

/**
 * Created by ecs on 2017/8/29.
 */
public interface CompDepApproval_Service {
    CompDepApproval_entity getPopupInfo(String userid,String loanid);
    String getPuid(String loanid);
    void saveApprovalInfo(String loanid,String userid,CompDepApproval_entity approval_entity);
    void approvalInfoSubmit(String userid,String loanid,String stepcode,String result);
}
