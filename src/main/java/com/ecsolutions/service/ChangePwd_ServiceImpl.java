package com.ecsolutions.service;

import com.ecsolutions.dao.ChangePwd_DAO;
import com.ecsolutions.entity.ChangePwd_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/8/11.
 */
@Service("ChangePwd_Service")
public class ChangePwd_ServiceImpl implements  ChangePwd_Service {
    private ChangePwd_DAO changePwd_dao ;
    @Autowired
    public ChangePwd_ServiceImpl(ChangePwd_DAO changePwd_dao) {
        this.changePwd_dao= changePwd_dao;
    }

    @Override
    public String getPassword(String id){
        String password =changePwd_dao.getPassword(id);
        return password;
    }
    @Override
    public void changePwd(ChangePwd_Entity changePwd_entity){
        changePwd_dao.update(changePwd_entity);
    };
}
