package com.ecsolutions.entity;

import org.hibernate.validator.constraints.Email;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 2017/7/31.
 */
public class ApplicationInfo_Entity {
    private String custcode;
    private String line_no;
    private String loanref;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date applicationdate;
    private BigDecimal applicationamount;
    private BigDecimal rate;
    private BigDecimal deadline;
    private String loanpurpose;
    private String loanpurposedesc;
    private String fluserid;

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public String getLine_no() {
        return line_no;
    }

    public void setLine_no(String line_no) {
        this.line_no = line_no;
    }

    public String getLoanref() {
        return loanref;
    }

    public void setLoanref(String loanref) {
        this.loanref = loanref;
    }

    public Date getApplicationdate() {
        return applicationdate;
    }

    public void setApplicationdate(Date applicationdate) {
        this.applicationdate = applicationdate;
    }

    public BigDecimal getApplicationamount() {
        return applicationamount;
    }

    public void setApplicationamount(BigDecimal applicationamount) {
        this.applicationamount = applicationamount;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getDeadline() {
        return deadline;
    }

    public void setDeadline(BigDecimal deadline) {
        this.deadline = deadline;
    }

    public String getLoanpurpose() {
        return loanpurpose;
    }

    public void setLoanpurpose(String loanpurpose) {
        this.loanpurpose = loanpurpose;
    }

    public String getLoanpurposedesc() {
        return loanpurposedesc;
    }

    public void setLoanpurposedesc(String loanpurposedesc) {
        this.loanpurposedesc = loanpurposedesc;
    }

    public String getFluserid() {
        return fluserid;
    }

    public void setFluserid(String fluserid) {
        this.fluserid = fluserid;
    }
}