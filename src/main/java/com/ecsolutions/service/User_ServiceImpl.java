package com.ecsolutions.service;

import com.ecsolutions.dao.TABCHMAS_DAO;
import com.ecsolutions.dao.TblRole_DAO;
import com.ecsolutions.dao.User_DAO;
import com.ecsolutions.dao.tbluser_role_DAO;
import com.ecsolutions.entity.User_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/8/18.
 */
@Service("User_Service")
public class User_ServiceImpl implements User_Service {
    private TABCHMAS_DAO tabchmas_dao;
    private User_DAO user_dao;
    private TblRole_DAO tblRole_dao;
    private tbluser_role_DAO tbluser_role_dao;

    @Autowired
    public User_ServiceImpl(TABCHMAS_DAO tabchmas_dao, User_DAO user_dao, TblRole_DAO tblRole_dao, tbluser_role_DAO tbluser_role_dao) {
        this.tabchmas_dao = tabchmas_dao;
        this.user_dao = user_dao;
        this.tblRole_dao = tblRole_dao;
        this.tbluser_role_dao = tbluser_role_dao;
    }

    @Override
    public User_Entity getByUserId(String UserId) {
        return user_dao.getByUserId(UserId);
    }

    @Override
    public User_Entity getByUserName(String UserName) {
        return user_dao.getByUserName(UserName);
    }

    @Override
    public List<HashMap<String, String>> getBranchCodeList() {
        return tabchmas_dao.getBranchCodeList();
    }

    @Override
    public boolean findOne(String UserId) {
        Integer count = user_dao.countOne(UserId);
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    public Integer countAll() {
        return user_dao.countAll();
    }

    @Override
    public boolean save(User_Entity entity) {
        return user_dao.save(entity);
    }

    @Override
    public boolean update(User_Entity entity) {
        return user_dao.update(entity);
    }

    @Override
    public List<User_Entity> findAll() {
        return user_dao.findAll();
    }

    @Override
    public boolean delete(String userid) {
        return user_dao.delete(userid);
    }

    @Override
    public List<String> getRoleInfoList() {
        return tblRole_dao.getRoleId();
    }

    @Override
    public List<String> getUserRoleInfoList(String UserId) {
        return tbluser_role_dao.getUserRole(UserId);
    }

    @Override
    public List<String> getAllRoleInfoList() {
        return tblRole_dao.getRoleId();
    }

    @Override
    public boolean checkUserRole(String UserId, String RoleId) {
        Integer count = tbluser_role_dao.checkUserRole(UserId, RoleId);
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    public boolean saveUserRole(String UserId, String RoleId) {
        return tbluser_role_dao.saveUserRole(UserId, RoleId);
    }

    @Override
    public boolean updateUserRole(String UserId, String RoleId) {
        return tbluser_role_dao.updateUserRole(UserId, RoleId);
    }

    @Override
    public boolean deleteUserRole(String UserId, String RoleId) {
        return tbluser_role_dao.deleteUserRole(UserId, RoleId);
    }

    @Override
    public boolean deleteRoleByUserId(String UserId) {
        return tbluser_role_dao.deleteRoleByUserId(UserId);
    }

    @Override
    public List<User_Entity> findByBranch(String branch) {
        return user_dao.findByBranch(branch);
    }

    @Override
    public List<User_Entity> findByUserId(String UserId) {
        return user_dao.findByUserId(UserId);
    }

    @Override
    public List<User_Entity> findByUserIdandBranch(String UserId, String branch) {
        return user_dao.findByUserIdandBranch(UserId, branch);
    }

    @Override
    public Integer countByBranch(String branch) {
        return user_dao.countByBranch(branch);
    }

    @Override
    public Integer countByUserId(String UserId) {
        return user_dao.countByUserId(UserId);
    }

    @Override
    public Integer countByUserIdandBranch(String UserId, String branch) {
        return user_dao.countByUserIdandBranch(UserId, branch);
    }

    @Override
    public String getMaxId() {
        return user_dao.getMaxId();
    }

    //通过userid获取用户信息
    @Override
    public User_Entity getUserInfo(String userid){
        return user_dao.getInfoByUserid(userid);
    }
}
