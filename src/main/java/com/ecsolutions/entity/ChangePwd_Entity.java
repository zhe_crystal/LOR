package com.ecsolutions.entity;

/**
 * Created by Administrator on 2017/8/11.
 */
public class ChangePwd_Entity {

    private String userID;
    private String OldPassword;
    private String NewPassword;
    private String NewPassword2;

    public String getNewPassword2() {
        return NewPassword2;
    }

    public void setNewPassword2(String newPassword2) {
        NewPassword2 = newPassword2;
    }



    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getOldPassword() {
        return OldPassword;
    }

    public void setOldPassword(String oldPassword) {
        OldPassword = oldPassword;
    }

    public String getNewPassword() {
        return NewPassword;
    }

    public void setNewPassword(String newPassword) {
        NewPassword = newPassword;
    }


}
