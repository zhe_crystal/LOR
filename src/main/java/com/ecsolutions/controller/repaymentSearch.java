package com.ecsolutions.controller;

import com.ecsolutions.entity.repaymentInput_entity;
import com.ecsolutions.service.repaymentInput_Service;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class repaymentSearch {

    @Autowired
    private repaymentInput_Service repaymentinput_service;

    @GetMapping("/repaymentSearch")
    public String getRepaymentInfo() {
        return "repaymentSearch";
    }

    @PostMapping("/repaymentSearch")
    public String postRepaymentInfo() {
        return "repaymentSearch";
    }

    @RequestMapping(value = "/repaymentSearch/Search", method = RequestMethod.GET)
    @ResponseBody
    public String searchResult(@RequestParam String dwndate, @RequestParam String matdate, @RequestParam String custcod, @RequestParam String refno, int limit, int offset) {
        Gson gson = new Gson();
        int count_result = repaymentinput_service.countResultList(dwndate, matdate, custcod, refno);
        PageHelper.startPage(offset, limit);
        List<repaymentInput_entity> reuslt_list = repaymentinput_service.getResultList(dwndate, matdate, custcod, refno);
        String searchResultList = gson.toJson(reuslt_list);
        String errorCode = "0";
        return "{\"rowDatas\":" + searchResultList + ",\"dataLength\":" + count_result + ",\"errCode\":" + errorCode + "}";
    }
}
