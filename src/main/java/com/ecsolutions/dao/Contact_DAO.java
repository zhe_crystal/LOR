package com.ecsolutions.dao;

import com.ecsolutions.entity.Contact_Entity;
import com.ecsolutions.entity.Pledge_Entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/7/24.
 */
@Repository
public interface Contact_DAO {

    // custcode和bpm_no查询出的信息
    @Select("SELECT * FROM contacthistorynew WHERE TRIM(custcode) = TRIM(#{custcode}) AND TRIM(bpm_no) = TRIM(#{bpm_no})")
    @ResultType(Contact_Entity.class)
    List<Contact_Entity> getByCustcodeAndBpm_no(@Param("custcode") String custcode, @Param("bpm_no") String bpm_no);

    // custcode和bpm_no查询出的max id信息
    @Select("SELECT Max(id) FROM contacthistorynew WHERE TRIM(custcode) = TRIM(#{custcode}) AND TRIM(bpm_no) = TRIM(#{bpm_no})")
    @ResultType(Integer.class)
    Integer getIdByCustcodeAndBpm_no(@Param("custcode") String custcode, @Param("bpm_no") String bpm_no);

    //保存联系记录信息
    @Insert({"insert into contacthistorynew(id,custcode,bpm_no,clientname,contactperson,contactreason,remarks,contactdate,contacttime,contactuser,appointbooked,createdby,createdon,modifiedby,modifiedon,clientname_state,contactperson_state,contactreason_state,remarks_state,contactdate_state,contacttime_state,contactuser_state,appointbooked_state,contactlocation,contactlocation_state)","values(#{id,jdbcType=NUMERIC},#{custcode,jdbcType=VARCHAR},#{bpm_no,jdbcType=VARCHAR},#{clientname,jdbcType=VARCHAR},#{contactperson,jdbcType=VARCHAR},#{contactreason,jdbcType=VARCHAR},#{remarks,jdbcType=VARCHAR},#{contactdate,jdbcType=TIMESTAMP},#{contacttime,jdbcType=VARCHAR},#{contactuser,jdbcType=VARCHAR},#{appointbooked,jdbcType=CHAR},#{createdby,jdbcType=VARCHAR},#{createdon,jdbcType=TIMESTAMP},#{modifiedby,jdbcType=VARCHAR},#{modifiedon,jdbcType=TIMESTAMP},#{clientname_state,jdbcType=NUMERIC},#{contactperson_state,jdbcType=NUMERIC},#{contactreason_state,jdbcType=NUMERIC},#{remarks_state,jdbcType=NUMERIC},#{contactdate_state,jdbcType=NUMERIC},#{contacttime_state,jdbcType=NUMERIC},#{contactuser_state,jdbcType=NUMERIC},#{appointbooked_state,jdbcType=VARCHAR},#{contactlocation,jdbcType=VARCHAR},#{contactlocation_state,jdbcType=NUMERIC})"})
    void saveContactInfo(Contact_Entity contact_entity);

    //更新联系记录信息
    @Update("update contacthistorynew set clientname=#{clientname,jdbcType=VARCHAR},contactperson=#{contactperson,jdbcType=VARCHAR},contactreason=#{contactreason,jdbcType=VARCHAR},remarks=#{remarks,jdbcType=VARCHAR},contactdate=#{contactdate,jdbcType=TIMESTAMP},contacttime=#{contacttime,jdbcType=VARCHAR},contactuser=#{contactuser,jdbcType=VARCHAR},appointbooked=#{appointbooked,jdbcType=CHAR},createdby=#{createdby,jdbcType=VARCHAR},createdon=#{createdon,jdbcType=TIMESTAMP},modifiedby=#{modifiedby,jdbcType=VARCHAR},modifiedon=#{modifiedon,jdbcType=TIMESTAMP},clientname_state=#{clientname_state,jdbcType=NUMERIC},contactperson_state=#{contactperson_state,jdbcType=NUMERIC},contactreason_state=#{contactreason_state,jdbcType=NUMERIC},remarks_state=#{remarks_state,jdbcType=NUMERIC},contactdate_state=#{contactdate_state,jdbcType=NUMERIC},contacttime_state=#{contacttime_state,jdbcType=NUMERIC},contactuser_state=#{contactuser_state,jdbcType=NUMERIC},appointbooked_state=#{appointbooked_state,jdbcType=VARCHAR},contactlocation=#{contactlocation,jdbcType=VARCHAR},contactlocation_state=#{contactlocation_state,jdbcType=NUMERIC} WHERE id=#{id}")
    void updateContactInfo(Contact_Entity contact_entity);

    //删除联系记录信息
    @Delete("delete from contacthistorynew where TRIM(id)=TRIM(#{id})")
    void deleteContactInfo(@Param("id") Integer id);

}