function Format(value){
    if (value != null) {
        var date = new Date(parseInt(value));
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        return date.getFullYear()  + month  + currentDate;
    }
}

$(function() {

    //当其采取后台分页的时候，在这里把所需要的参数传给后台
    function queryParams(params) { // 配置参数
        var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            // limit : params.limit, // 页面大小
            // offset : params.offset, // 页码
            // sortname : params.sort, // 排序列名
            // sortorder : params.order// 排序规则
            loanid : $("#loanid").val()
        };
        return temp;
    }

    $("#routeTable").bootstrapTable({
        method : "post",// 请求方式（*）
        url: "/Route_EntityList",// 请求后台的URL（*）
        queryParams : queryParams,// 传递参数（*)
        uniqueId : "puid",// 唯一标识
        sidePagination : 'client',// 分页方式：client客户端分页，server服务端分页（*）
        sortName : "puid",// 排序字段
        sortOrder : "asc",// 排序方式
        pageList : [ 5, 10, 15, 20, 25, 30 ],// 可供选择的每页的行数（*）
        pageNumber : 1,// 初始化加载第一页，默认第一页
        pagination : true,// 是否显示分页（*）
        pageSize : 12,// 每页的记录行数（*）
        showColumns : true, // 是否显示所有的列
        toolbar : "#toolbar",// 工具按钮用哪个容器
        clickToSelect : true,// 是否启用点击选中行
        cache : false,// 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped : false,// 是否显示行间隔色
        showRefresh : true,// 是否显示刷新按钮
        showToggle : true,// 是否显示详细视图和列表视图的切换按钮
        cardView : true,// 是否显示详细视图
        detailView : false,// 是否显示父子表
        paginationLoop : false,// 是否允许循环分页
        paginationPreText : "上一页",// 指定上一页按钮文字，不配置默认
        paginationNextText : "下一页",
        columns : [{
            field: 'puid',
            title: '审批序号'
        },{
            field: 'stepcode',
            title: '审批步骤'
        }, {
            field: 'audittime',
            title: '审批日期',
            formatter:function(value,row,index){
                if (value != null) {
                    var date = new Date(parseInt(value));
                    var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
                    var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                    var dateNew = date.getFullYear()  + month  + currentDate;
                    return dateNew;
                }
            }
        }, {
            field : 'workitemtitle',
            title : '审批意见'
        }, {
            field : 'audituserbankcode',
            title : '审批银行'
        }, {
            field : 'actionusername',
            title : '参与审批人'
        }, {
            field : 'result',
            title : '审批结果'
        }],

        onClickRow : function(row) {

        }
    });

});