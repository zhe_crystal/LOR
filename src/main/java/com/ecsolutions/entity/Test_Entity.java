package com.ecsolutions.entity;

/**
 * Created by Administrator on 2017/7/31.
 */
public class Test_Entity {
    private String lineamt;
    private String prodtype;
    private String schetype;
    private String bankid;
    private String custcod;
    private String loanref;
    private String drawdate;
    private String drawccy;
    private String drawamt;
    private String tenor;
    private String lineno;
    private String avliamt;
    private String schemarate;
    private String ovdayinrate;
    private String marginamt;
    private String chgtype;
    private String chgdate;
    private String chgamt;
    private String paymentmethod;
    private String ratetype;
    private String firstpaydate;
    private String songkong;
    private String payper1;
    private String jiaoyu;
    private String luru;
    private String lineccy;
    private String rate;
    private String spread;
    private String fixflag;
    private String payfreq;
    private String payper;
    private String duedate;

    public String getLineamt() {
        return lineamt;
    }

    public void setLineamt(String lineamt) {
        this.lineamt = lineamt;
    }

    public String getProdtype() {
        return prodtype;
    }

    public void setProdtype(String prodtype) {
        this.prodtype = prodtype;
    }

    public String getSchetype() {
        return schetype;
    }

    public void setSchetype(String schetype) {
        this.schetype = schetype;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getCustcod() {
        return custcod;
    }

    public void setCustcod(String custcod) {
        this.custcod = custcod;
    }

    public String getLoanref() {
        return loanref;
    }

    public void setLoanref(String loanref) {
        this.loanref = loanref;
    }

    public String getDrawdate() {
        return drawdate;
    }

    public void setDrawdate(String drawdate) {
        this.drawdate = drawdate;
    }

    public String getDrawccy() {
        return drawccy;
    }

    public void setDrawccy(String drawccy) {
        this.drawccy = drawccy;
    }

    public String getDrawamt() {
        return drawamt;
    }

    public void setDrawamt(String drawamt) {
        this.drawamt = drawamt;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getLineno() {
        return lineno;
    }

    public void setLineno(String lineno) {
        this.lineno = lineno;
    }

    public String getAvliamt() {
        return avliamt;
    }

    public void setAvliamt(String avliamt) {
        this.avliamt = avliamt;
    }

    public String getSchemarate() {
        return schemarate;
    }

    public void setSchemarate(String schemarate) {
        this.schemarate = schemarate;
    }

    public String getOvdayinrate() {
        return ovdayinrate;
    }

    public void setOvdayinrate(String ovdayinrate) {
        this.ovdayinrate = ovdayinrate;
    }

    public String getMarginamt() {
        return marginamt;
    }

    public void setMarginamt(String marginamt) {
        this.marginamt = marginamt;
    }

    public String getChgtype() {
        return chgtype;
    }

    public void setChgtype(String chgtype) {
        this.chgtype = chgtype;
    }

    public String getChgdate() {
        return chgdate;
    }

    public void setChgdate(String chgdate) {
        this.chgdate = chgdate;
    }

    public String getChgamt() {
        return chgamt;
    }

    public void setChgamt(String chgamt) {
        this.chgamt = chgamt;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getRatetype() {
        return ratetype;
    }

    public void setRatetype(String ratetype) {
        this.ratetype = ratetype;
    }

    public String getFirstpaydate() {
        return firstpaydate;
    }

    public void setFirstpaydate(String firstpaydate) {
        this.firstpaydate = firstpaydate;
    }

    public String getSongkong() {
        return songkong;
    }

    public void setSongkong(String songkong) {
        this.songkong = songkong;
    }

    public String getPayper1() {
        return payper1;
    }

    public void setPayper1(String payper1) {
        this.payper1 = payper1;
    }

    public String getJiaoyu() {
        return jiaoyu;
    }

    public void setJiaoyu(String jiaoyu) {
        this.jiaoyu = jiaoyu;
    }

    public String getLuru() {
        return luru;
    }

    public void setLuru(String luru) {
        this.luru = luru;
    }

    public String getLineccy() {
        return lineccy;
    }

    public void setLineccy(String lineccy) {
        this.lineccy = lineccy;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getSpread() {
        return spread;
    }

    public void setSpread(String spread) {
        this.spread = spread;
    }

    public String getFixflag() {
        return fixflag;
    }

    public void setFixflag(String fixflag) {
        this.fixflag = fixflag;
    }

    public String getPayfreq() {
        return payfreq;
    }

    public void setPayfreq(String payfreq) {
        this.payfreq = payfreq;
    }

    public String getPayper() {
        return payper;
    }

    public void setPayper(String payper) {
        this.payper = payper;
    }

    public String getDuedate() {
        return duedate;
    }

    public void setDuedate(String duedate) {
        this.duedate = duedate;
    }
}