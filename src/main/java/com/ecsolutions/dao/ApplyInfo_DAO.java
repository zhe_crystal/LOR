package com.ecsolutions.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/8/8.
 */
@Repository
public interface ApplyInfo_DAO {
    @Select("SELECT actiondate,actionuser,stepcode FROM LoanRouteHistory WHERE loanid= #{loanid} order by puid")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getWorkflow( String loanid);



    @Select("SELECT branch FROM Tblusers WHERE userid= #{userId}")
    String getBranch( String userId);

    @Insert("insert into LoanRouteHistory(puid,loanid,actiondate,actionuser,stepcode,result,workitemid) values(" +
            "(select (nvl(max(puid),0))+1 from LoanRouteHistory where loanid =#{arg1})," +
            "#{arg1}," +
            "(select sysdate from dual)," +
            "#{arg0}," +
            "#{arg2}," +
            "#{arg3,jdbcType=VARCHAR}," +
            "(select workitemid from LoanRouteHistory where loanid =#{arg1} and rownum=1)" +
            ")")
    void applyInfoSubmit(String userid,String loanid,String stepcode,String result);

    //用loanid获取workitemid/taskid
    @Select("select workitemid from LoanRouteHistory where loanid =#{loanid} and rownum=1")
    String getWorkitemIdbyLoanid(String loanid);

    //开启工作流时在LoanRouteHistory插入第一条数据
    @Insert("insert into LoanRouteHistory(puid,loanid,actiondate,actionuser,stepcode,result,workitemid,workflowstarttime) values(" +
            "(select (nvl(max(puid),0))+1 from LoanRouteHistory where loanid =#{arg1})," +
            "#{arg1}," +
            "(select sysdate from dual)," +
            "#{arg0}," +
            "#{arg2}," +
            "#{arg3,jdbcType=VARCHAR}," +
            "#{arg4,jdbcType=VARCHAR}," +
            "(select sysdate from dual)" +
            ")")
    @ResultType(Boolean.class)
    boolean applyInfoInsert(String userid, String loanid, String stepcode, String result, String taskid);

    //check insert
    @Select("SELECT count(*) FROM tbl_workflow WHERE bpm_no = #{bpm_no}")
    @ResultType(String.class)
    String checkinsert(String bpm_no);

    //开启工作流时将工作流记录保存在tbl_workflow表中
    @Insert("insert into tbl_workflow(bpm_no,processid,taskname,initiatorname,templatename,state,createtime) values(" +
    "#{arg0},#{arg1},#{arg2},#{arg3},#{arg4},#{arg5},#{arg6}")
    void saveWorkflowList(String bpm_no,String processid,String taskname,String initiatorname,String templatename,String state,String createtime);

    //更新工作流信息
    @Update("update tbl_workflow set bpm_no=#{arg0},processid=#{arg1},taskname=#{arg2},initiatorname=#{arg3},templatename=#{arg4},state=#{arg5},createtime=#{arg6} where bpm_no=#{arg0} ")
    void updateWorkflowList(String bpm_no,String processid,String taskname,String initiatorname,String templatename,String state,String createtime);

//    @Select("Select workitemid from LoanRouteHistory where puid='1' and loanid=#{arg0,jdbcType=VARCHAR} and actionuser=#{arg1,jdbcType=VARCHAR}")
@Select("Select workitemid from LoanRouteHistory where puid='1' and loanid=#{arg0,jdbcType=VARCHAR}")
@ResultType(String.class)
    String getTaskId(String BPM_NO);
//    boolean applyInfoStart(String userid, String loanid, String stepcode, String result, String taskid);

    //在开启工作流后，申请信息-状况 申贷会审批 合规部审批 三个流程对LoanRouteHistory的新增  区别是少一个taskid参数
    @Insert("insert into LoanRouteHistory(puid,loanid,actiondate,actionuser,stepcode,result,workitemid,workflowstarttime) values(" +
            "(select (nvl(max(puid),0))+1 from LoanRouteHistory where loanid =#{arg1})," +
            "#{arg1}," +
            "(select sysdate from dual)," +
            "#{arg0}," +
            "#{arg2}," +
            "#{arg3,jdbcType=VARCHAR}," +
            "(select workitemid from LoanRouteHistory where loanid =#{arg1} and rownum=1)," +
            "(select sysdate from dual)" +
            ")")
    @ResultType(Boolean.class)
    boolean applyInfoFollow(String userid, String loanid, String stepcode, String result);
}
