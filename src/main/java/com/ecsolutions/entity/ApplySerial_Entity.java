package com.ecsolutions.entity;

public class ApplySerial_Entity {
    private String custCode;
    private String BPM_NO;
    private String startDate;
    private String lastSubmitDate;
    private String status;
    private String counterOfferCount;
    private String OOUser;
    private String BankCode;
    private String ProcessId;

    public String getProcessId() {
        return ProcessId;
    }

    public void setProcessId(String processId) {
        ProcessId = processId;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getBPM_NO() {
        return BPM_NO;
    }

    public void setBPM_NO(String BPM_NO) {
        this.BPM_NO = BPM_NO;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getLastSubmitDate() {
        return lastSubmitDate;
    }

    public void setLastSubmitDate(String lastSubmitDate) {
        this.lastSubmitDate = lastSubmitDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCounterOfferCount() {
        return counterOfferCount;
    }

    public void setCounterOfferCount(String counterOfferCount) {
        this.counterOfferCount = counterOfferCount;
    }

    public String getOOUser() {
        return OOUser;
    }

    public void setOOUser(String OOUser) {
        this.OOUser = OOUser;
    }

    public String getBankCode() {
        return BankCode;
    }

    public void setBankCode(String bankCode) {
        BankCode = bankCode;
    }
}
