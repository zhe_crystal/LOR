package com.ecsolutions.entity;

/**
 * Created by Administrator on 2017/8/21.
 */
public class Loan {
    private String userid;
    private String loanref;
    private String refno;
    private String custcod;

    private String cufulnm;
    private String wkcuad1;
    private String wkcuad2;
    private String wkcuad3;
    private String branch;
    private String bankent;
    private String relref;
    private String crline;
    private String lntype;
    private String contrano;
    private String contradt;
    private String menlei;
    private String dalei;
    private String zhonglei;
    private String xiaolei;
    private String country;


    private String accoff;
    private String accofname;
    private String schtyp;
    private String dwndate;
    private String lnccy;
    private String lnamt;
    private String rpymth;
    private String instno;
    private String instamt;
    private String inttype;
    private String ratetyp;
    private String rpyfeq;
    private String rpyper;
    private String wpaydat;
    private String matdate;
    private String rate;
    private String spread;
    private String percenta;
    private String wtotrat;
    private String wtotort;
    private String intcalf;
    private String collfalg;
    private String dueonhl;
    private String stlord1;
    private String stlord2;
    private String stlord3;
    private String stlord4;
    private String stlord5;
    private String latchgs;
    private String penchgs;
    private String grace;
    private String margin;
    private String lgamt;
    private String intbase;
    private String fun;
    private String wtotins;
    private String prtrps  ;
    private String fliuser;
    private String flidate;
    private String flitime;
    private String auser;
    private String adate;
    private String func;
    private String flstscd;

    /*SOA调用传递参数*/
    private String QREFNO; //贷款编号
    private String QDATE; //交易日期
    private String QFUN; //交易功能
    private String QPRO; //交易产品

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
    public String getAccofname() {
        return accofname;
    }

    public void setAccofname(String accofname) {
        this.accofname = accofname;
    }
    public String getLoanref() {
        return loanref;
    }

    public void setLoanref(String loanref) {
        this.loanref = loanref;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getCustcod() {
        return custcod;
    }

    public void setCustcod(String custcod) {
        this.custcod = custcod;
    }

    public String getCufulnm() {
        return cufulnm;
    }

    public void setCufulnm(String cufulnm) {
        this.cufulnm = cufulnm;
    }

    public String getWkcuad1() {
        return wkcuad1;
    }

    public void setWkcuad1(String wkcuad1) {
        this.wkcuad1 = wkcuad1;
    }

    public String getWkcuad2() {
        return wkcuad2;
    }

    public void setWkcuad2(String wkcuad2) {
        this.wkcuad2 = wkcuad2;
    }

    public String getWkcuad3() {
        return wkcuad3;
    }

    public void setWkcuad3(String wkcuad3) {
        this.wkcuad3 = wkcuad3;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBankent() {
        return bankent;
    }

    public void setBankent(String bankent) {
        this.bankent = bankent;
    }

    public String getRelref() {
        return relref;
    }

    public void setRelref(String relref) {
        this.relref = relref;
    }

    public String getCrline() {
        return crline;
    }

    public void setCrline(String crline) {
        this.crline = crline;
    }

    public String getLntype() {
        return lntype;
    }

    public void setLntype(String lntype) {
        this.lntype = lntype;
    }

    public String getContrano() {
        return contrano;
    }

    public void setContrano(String contrano) {
        this.contrano = contrano;
    }

    public String getContradt() {
        return contradt;
    }

    public void setContradt(String contradt) {
        this.contradt = contradt;
    }

    public String getMenlei() {
        return menlei;
    }

    public void setMenlei(String menlei) {
        this.menlei = menlei;
    }

    public String getDalei() {
        return dalei;
    }

    public void setDalei(String dalei) {
        this.dalei = dalei;
    }

    public String getZhonglei() {
        return zhonglei;
    }

    public void setZhonglei(String zhonglei) {
        this.zhonglei = zhonglei;
    }

    public String getXiaolei() {
        return xiaolei;
    }

    public void setXiaolei(String xiaolei) {
        this.xiaolei = xiaolei;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAccoff() {
        return accoff;
    }

    public void setAccoff(String accoff) {
        this.accoff = accoff;
    }

    public String getSchtyp() {
        return schtyp;
    }

    public void setSchtyp(String schtyp) {
        this.schtyp = schtyp;
    }

    public String getDwndate() {
        return dwndate;
    }

    public void setDwndate(String dwndate) {
        this.dwndate = dwndate;
    }

    public String getLnccy() {
        return lnccy;
    }

    public void setLnccy(String lnccy) {
        this.lnccy = lnccy;
    }

    public String getLnamt() {
        return lnamt;
    }

    public void setLnamt(String lnamt) {
        this.lnamt = lnamt;
    }

    public String getRpymth() {
        return rpymth;
    }

    public void setRpymth(String rpymth) {
        this.rpymth = rpymth;
    }

    public String getInstno() {
        return instno;
    }

    public void setInstno(String instno) {
        this.instno = instno;
    }

    public String getInstamt() {
        return instamt;
    }

    public void setInstamt(String instamt) {
        this.instamt = instamt;
    }

    public String getInttype() {
        return inttype;
    }

    public void setInttype(String inttype) {
        this.inttype = inttype;
    }

    public String getRatetyp() {
        return ratetyp;
    }

    public void setRatetyp(String ratetyp) {
        this.ratetyp = ratetyp;
    }

    public String getRpyfeq() {
        return rpyfeq;
    }

    public void setRpyfeq(String rpyfeq) {
        this.rpyfeq = rpyfeq;
    }

    public String getRpyper() {
        return rpyper;
    }

    public void setRpyper(String rpyper) {
        this.rpyper = rpyper;
    }

    public String getWpaydat() {
        return wpaydat;
    }

    public void setWpaydat(String wpaydat) {
        this.wpaydat = wpaydat;
    }

    public String getMatdate() {
        return matdate;
    }

    public void setMatdate(String matdate) {
        this.matdate = matdate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getSpread() {
        return spread;
    }

    public void setSpread(String spread) {
        this.spread = spread;
    }

    public String getPercenta() {
        return percenta;
    }

    public void setPercenta(String percenta) {
        this.percenta = percenta;
    }

    public String getWtotrat() {
        return wtotrat;
    }

    public void setWtotrat(String wtotrat) {
        this.wtotrat = wtotrat;
    }

    public String getWtotort() {
        return wtotort;
    }

    public void setWtotort(String wtotort) {
        this.wtotort = wtotort;
    }

    public String getIntcalf() {
        return intcalf;
    }

    public void setIntcalf(String intcalf) {
        this.intcalf = intcalf;
    }

    public String getCollfalg() {
        return collfalg;
    }

    public void setCollfalg(String collfalg) {
        this.collfalg = collfalg;
    }

    public String getDueonhl() {
        return dueonhl;
    }

    public void setDueonhl(String dueonhl) {
        this.dueonhl = dueonhl;
    }

    public String getStlord1() {
        return stlord1;
    }

    public void setStlord1(String stlord1) {
        this.stlord1 = stlord1;
    }

    public String getStlord2() {
        return stlord2;
    }

    public void setStlord2(String stlord2) {
        this.stlord2 = stlord2;
    }

    public String getStlord3() {
        return stlord3;
    }

    public void setStlord3(String stlord3) {
        this.stlord3 = stlord3;
    }

    public String getStlord4() {
        return stlord4;
    }

    public void setStlord4(String stlord4) {
        this.stlord4 = stlord4;
    }

    public String getStlord5() {
        return stlord5;
    }

    public void setStlord5(String stlord5) {
        this.stlord5 = stlord5;
    }

    public String getLatchgs() {
        return latchgs;
    }

    public void setLatchgs(String latchgs) {
        this.latchgs = latchgs;
    }

    public String getPenchgs() {
        return penchgs;
    }

    public void setPenchgs(String penchgs) {
        this.penchgs = penchgs;
    }

    public String getGrace() {
        return grace;
    }

    public void setGrace(String grace) {
        this.grace = grace;
    }

    public String getMargin() {
        return margin;
    }

    public void setMargin(String margin) {
        this.margin = margin;
    }

    public String getLgamt() {
        return lgamt;
    }

    public void setLgamt(String lgamt) {
        this.lgamt = lgamt;
    }

    public String getIntbase() {
        return intbase;
    }

    public void setIntbase(String intbase) {
        this.intbase = intbase;
    }

    public String getFun() {
        return fun;
    }

    public void setFun(String fun) {
        this.fun = fun;
    }

    public String getWtotins() {
        return wtotins;
    }

    public void setWtotins(String wtotins) {
        this.wtotins = wtotins;
    }

    public String getPrtrps() {
        return prtrps;
    }

    public void setPrtrps(String prtrps) {
        this.prtrps = prtrps;
    }

    public String getFliuser() {
        return fliuser;
    }

    public void setFliuser(String fliuser) {
        this.fliuser = fliuser;
    }

    public String getFlidate() {
        return flidate;
    }

    public void setFlidate(String flidate) {
        this.flidate = flidate;
    }

    public String getFlitime() {
        return flitime;
    }

    public void setFlitime(String flitime) {
        this.flitime = flitime;
    }

    public String getAuser() {
        return auser;
    }

    public void setAuser(String auser) {
        this.auser = auser;
    }

    public String getAdate() {
        return adate;
    }

    public void setAdate(String adate) {
        this.adate = adate;
    }

    public String getFunc() {
        return func;
    }

    public void setFunc(String func) {
        this.func = func;
    }

    public String getFlstscd() {
        return flstscd;
    }

    public void setFlstscd(String flstscd) {
        this.flstscd = flstscd;
    }

    public String getQREFNO() {
        return QREFNO;
    }

    public void setQREFNO(String QREFNO) {
        this.QREFNO = QREFNO;
    }

    public String getQDATE() {
        return QDATE;
    }

    public void setQDATE(String QDATE) {
        this.QDATE = QDATE;
    }

    public String getQFUN() {
        return QFUN;
    }

    public void setQFUN(String QFUN) {
        this.QFUN = QFUN;
    }

    public String getQPRO() {
        return QPRO;
    }

    public void setQPRO(String QPRO) {
        this.QPRO = QPRO;
    }
}
