package com.ecsolutions.dao;

import org.apache.ibatis.annotations.*;
import com.ecsolutions.entity.coCtrRelaInfo_entity;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/7/28.
 */
@Repository
public interface coCtrRelaInfo_DAO {
    // select idType
    @Select("SELECT distinct (CODTYPE||CSTCOD2) AS CODE,CSTLDES FROM TACSTMAS WHERE CODTYPE = 'CC'")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getIdType();

    // 根据custcode来显示企业控制关系信息
    @Select("SELECT * FROM loanRelationCorpCtrInfo WHERE custcode = #{custcode}")
    @ResultType(coCtrRelaInfo_entity.class)
    List<coCtrRelaInfo_entity> getCustcode(String custcode);

    //根据enterprisename删除企业控制信息
    @Delete("delete from loanRelationCorpCtrInfo where enterpriseName=#{enterpriseName}")
    void delete(String enterpriseName);

    @Insert("insert into loanRelationCorpCtrInfo(id,custcode,relationtype,enterpriseName,institutioncreditcode,registrationnumbertype,registrationNumber,organizationCode) values(#{id,jdbcType=VARCHAR},#{custcode,jdbcType=VARCHAR},#{relationtype,jdbcType=VARCHAR},#{enterpriseName,jdbcType=VARCHAR},"
            +"#{institutioncreditcode,jdbcType=VARCHAR},#{registrationnumbertype,jdbcType=VARCHAR},#{registrationNumber,jdbcType=VARCHAR},#{organizationCode,jdbcType=VARCHAR})")
    void insert(coCtrRelaInfo_entity coctrrelainfo_entity);

    @Update("update loanRelationCorpCtrInfo set relationtype=#{relationtype,jdbcType=VARCHAR},enterpriseName=#{enterpriseName,jdbcType=VARCHAR},institutioncreditcode=#{institutioncreditcode,jdbcType=VARCHAR},"
            +"registrationnumbertype=#{registrationnumbertype,jdbcType=VARCHAR},registrationNumber=#{registrationNumber,jdbcType=VARCHAR},organizationCode=#{organizationCode,jdbcType=VARCHAR} "
            +"where id=#{id}")
    void update(coCtrRelaInfo_entity coctrrelainfo_entity);
}
