package com.ecsolutions.Validators;

import com.ecsolutions.entity.ApplicationInfo_Entity;
import com.ecsolutions.entity.Contact_Entity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Administrator on 2017/7/26.
 * 抵押信息校验
 */
@Component
public class LoanApplicationValidate implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return ApplicationInfo_Entity.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ApplicationInfo_Entity applicationInfo_entity = (ApplicationInfo_Entity) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "applicationdate", "amountrequired", "不能为空");
    }
}
