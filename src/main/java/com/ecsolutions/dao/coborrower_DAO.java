package com.ecsolutions.dao;

import com.ecsolutions.entity.coborrower_entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/7/28.
 */
@Repository
public interface coborrower_DAO {

    // select idType
    @Select("SELECT distinct CODTYPE||CSTCOD2 AS CODE,CSTLDES FROM TACSTMAS WHERE CODTYPE = 'ID'")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getIdType();

    //check line_no
    @Select("SELECT count(*) FROM Loanapplicantfacilityinfo WHERE custcod = #{custcode} and lineno = #{line_no}")
    @ResultType(String.class)
    String checklineno(@Param("line_no") String line_no, @Param("custcode") String custcode);

    //check insert
    @Select("SELECT count(*) FROM LoanJointApplicant_New WHERE loanjointapplicantid = #{loanjointapplicantid}")
    @ResultType(String.class)
    String checkinsert(@Param("loanjointapplicantid") String loanjointapplicantid);

    // select perCoBorrower by custcode
    @Select("SELECT * FROM LoanJointApplicant_New WHERE TRIM(custcode) = TRIM(#{custcode}) and personalflag = '1'")
    @ResultType(coborrower_entity.class)
    List<coborrower_entity> getPerCoBorrowerInfo(String custcode);

    // select comCoBorrower by custcode
    @Select("SELECT * FROM LoanJointApplicant_New WHERE TRIM(custcode) = TRIM(#{custcode}) and personalflag = '2'")
    @ResultType(coborrower_entity.class)
    List<coborrower_entity> getComCoBorrowerInfo(String custcode);

    //根据loanjointapplicantid删除共同借款人（个人）信息
    @Delete("delete from LoanJointApplicant_New where loanjointapplicantid=#{loanjointapplicantid}")
    void delete_per(String loanjointapplicantid);

    //根据loanjointapplicantid删除共同借款人（企业）信息
    @Delete("delete from LoanJointApplicant_New where loanjointapplicantid=#{loanjointapplicantid}")
    void delete_com(String loanjointapplicantid);

    @Insert("insert into LoanJointApplicant_New(custcode,loanjointapplicantid,OwnerCustCode,LoanID,personalflag,line_no,name,firstName,lastName,pidtypeid,pIdNo,ethnic,CreateDateTime,remark) "
            + "values(#{custcode,jdbcType=VARCHAR},#{loanjointapplicantid,jdbcType=VARCHAR},#{OwnerCustCode,jdbcType=VARCHAR},#{LoanID,jdbcType=VARCHAR},#{personalflag,jdbcType=VARCHAR},"
            +"#{line_no,jdbcType=VARCHAR},#{name,jdbcType=VARCHAR},#{firstName,jdbcType=VARCHAR},#{lastName,jdbcType=VARCHAR},#{pidtypeid,jdbcType=VARCHAR},#{pIdNo,jdbcType=VARCHAR},#{ethnic,jdbcType=VARCHAR},"
            +"to_date(#{CreateDateTime,jdbcType=VARCHAR},'yyyymmdd'),#{remark,jdbcType=VARCHAR})")
    void insert(coborrower_entity coborrower_entity);

    @Update("update LoanJointApplicant_New set loanjointapplicantid=#{loanjointapplicantid},custcode=#{custcode,jdbcType=VARCHAR},OwnerCustCode=#{OwnerCustCode,jdbcType=VARCHAR},LoanID=#{LoanID,jdbcType=VARCHAR},"
            +"personalflag=#{personalflag,jdbcType=VARCHAR},line_no=#{line_no,jdbcType=VARCHAR},name=#{name,jdbcType=VARCHAR},firstName=#{firstName,jdbcType=VARCHAR},"
            +"lastName=#{lastName,jdbcType=VARCHAR},pidtypeid=#{pidtypeid,jdbcType=VARCHAR},pIdNo=#{pIdNo,jdbcType=VARCHAR},ethnic=#{ethnic,jdbcType=VARCHAR},CreateDateTime=#{CreateDateTime,jdbcType=VARCHAR},remark=#{remark,jdbcType=VARCHAR} "
            +"where loanjointapplicantid=#{loanjointapplicantid}")
    void update(coborrower_entity coborrower_entity);
}
