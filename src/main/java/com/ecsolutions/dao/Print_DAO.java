package com.ecsolutions.dao;

import com.ecsolutions.entity.Print_entity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/8/4.
 */
@Repository
public interface Print_DAO {
    @Select("select bpm_no,lineno,loanref,to_char(anndate,'YYYYMMDD') as anndate,dircdesc from utilizationdetail WHERE bpm_no =#{bpm_no}")
    @ResultType(Print_entity.class)
    Print_entity getUtilizationdetailInfo(String bpm_no);

    @Select("select to_char(startdate,'YYYYMMDD') as startdate from Applyserial WHERE bpm_no =#{bpm_no}")
    @ResultType(String.class)
    String getStrdate(String bpm_no);

    @Select("select custcod,lastname,CommunicationAddress,Businessscope,lnofemp,legalrepresentativename from LoanApplicant WHERE custcod =#{custcod}")
    @ResultType(Print_entity.class)
    Print_entity getLoanApplicantInfo(String custcod);

    @Select("select osamt,to_char(expirydate,'YYYY/MM/DD HH:mm:ss') as expirydate from Loanapplicantfacilityinfo WHERE custcod =#{custcod} and lineno =#{lineno} order by lineno")
    @ResultType(Print_entity.class)
    List<Print_entity> getLoanapplicantfacilityinfo(@Param("custcod")String custcod, @Param("lineno")String lineno);

    @Select("select lineno,loanref,to_char(drawdate,'YYYY/MM/DD HH:mm:ss') as drawdate,drawamt,schemarate from Utilizationdetail")
    @ResultType(Print_entity.class)
    List<Print_entity> getLoanListInfo();
}
