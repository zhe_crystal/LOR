package com.ecsolutions.Validators;

import com.ecsolutions.entity.Tbluser_Properties_Entity;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class Tbluser_Properties_Validate implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Tbluser_Properties_Entity.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Tbluser_Properties_Entity entity = (Tbluser_Properties_Entity) o;
    }
}
