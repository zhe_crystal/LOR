package com.ecsolutions.dao;

import com.ecsolutions.entity.finance_body_entity;
import com.ecsolutions.entity.finance_header_entity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ecs on 2017/8/18.
 */
@Repository
public interface finance_DAO {
    // select by custcode
    @Select("SELECT * FROM FinacialReport WHERE TRIM(custcode) = TRIM(#{custcode}) order by typeid")
    @ResultType(finance_header_entity.class)
    List<finance_header_entity> getHeaderInfo(String custcode);

    // select by reportno
    @Select("SELECT * FROM FinacialReportDetail WHERE TRIM(reportno) = TRIM(#{reportno}) order by seqno")
    @ResultType(finance_body_entity.class)
    List<finance_body_entity> getBodyInfo(String reportno);

    // select by custcode
    @Select("SELECT reportno FROM FinacialReport WHERE TRIM(custcode) = TRIM(#{custcode}) order by typeid")
    @ResultType(List.class)
    List<String> getReportnoList(String custcode);

    //check insert
    @Select("SELECT count(*) FROM FinacialReport WHERE custcode = #{custcode}")
    @ResultType(String.class)
    String checkinsert(@Param("custcode") String custcode);

    @Insert("insert into FinacialReport(custcode,typeid,reportno,reportdate,reportenddate,auditoffice,audittime,auditofficer) values(#{custcode,jdbcType=VARCHAR},#{typeid,jdbcType=NUMERIC},#{reportno,jdbcType=VARCHAR},"
            +"#{reportdate,jdbcType=TIMESTAMP},#{reportenddate,jdbcType=TIMESTAMP},#{auditoffice,jdbcType=VARCHAR},#{audittime,jdbcType=TIMESTAMP},#{auditofficer,jdbcType=VARCHAR})")
    void insert_header(finance_header_entity finance_header);

    @Insert("insert into FinacialReportDetail(reportno,seqno,typeid,origvalue,endvalue) values(#{reportno,jdbcType=VARCHAR},#{seqno,jdbcType=NUMERIC},#{typeid,jdbcType=VARCHAR},#{origvalue,jdbcType=NUMERIC},"
            +"#{endvalue,jdbcType=NUMERIC})")
    void insert_body(finance_body_entity finance_body);
}
