package com.ecsolutions.controller;

import com.ecsolutions.entity.CreditInfo_Entity;
import com.ecsolutions.service.CreditInfo_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Administrator on 2017/7/28.
 */
@Controller
public class CreditInfo_Controller {

    @Autowired
    private CreditInfo_Service creditInfo_service;

    public CreditInfo_Service getcreditInfo_service() {
        return creditInfo_service;
    }

    @GetMapping("/creditInfo")
    public String getApplyPage(Model model,@RequestParam("custcode") String custcode) {
        model.addAttribute("creditInfo_entity_insert", new CreditInfo_Entity());
        model.addAttribute("creditInfo_entity_update", new CreditInfo_Entity());
        model.addAttribute("creditInfo_info", creditInfo_service.getPopupInfo(custcode));
        model.addAttribute("custcode",custcode);
        return "Credit/creditInfo";
    }

    @PostMapping("/creditInfo")
    public String saveCreditEntity(@ModelAttribute CreditInfo_Entity creditInfo_entity,Model model,@RequestParam("custcode") String custcode) {
        creditInfo_service.saveCreditInfoEntity(custcode,creditInfo_entity);

        model.addAttribute("creditInfo_entity_insert", new CreditInfo_Entity());
        model.addAttribute("creditInfo_entity_update", new CreditInfo_Entity());
        model.addAttribute("creditInfo_info", creditInfo_service.getPopupInfo(custcode));
        model.addAttribute("custcode",custcode);
        return "Credit/creditInfo";
    }

    @PostMapping("/creditInfoUpdate")
    public String UpdateCreditEntity(@ModelAttribute CreditInfo_Entity creditInfo_entity_update,Model model,@RequestParam("custcode") String custcode) {
        creditInfo_service.updateCreditInfoEntity(creditInfo_entity_update);

        model.addAttribute("creditInfo_entity_insert", new CreditInfo_Entity());
        model.addAttribute("creditInfo_entity_update", new CreditInfo_Entity());
        model.addAttribute("creditInfo_info", creditInfo_service.getPopupInfo(custcode));
        model.addAttribute("custcode",custcode);
        return "Credit/creditInfo";
    }


    @ResponseBody
    @RequestMapping(value = "/creditInfoDelete",method = RequestMethod.POST)
    public String DeleteCreditEntity(String deleteId) {
        creditInfo_service.deleteCreditInfoEntity(deleteId);

        return "success";
    }


}
