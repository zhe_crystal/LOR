package com.ecsolutions.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/8/28.
 */

@Repository
public interface Workflow_DAO {
    @Select("select USGID from TAUEGMAP  where TRIM(USEID) = TRIM(#{user_id})")
    @ResultType(String.class)
    List<String> getRoleByUser(@Param("user_id") String userId);
}
