package com.ecsolutions.dao;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/8/21.
 */
@Repository
public interface tbluser_role_DAO {
    @Select("Select ROLEID from tbluser_role Where USERID=#{UserId,jdbcType=VARCHAR}")
    @ResultType(String.class)
    List<String> getUserRole(String UserId);

    @Select("Select COUNT(*) from tbluser_role Where USERID=" + "#{param1,jdbcType=VARCHAR}" + " AND ROLEID=" + "#{param2,jdbcType=VARCHAR}")
    @ResultType(Integer.class)
    Integer checkUserRole(String UserId, String RoleId);

    @Select("Select COUNT(*) from tbluser_role Where ROLEID= #{RoleId,jdbcType=VARCHAR}")
    @ResultType(Integer.class)
    Integer countUserinOneRole(String RoleId);

    @Select("Select UserID from tbluser_role Where ROLEID= #{RoleId,jdbcType=VARCHAR}")
    @ResultType(HashMap.class)
    List<HashMap<String, String>> getUserinOneRole(String RoleId);

    @Insert("Insert into tbluser_role (UserId,RoleId) VALUES(" +
            "#{param1,jdbcType=VARCHAR}," +
            "#{param2,jdbcType=VARCHAR}" +
            ")")
    @ResultType(Boolean.class)
    boolean saveUserRole(String UserId, String RoleId);

    @Update("Update tbluser_role " +
            "SET " +
            "RoleId=" + "#{param2,jdbcType=VARCHAR}" +
            "Where UserId=" + "#{param1,jdbcType=VARCHAR}")
    @ResultType(Boolean.class)
    boolean updateUserRole(String UserId, String RoleId);

    @Delete("Delete from tbluser_role Where UserId=#{param1,jdbcType=VARCHAR} AND RoleId=#{param2,jdbcType=VARCHAR}")
    @ResultType(Boolean.class)
    boolean deleteUserRole(String UserId, String RoleId);

    @Delete("Delete from tbluser_role Where UserId=#{UserId,jdbcType=VARCHAR}")
    @ResultType(Boolean.class)
    boolean deleteRoleByUserId(String UserId);
}
