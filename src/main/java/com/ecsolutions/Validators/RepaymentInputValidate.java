package com.ecsolutions.Validators;

import com.ecsolutions.dao.repaymentInput_DAO;
import com.ecsolutions.entity.repaymentInput_entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by tim on 2017/7/4.
 */

@Component
public class RepaymentInputValidate implements Validator {
    private repaymentInput_DAO repaymentInput_dao;

    @Autowired
    public RepaymentInputValidate(repaymentInput_DAO repaymentInput_dao) {
        this.repaymentInput_dao = repaymentInput_dao;
    }

    @Override
    public void validate(Object target, Errors errors) {
        repaymentInput_entity repaymentInputForm = (repaymentInput_entity) target;
        String refno = repaymentInputForm.getRefno();
        String osbamt = repaymentInput_dao.getOsbamt(refno);
        String prinamt = repaymentInputForm.getPrinamt();
        String intamt = repaymentInputForm.getIntamt();
        String odintamt = repaymentInputForm.getOdintamt();
        String odadjamt = repaymentInputForm.getOdadjamt();
        String compint = repaymentInputForm.getCompint();
        String latechg = repaymentInputForm.getLatechg();
        String lchgadj = repaymentInputForm.getLchgadj();
        String payamt = repaymentInputForm.getPayamt();
        Boolean margflag = repaymentInputForm.isMargflag();
        if (osbamt == null){
            osbamt = "0";
        }
        if (!(odintamt.isEmpty()) && !(odadjamt.isEmpty())){
            if ( Double.valueOf(odintamt) + Double.valueOf(odadjamt) < 0 ) {
                errors.rejectValue("odintamt", "ddd", "逾期利息与调整金额的和不能小于0.");
                errors.rejectValue("odadjamt", "ddd", "逾期利息与调整金额的和不能小于0.");
            }
        }
        if (!(latechg.isEmpty()) && !(lchgadj.isEmpty())){
            if ( Double.valueOf(latechg) + Double.valueOf(lchgadj) < 0 ) {
                errors.rejectValue("latechg", "ddd", "逾期罚息与调整金额的和不能小于0.");
                errors.rejectValue("lchgadj", "ddd", "逾期罚息与调整金额的和不能小于0.");
            }
        }
        if (!(payamt.isEmpty()) && !(prinamt.isEmpty()) && !(intamt.isEmpty()) && !(odintamt.isEmpty()) && !(odadjamt.isEmpty()) && !(compint.isEmpty()) && !(latechg.isEmpty()) && !(lchgadj.isEmpty())) {
            if (Double.valueOf(payamt) >( Double.valueOf(prinamt) + Double.valueOf(intamt) + Double.valueOf(odintamt) + Double.valueOf(odadjamt) + Double.valueOf(compint) + Double.valueOf(latechg) + Double.valueOf(lchgadj) )) {
                errors.rejectValue("payamt", "ddd", "还款金额超出了您所需偿还的金额.");
            }
        }
        if (margflag){
            if ((!(osbamt.isEmpty())) && !(payamt.isEmpty())){
                if (Double.valueOf(osbamt) < Double.valueOf(payamt)){
                    errors.rejectValue("margflag", "ddd", "保证金不够缴清本次费用.");
                }
            }
        }
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return repaymentInput_entity.class.isAssignableFrom(clazz);
    }

}
