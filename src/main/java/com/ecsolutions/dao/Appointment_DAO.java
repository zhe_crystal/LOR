package com.ecsolutions.dao;

import com.ecsolutions.entity.Appointment_Entity;
import com.ecsolutions.entity.Contact_Entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/7/24.
 */
@Repository
public interface Appointment_DAO {

    // custcode和bpm_no查询出的信息
    @Select("SELECT * FROM appointmenthistorynew WHERE TRIM(custcode) = TRIM(#{custcode}) AND TRIM(bpm_no) = TRIM(#{bpm_no})")
    @ResultType(Appointment_Entity.class)
    List<Appointment_Entity> getByCustcodeAndBpm_no(@Param("custcode") String custcode, @Param("bpm_no") String bpm_no);

    // custcode和bpm_no查询出的max id信息
    @Select("SELECT Max(id) FROM appointmenthistorynew WHERE TRIM(custcode) = TRIM(#{custcode}) AND TRIM(bpm_no) = TRIM(#{bpm_no})")
    @ResultType(Integer.class)
    Integer getIdByCustcodeAndBpm_no(@Param("custcode") String custcode, @Param("bpm_no") String bpm_no);

    //保存预约记录信息
    @Insert({"insert into appointmenthistorynew(id,custcode,bpm_no,contactid,appointclient,appointdate,appointtime,appointplace,appointuser,remarks,createdby,createdon,modifiedon,modifiedby,appointclient_state,appointdate_state,appointtime_state,appointplace_state,appointuser_state,remarks_state)","values(#{id,jdbcType=NUMERIC},#{custcode,jdbcType=VARCHAR},#{bpm_no,jdbcType=VARCHAR},#{contactid,jdbcType=NUMERIC},#{appointclient,jdbcType=VARCHAR},#{appointdate,jdbcType=TIMESTAMP},#{appointtime,jdbcType=TIMESTAMP},#{appointplace,jdbcType=VARCHAR},#{appointuser,jdbcType=VARCHAR},#{remarks,jdbcType=VARCHAR},#{createdby,jdbcType=VARCHAR},#{createdon,jdbcType=TIMESTAMP},#{modifiedon,jdbcType=TIMESTAMP},#{modifiedby,jdbcType=VARCHAR},#{appointclient_state,jdbcType=NUMERIC},#{appointdate_state,jdbcType=NUMERIC},#{appointtime_state,jdbcType=NUMERIC},#{appointplace_state,jdbcType=NUMERIC},#{appointuser_state,jdbcType=NUMERIC},#{remarks_state,jdbcType=NUMERIC})"})
    void saveAppointmentInfo(Appointment_Entity appointment_entity);

    //更新预约记录信息
    @Update("update appointmenthistorynew set contactid=#{contactid,jdbcType=NUMERIC},appointclient=#{appointclient,jdbcType=VARCHAR},appointdate=#{appointdate,jdbcType=TIMESTAMP},appointtime=#{appointtime,jdbcType=TIMESTAMP},appointplace=#{appointplace,jdbcType=VARCHAR},appointuser=#{appointuser,jdbcType=VARCHAR},remarks=#{remarks,jdbcType=VARCHAR},createdby=#{createdby,jdbcType=VARCHAR},createdon=#{createdon,jdbcType=TIMESTAMP},modifiedon=#{modifiedon,jdbcType=TIMESTAMP},modifiedby=#{modifiedby,jdbcType=VARCHAR},appointclient_state=#{appointclient_state,jdbcType=NUMERIC},appointdate_state=#{appointdate_state,jdbcType=NUMERIC},appointtime_state=#{appointtime_state,jdbcType=NUMERIC},appointplace_state=#{appointplace_state,jdbcType=NUMERIC},appointuser_state=#{appointuser_state,jdbcType=NUMERIC},remarks_state=#{remarks_state,jdbcType=NUMERIC} WHERE id=#{id}")
    void updateAppointmentInfo(Appointment_Entity appointment_entity);

    //删除预约记录信息
    @Delete("delete from appointmenthistorynew where TRIM(id)=TRIM(#{id})")
    void deleteAppointmentInfo(@Param("id") Integer id);

}