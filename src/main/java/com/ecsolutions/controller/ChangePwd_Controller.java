package com.ecsolutions.controller;

import com.ecsolutions.entity.ChangePwd_Entity;
import com.ecsolutions.service.ChangePwd_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by Administrator on 2017/7/31.
 */
@Controller
public class ChangePwd_Controller {

    @Autowired
    private ChangePwd_Service changePwd_service;
    public ChangePwd_Service getChangePwd_Service() {
        return changePwd_service;
    }

    @Autowired
    @Qualifier("changePwdValidate")
    private Validator validator;

    @InitBinder
    public void initBinder(DataBinder binder){
        binder.setValidator(validator);
    }
    public ChangePwd_Controller(){}


    @GetMapping("/changePwd")
    public String getChangePwdPage(Model model, @RequestParam("userid") String userid) {
        model.addAttribute("userid",userid);

        ChangePwd_Entity changePwd_entity=new ChangePwd_Entity();
        changePwd_entity.setUserID(userid);
        model.addAttribute("changePwd_entity",changePwd_entity);

        System.out.println("getmapping"+userid);
        return "changePwd";
    }



    @PostMapping("/changePwd")
    public String savaPwdPage(@Valid @ModelAttribute("changePwd_entity") ChangePwd_Entity changePwd_entity, BindingResult result, Model model,@RequestParam("userid") String userid ) {
//        ,
        System.out.println("post call");

        if(!result.hasErrors()){
           System.out.println("没有错误");
            changePwd_service.changePwd(changePwd_entity);
            changePwd_entity = new ChangePwd_Entity();
            changePwd_entity.setUserID(userid);
            model.addAttribute("userid",userid);
            model.addAttribute("changePwd_entity",changePwd_entity);
            return "changePwd";
        }
        changePwd_entity.setUserID(userid);
        model.addAttribute("userid",userid);

        return "changePwd";
    }

}
