package com.ecsolutions.controller;

import com.ecsolutions.common.ObjectHelp;
import com.ecsolutions.entity.Loan;
import com.ecsolutions.service.LoanService;
import com.ecsolutions.soaClient.TransferClient;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Administrator on 2017/8/24.
 */
@Controller

public class LoanReviewController {
    @Autowired
    private LoanService loanService;
    public LoanService getLoanService() {
        return loanService;
    }

    @Autowired
    private TransferClient client;

    @GetMapping("/loanReview")
    public String getLoanReviewPage(@RequestParam("userid") String userid, @RequestParam("bpm_no") String bpm_no, Model model) {
        model.addAttribute("popup_Loan",loanService.getLoan(bpm_no));
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loanReview";
    }

    @PostMapping("/loanReview")
    public String PostLoanReviewPage(@RequestParam("userid") String userid, @RequestParam("bpm_no") String bpm_no, Model model) {
        loanService.saveLoanReview(bpm_no, userid);
        JSONObject MakeLoansJSON = new JSONObject();
        MakeLoansJSON.put("QREFNO","IN10170000310"); //HKBINMAS中的refno
        MakeLoansJSON.put("QDATE","20170806"); //8位交易日期
        MakeLoansJSON.put("QFUN","HKBINCAA");
        MakeLoansJSON.put("QPRO","HKBINMAS");
        System.out.println("call TransferClient.transfer(MakeLoansTx)");
        String prspcod = "";
        try {
            String mess = ObjectHelp.InitTransferData("MakeLoansTx", MakeLoansJSON);
//        System.out.println("****customer_code:"+client.transfer(mess));
            String recieveMsg = client.transfer(mess);
            if (recieveMsg != null || recieveMsg.length() > 0) {
                recieveMsg = recieveMsg.replace("{", "").replace("}", "");
                String arr[] = recieveMsg.split(",");
                int prspcodIndex = arr[0].lastIndexOf(":");
                prspcod = arr[1].substring(prspcodIndex + 1).replace("\"", "");
//                Map<String,String> map=new HashMap<String,String>();
//                map.put("pcustcd",pcustcd);
//                map.put("prspcod",prspcod);
                if (prspcod.equals("0000000")) {
                    System.out.println("get LoanInformation success");
//                    model.addAttribute("generateCustcd",map);
                } else {
                    System.out.println("Cannot get LoanInformation From Soa");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("popup_Loan",loanService.getLoan(bpm_no));
        model.addAttribute("userid",userid);
        model.addAttribute("bpm_no",bpm_no);
        return "Loan/loanReview";
    }
//    @GetMapping("/loan")
//    public String getLoanPage(@RequestParam("userid") String userid,@RequestParam("bpm_no") String bpm_no,Model model) {
//        Loan popup_Loan=loanService.getPopupInfo(userid,bpm_no);
//        popup_Loan=loanService.caulMatdate(popup_Loan);
//        model.addAttribute("popup_Loan",popup_Loan);
//        model.addAttribute("userid",userid);
//        model.addAttribute("bpm_no",bpm_no);
//        return "Loan/loan";
//    }
}
