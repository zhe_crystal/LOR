package com.ecsolutions.Validators;

import com.ecsolutions.entity.Prenda_Entity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2017/7/26.
 * 质押信息校验
 */
@Component
public class PrendaValidate implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Prenda_Entity.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Prenda_Entity prenda_entity = (Prenda_Entity)target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "line_no", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mode1", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amountccy", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "valueccy", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "amount", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "value", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankpledgename", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankpledgeownertype", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankpledgeeffectivedate", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankpledgelastdate", "amountrequired", "不能为空");

        if(prenda_entity.getAmount()!=null || prenda_entity.getValue()!=null){
            if(!String.valueOf(prenda_entity.getAmount()).matches("([1-9]\\d*(\\.\\d*[1-9])?)|(0\\.\\d*[1-9])")){
                errors.rejectValue("amount","ddd","金额必须大于等于0");
            }
            if(!String.valueOf(prenda_entity.getValue()).matches("([1-9]\\d*(\\.\\d*[1-9])?)|(0\\.\\d*[1-9])")){
                errors.rejectValue("value","ddd","金额必须大于等于0");
            }
        }
        //质押生效日不能大于到期日
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
        Date bankpledgeeffectivedate = prenda_entity.getBankpledgeeffectivedate();//质押生效日
        Date bankpledgelastdate = prenda_entity.getBankpledgelastdate();//质押到期日
        if(bankpledgeeffectivedate!=null && bankpledgelastdate!=null){
            Integer bankpledgeeffectivedatenew = Integer.valueOf(sdf.format(bankpledgeeffectivedate));
            Integer bankpledgelastdatenew = Integer.valueOf(sdf.format(bankpledgelastdate));
            if(bankpledgeeffectivedatenew>bankpledgelastdatenew){
                errors.rejectValue("bankpledgelastdate","ddd","质押生效日不能晚于到期日");
            }
        }
        //应收账款质押
        if(prenda_entity.getMode1().equals("1")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accrecproveperson", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accrecowner", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accrecamountccy", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accrecamount", "amountrequired", "不能为空");

            //金额必须大于等于0
            if(prenda_entity.getAccrecamount()!=null){
                if(!String.valueOf(prenda_entity.getAccrecamount()).matches("([1-9]\\d*(\\.\\d*[1-9])?)|(0\\.\\d*[1-9])")){
                    errors.rejectValue("accrecamount","ddd","金额必须大于等于0");
                }
            }
        }

        //银行账户质押
        if(prenda_entity.getMode1().equals("2")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankaccountname", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankaccounttype", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankname", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankaccount", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankaccountccy", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankaccountbalance", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankisfreezed", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankaccountistrust", "amountrequired", "不能为空");

            //金额必须大于等于0
            if(prenda_entity.getBankaccountbalance()!=null){
                if(!String.valueOf(prenda_entity.getBankaccountbalance()).matches("([1-9]\\d*(\\.\\d*[1-9])?)|(0\\.\\d*[1-9])")){
                    errors.rejectValue("bankaccountbalance","ddd","金额必须大于等于0");
                }
            }
        }
        //汽车合格证质押
        if(prenda_entity.getMode1().equals("3")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "carproveperson", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "carproduction", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "carno", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "carbrand", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "carchassisno", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "carprice", "amountrequired", "不能为空");

            //金额必须大于等于0
            if(prenda_entity.getCarprice()!=null){
                if(!String.valueOf(prenda_entity.getCarprice()).matches("([1-9]\\d*(\\.\\d*[1-9])?)|(0\\.\\d*[1-9])")){
                    errors.rejectValue("carprice","ddd","金额必须大于等于0");
                }
            }
        }
        //存单(折)质押
        if(prenda_entity.getMode1().equals("4")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cdname", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cdtype", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cdno", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cdaccount", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cdbank", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cdisselfbank", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cdamount", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cdisstoppay", "amountrequired", "不能为空");

            //金额必须大于等于0
            if(prenda_entity.getCdamount()!=null){
                if(!String.valueOf(prenda_entity.getCdamount()).matches("([1-9]\\d*(\\.\\d*[1-9])?)|(0\\.\\d*[1-9])")){
                    errors.rejectValue("cdamount","ddd","金额必须大于等于0");
                }
            }
        }
        //权利凭证质押
        if(prenda_entity.getMode1().equals("5")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cerorg", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cercustomer", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cerno", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ceraccount", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cereffectivedate", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cerlastdate", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ceramount", "amountrequired", "不能为空");

            //金额必须大于等于0
            if(prenda_entity.getCeramount()!=null){
                if(!prenda_entity.getCeramount().matches("([1-9]\\d*(\\.\\d*[1-9])?)|(0\\.\\d*[1-9])")){
                    errors.rejectValue("ceramount","ddd","金额必须大于等于0");
                }
            }
            //凭证生效日不能大于到期日
            Date cereffectivedate = prenda_entity.getCereffectivedate();//凭证生效日
            Date cerlastdate = prenda_entity.getCerlastdate();//凭证到期日
            if(cereffectivedate!=null && cerlastdate!=null){
                Integer cereffectivedatenew = Integer.valueOf(sdf.format(cereffectivedate));
                Integer cerlastdatenew = Integer.valueOf(sdf.format(cerlastdate));
                if(cereffectivedatenew>cerlastdatenew){
                    errors.rejectValue("cerlastdate","ddd","凭证生效日不能晚于到期日");
                }
            }
        }
        //收费权质押
        if(prenda_entity.getMode1().equals("6")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "chargeorg", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "chargeproject", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "chargelicenceno", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "chargeregorg", "amountrequired", "不能为空");
        }
        //知识产权质押
        if(prenda_entity.getMode1().equals("7")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "intprotype", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "intproownername", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "intproproveorg", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "intpropatentnumber", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "intproregorg", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "intproregdate", "amountrequired", "不能为空");
        }
        //提货单质押
        if(prenda_entity.getMode1().equals("8")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ladorg", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ladno", "amountrequired", "不能为空");
        }
        //票据质押
        if(prenda_entity.getMode1().equals("9")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "notestype", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "notesno", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "notesamount", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "notesbank", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "notessigneddate", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "noteslastdate", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "notesaddress", "amountrequired", "不能为空");

            //金额必须大于等于0
            if(prenda_entity.getNotesamount()!=null){
                if(!String.valueOf(prenda_entity.getNotesamount()).matches("([1-9]\\d*(\\.\\d*[1-9])?)|(0\\.\\d*[1-9])")){
                    errors.rejectValue("notesamount","ddd","金额必须大于等于0");
                }
            }
            //票据签发日期不能晚于票据到期日期
            Date notessigneddate = prenda_entity.getNotessigneddate();//票据签发日期
            Date noteslastdate = prenda_entity.getNoteslastdate();//票据到期日期
            if(notessigneddate!=null && noteslastdate!=null){
                Integer notessigneddatenew = Integer.valueOf(sdf.format(notessigneddate));
                Integer noteslastdatenew = Integer.valueOf(sdf.format(noteslastdate));
                if(notessigneddatenew>noteslastdatenew){
                    errors.rejectValue("noteslastdate","ddd","票据签发日期不能晚于票据到期日期");
                }
            }
        }
        //股份、股票质押
        if(prenda_entity.getMode1().equals("10")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "stockownername", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "stocknumber", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "stockno", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "stocknature", "amountrequired", "不能为空");
        }
        //仓单质押
        if(prenda_entity.getMode1().equals("11")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrowner", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrorg", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrno", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrname", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrstandard", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrunit", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrnumber", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrdeliverystaff", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wracceptancestaff", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrgoodsvalue", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wrhaveinsurance", "amountrequired", "不能为空");
        }
        //国债质押
        if(prenda_entity.getMode1().equals("12")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_kind", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_id", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_rate", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_deadline", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_amount", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_bank", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_begindate", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_enddate", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nd_address", "amountrequired", "不能为空");

            //国债到期日期不应晚于国债认购日期
            String nd_begindate = prenda_entity.getNd_begindate();//国债认购日期
            String nd_enddate = prenda_entity.getNd_enddate();//国债到期日期
            if(!nd_begindate.equals("") && !nd_enddate.equals("")){
                Integer nd_begindatenew = Integer.valueOf(nd_begindate);
                Integer nd_enddatenew = Integer.valueOf(nd_enddate);
                if(nd_begindatenew>nd_enddatenew){
                    errors.rejectValue("nd_enddate","ddd","国债到期日期不应早于国债认购日期");
                }
            }
        }
        //其他质押
        if(prenda_entity.getMode1().equals("13")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "other_name", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "other_number", "amountrequired", "不能为空");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "other_address", "amountrequired", "不能为空");
        }
    }
}
