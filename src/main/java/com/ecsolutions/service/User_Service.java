package com.ecsolutions.service;

import com.ecsolutions.entity.User_Entity;
import org.apache.catalina.User;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/8/18.
 */
public interface User_Service {

    User_Entity getByUserId(String UserId);

    User_Entity getByUserName(String UserName);

    List<HashMap<String, String>> getBranchCodeList();

    boolean findOne(String UserId);

    Integer countAll();

    boolean save(User_Entity entity);

    boolean update(User_Entity entity);

    List<User_Entity> findAll();

    boolean delete(String userid);

    List<String> getRoleInfoList();

    List<String> getUserRoleInfoList(String UserId);

    List<String> getAllRoleInfoList();

    boolean checkUserRole(String UserId, String RoleId);

    boolean saveUserRole(String UserId, String RoleId);

    boolean updateUserRole(String UserId, String RoleId);

    boolean deleteUserRole(String UserId, String RoleId);

    boolean deleteRoleByUserId(String UserId);

    List<User_Entity> findByBranch(String branch);

    List<User_Entity> findByUserId(String UserId);

    List<User_Entity> findByUserIdandBranch(String UserId, String branch);

    Integer countByBranch(String branch);

    Integer countByUserId(String UserId);

    Integer countByUserIdandBranch(String UserId, String branch);

    String getMaxId();

    User_Entity getUserInfo(String userid); //获取用户信息
}
