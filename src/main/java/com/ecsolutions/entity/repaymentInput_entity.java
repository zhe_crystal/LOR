package com.ecsolutions.entity;

import java.util.Date;

/**
 * Created by ecs on 2017/8/30.
 */
public class repaymentInput_entity {
    private String refno;
    private String custcod;
    private String cusnm;
    private String custnm;
    private String flstscd;
    private String custref;
    private String schetyp;
    private String schedes;
    private String lnccy = "RMB";
    private String lnamt = "80000.00";
    private String lnosccy = "RMB";
    private String lnosamt = "80000.00";
    private String instnof = "1";
    private String instnot = "1";
    private String prinamt = "6470.06";
    private String setord1;
    private String intamt = "423.67";
    private String setord2;
    private String odintamt = "0.00";
    private String setord3;
    private String odadjamt = "0.00";
    private String compint = "0.00";
    private String latechg = "0.00";
    private String setord4;
    private String lchgadj = "0.00";
    private String QSETSEQ = "54321";
    private String overid = "N";
    private boolean margflag;
    private String margflagString;
    private String payamt;
    private String inpdate;
    private String inpuser = "1";
    private String status = "P";

    public String getMargflagString() {
        return margflagString;
    }

    public void setMargflagString(String margflagString) {
        this.margflagString = margflagString;
    }


    public String getOverid() {
        return overid;
    }

    public void setOverid(String overid) {
        this.overid = overid;
    }
    public String getQSETSEQ() {
        return QSETSEQ;
    }

    public void setQSETSEQ(String QSETSEQ) {
        this.QSETSEQ = QSETSEQ;
    }

    public String getPrinamt() {
        return prinamt;
    }

    public void setPrinamt(String prinamt) {
        this.prinamt = prinamt;
    }

    public String getSetord1() {
        return setord1;
    }

    public void setSetord1(String setord1) {
        this.setord1 = setord1;
    }

    public String getIntamt() {
        return intamt;
    }

    public void setIntamt(String intamt) {
        this.intamt = intamt;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getCustcod() {
        return custcod;
    }

    public void setCustcod(String custcod) {
        this.custcod = custcod;
    }

    public String getCusnm() {
        return cusnm;
    }

    public void setCusnm(String cusnm) {
        this.cusnm = cusnm;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getFlstscd() {
        return flstscd;
    }

    public void setFlstscd(String flstscd) {
        this.flstscd = flstscd;
    }

    public String getCustref() {
        return custref;
    }

    public void setCustref(String custref) {
        this.custref = custref;
    }

    public String getSchetyp() {
        return schetyp;
    }

    public void setSchetyp(String schetyp) {
        this.schetyp = schetyp;
    }

    public String getSchedes() {
        return schedes;
    }

    public void setSchedes(String schedes) {
        this.schedes = schedes;
    }

    public String getLnccy() {
        return lnccy;
    }

    public void setLnccy(String lnccy) {
        this.lnccy = lnccy;
    }

    public String getLnamt() {
        return lnamt;
    }

    public void setLnamt(String lnamt) {
        this.lnamt = lnamt;
    }

    public String getLnosccy() {
        return lnosccy;
    }

    public void setLnosccy(String lnosccy) {
        this.lnosccy = lnosccy;
    }

    public String getLnosamt() {
        return lnosamt;
    }

    public void setLnosamt(String lnosamt) {
        this.lnosamt = lnosamt;
    }

    public String getInstnof() {
        return instnof;
    }

    public void setInstnof(String instnof) {
        this.instnof = instnof;
    }

    public String getInstnot() {
        return instnot;
    }

    public void setInstnot(String instnot) {
        this.instnot = instnot;
    }

    public String getSetord2() {
        return setord2;
    }

    public void setSetord2(String setord2) {
        this.setord2 = setord2;
    }

    public String getOdintamt() {
        return odintamt;
    }

    public void setOdintamt(String odintamt) {
        this.odintamt = odintamt;
    }

    public String getSetord3() {
        return setord3;
    }

    public void setSetord3(String setord3) {
        this.setord3 = setord3;
    }

    public String getOdadjamt() {
        return odadjamt;
    }

    public void setOdadjamt(String odadjamt) {
        this.odadjamt = odadjamt;
    }

    public String getCompint() {
        return compint;
    }

    public void setCompint(String compint) {
        this.compint = compint;
    }

    public String getLatechg() {
        return latechg;
    }

    public void setLatechg(String latechg) {
        this.latechg = latechg;
    }

    public String getSetord4() {
        return setord4;
    }

    public void setSetord4(String setord4) {
        this.setord4 = setord4;
    }

    public String getLchgadj() {
        return lchgadj;
    }

    public void setLchgadj(String lchgadj) {
        this.lchgadj = lchgadj;
    }

    public boolean isMargflag() {
        return margflag;
    }

    public void setMargflag(boolean margflag) {
        this.margflag = margflag;
    }

    public String getPayamt() {
        return payamt;
    }

    public void setPayamt(String payamt) {
        this.payamt = payamt;
    }

    public String getInpdate() {
        return inpdate;
    }

    public void setInpdate(String inpdate) {
        this.inpdate = inpdate;
    }

    public String getInpuser() {
        return inpuser;
    }

    public void setInpuser(String inpuser) {
        this.inpuser = inpuser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
