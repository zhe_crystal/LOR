package com.ecsolutions.controller;

import com.ecsolutions.entity.rePerInfo_entity;
import com.ecsolutions.service.rePerInfo_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/6/27.
 */
@Controller
public class rePerInfo_Controller{


    @Autowired
    private rePerInfo_Service reperinfo_service;

    public rePerInfo_Service getReperinfo_service() {
        return reperinfo_service;
    }

    @GetMapping("/rePerInfo")
    public String getApplyPage(@RequestParam String custcode, Model model) {
        rePerInfo_entity reperinfo_entity = new rePerInfo_entity();
        reperinfo_entity.setCustcode(custcode);
        model.addAttribute("reperinfo_entity", reperinfo_entity);
        model.addAttribute("reper_info", reperinfo_service.getPopupInfo());
        return "rePerInformation";
    }

//    @ResponseBody
//    @RequestMapping(value = "/rePerInfo_insertCheck",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
//    public String findAll(@RequestBody Map<String, Object> map){;
//        String custcode = map.get("custcode").toString();
//        String custNo = map.get("custNo").toString();
//
//        return insert_result;
//    }

    @PostMapping("/rePerInfo")
    public String saveApplyInfo(@Valid @ModelAttribute rePerInfo_entity reperinfo_entity,@RequestParam String custcode,@RequestParam String custNo,  Model model) {
        model.addAttribute("reperinfo_entity", reperinfo_entity);
        model.addAttribute("reper_info", reperinfo_service.getPopupInfo());
        String insert_result;
        insert_result = reperinfo_service.insertCheck(custcode,custNo);
        if (insert_result.equals("0")){
            reperinfo_service.saveRePerEntity(reperinfo_entity);
        }else {
            reperinfo_service.updateRePerEntity(reperinfo_entity);
        }
        return "rePerInformation";
    }

    @ResponseBody
    @RequestMapping(value = "/rePerInfo_pIdNoCheck",method = RequestMethod.POST)
    public rePerInfo_entity findByPIdNo(String pIdNo){
        System.out.print(pIdNo);
        rePerInfo_entity result = reperinfo_service.pIdNoCheck(pIdNo);
        return result;
    }

    //根据客户号custcode显示关联方个人信息
    @ResponseBody
    @RequestMapping(value = "/rePerInfo_Search",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public List<rePerInfo_entity> findByCustcode(@RequestBody Map<String, Object> map){
        List<rePerInfo_entity> result = reperinfo_service.rePerInfoSearch(map.get("custcode").toString());
        return result;
    }

//    新增关联方个人信息
    @ResponseBody
    @RequestMapping(value = "/rePerInfo_add",method = RequestMethod.POST)
    public rePerInfo_entity getNewReperinfoForm(){
        rePerInfo_entity reperinfo_entity = new rePerInfo_entity();
        return reperinfo_entity;
    }

    //通过custNo删除关联方个人信息
    @ResponseBody
    @RequestMapping(value = "/deleteReperInfo",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public Map<String,String> deleteByHierarchy(@RequestBody List<String> list){
        Map<String, String> resultMap = new HashMap<>();
        if(list == null){
            resultMap.put("message","删除失败");
        }else {
            for(String custNO: list){
                reperinfo_service.deleteReperInfo(custNO);
            }
            resultMap.put("message","删除成功");
        }
        return resultMap;
    }
}
