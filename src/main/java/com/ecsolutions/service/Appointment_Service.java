package com.ecsolutions.service;

import com.ecsolutions.entity.Appointment_Entity;
import com.ecsolutions.entity.Contact_Entity;

import java.util.List;

/**
 * Created by Administrator on 2017/7/24.
 */
public interface Appointment_Service {

    public List<Appointment_Entity> getAppointmentInfo(String custcode, String bpm_no);

    public Integer getContactId(String custcode, String bpm_no);

    public void saveAppointmentInfo(Appointment_Entity appointment_entity);

    public void updateAppointmentInfo(Appointment_Entity appointment_entity);

    public void deleteAppointmentInfo(Integer id);


}

