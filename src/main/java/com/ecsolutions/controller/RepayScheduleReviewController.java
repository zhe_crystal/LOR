package com.ecsolutions.controller;

import com.ecsolutions.entity.repaymentScheModify_entity;
import com.ecsolutions.entity.repaymentSchedule_entity;
import com.ecsolutions.service.RepayScheModReviewService;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017-09-07.
 */
@Controller
public class RepayScheduleReviewController {
    @Autowired
    private RepayScheModReviewService repayScheModReviewService;

    public RepayScheModReviewService getRepayScheModReviewService() {
        return repayScheModReviewService;
    }

    @GetMapping("/repaymentScheModifyReview")
    public String getLoanInfo(Model model) {
        repaymentScheModify_entity repaymentschemodify_entity = new repaymentScheModify_entity();
        model.addAttribute("repaymentScheModify_entity", repaymentschemodify_entity);
        return "repaymentScheModify/repaymentScheModReview";
    }


    @RequestMapping(value = "/repaymentScheModifyReview/Search", method = RequestMethod.GET)
    @ResponseBody
    public String searchResult(@RequestParam String refno, int limit, int offset) {
        Gson gson = new Gson();

        PageHelper.startPage(offset, limit);
        List<repaymentScheModify_entity> reuslt_list = repayScheModReviewService.getResultList(refno);
        for(repaymentScheModify_entity repaymentschemodify_entity : reuslt_list){
            repaymentschemodify_entity.setIntrate(repayScheModReviewService.getIntrate(repaymentschemodify_entity.getRefno()));
        }
        String searchResultList = gson.toJson(reuslt_list);
        int count_result = reuslt_list.size();
        String errorCode = "0";
        return "{\"rowDatas\":" + searchResultList + ",\"dataLength\":" + count_result + ",\"errCode\":" + errorCode + "}";
    }

    @RequestMapping(value = "/repaymentScheModifyReview/viewLoanDetails",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    @ResponseBody
    public repaymentScheModify_entity findByRefno(@RequestBody Map<String, Object> map) {
        String refno = map.get("refno").toString();
        repaymentScheModify_entity result = repayScheModReviewService.getLoanInfo(refno);
        result.setIntrate(repayScheModReviewService.getIntrate(result.getRefno()));
        result.setStrdate(repayScheModReviewService.getStrdate(result.getRefno(),result.getCurinno()));
        return result;
    }

    @RequestMapping(value = "/repaymentScheduleReviewInfo",method = RequestMethod.GET)
    public String getPay(String refno,Model model){
        repaymentScheModify_entity repaymentschemodify_entity =repayScheModReviewService.getLoanInfo(refno);
        repaymentschemodify_entity.setIntrate(repayScheModReviewService.getIntrate(repaymentschemodify_entity.getRefno()));
        model.addAttribute("repaymentScheModify_entity",repaymentschemodify_entity);
        return "repaymentScheModify/repaymentScheInfoReview";
    }

    /*通过refno查询信息到还款计划表格中*/
    @ResponseBody
    @RequestMapping(value = "/getRepaymentScheduleReviewInfo",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public List<repaymentSchedule_entity> getRepaymentScheduleInfo(@RequestBody Map<String, Object> map ){
        String refno = map.get("refno").toString();
        List<repaymentSchedule_entity> repaymentScheduleList = repayScheModReviewService.getRepaymentScheduleList(refno);
        return repaymentScheduleList;
    }

}
