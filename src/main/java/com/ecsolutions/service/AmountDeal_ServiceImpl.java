package com.ecsolutions.service;

import org.springframework.stereotype.Service;

@Service("AmountDeal_Service")
public class AmountDeal_ServiceImpl implements AmountDeal_Service {
    @Override
    public String dealWithMoneyAmount(String Amount, int digit1, int digit2) {
        StringBuffer result = new StringBuffer("");
        if ((digit1 + digit2 + 1) >= Amount.length())
            if (Amount.contains(".")) {
                int leftNumber = Amount.indexOf(".") - 1;
                int rightNumber = Amount.length() - leftNumber - 1;
                for (int i = 0; i < digit1 - leftNumber; i++) {
                    result.append("0");
                }
                result.append(Amount.replace(".",""));
                for (int i = 0; i < digit2 - rightNumber; i++) {
                    result.append("0");
                }
            }
            else {
                for (int i = 0; i < digit1 - Amount.length(); i++) {
                    result.append("0");
                }
                result.append(Amount);
                for (int i = 0; i < digit2; i++) {
                    result.append("0");
                }
            }
        return result.toString();
    }

    @Override
    public String dealWithInterestRate(String Rate, int digit1, int digit2) {
        return null;
    }
}
