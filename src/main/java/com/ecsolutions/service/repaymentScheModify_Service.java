package com.ecsolutions.service;

import com.ecsolutions.entity.repaymentScheModify_entity;
import com.ecsolutions.entity.repaymentSchedule_entity;

import java.util.List;

/**
 * Created by ecs on 2017/9/4.
 */
public interface repaymentScheModify_Service {
    int countResultList(String refno);
    List<repaymentScheModify_entity> getResultList(String refno);
    String getIntrate(String refno);
    String getStrdate(String refno);
    repaymentScheModify_entity getLoanInfo(String refno);
    List<repaymentSchedule_entity> getRepaymentScheduleList(String refno);
    repaymentScheModify_entity getRepaymentSchedulePage(String refno);
    void saveToAdsinmf4(repaymentSchedule_entity repaymentschedule_entity);
    void saveToHkbppmas(repaymentScheModify_entity repaymentschemodify_entity);
    void saveToHkbpptmp(repaymentScheModify_entity repaymentschemodify_entity,repaymentSchedule_entity repaymentschedule_entity);
    void saveToHkbindtl(repaymentScheModify_entity repaymentschemodify_entity,repaymentSchedule_entity repaymentschedule_entity);
    String exportExcel(List<repaymentSchedule_entity> repaymentScheduleList,repaymentScheModify_entity repaymentschemodify_entity);
}
