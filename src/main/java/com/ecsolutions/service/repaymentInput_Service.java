package com.ecsolutions.service;

import com.ecsolutions.entity.repaymentInput_entity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/8/30.
 */
public interface repaymentInput_Service {
    List<repaymentInput_entity> getResultList(String dwndate,String matdate,String custcod,String refno);
    repaymentInput_entity getRepaymentInfo(String refno);
    void saveRepaymentInfo(repaymentInput_entity repaymentinput_entity);
    void updateRepaymentInfo(repaymentInput_entity repaymentinput_entity);
    int countResultList(String dwndate,String matdate,String custcod,String refno);
    String insertCheck(String refno,String custcod,String instnof,String instnot,String status);
    String QueryBySchetyp(String schetyp);
    String getPayamt(String refno);
}
