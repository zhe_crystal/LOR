package com.ecsolutions.entity;

/**
 * Created by ecs on 2017/8/18.
 */
public class Tbluser_Properties_Entity {
    private String UserId;
    private String PropertiesId;
    private String PropertiesValue;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getPropertiesId() {
        return PropertiesId;
    }

    public void setPropertiesId(String propertiesId) {
        PropertiesId = propertiesId;
    }

    public String getPropertiesValue() {
        return PropertiesValue;
    }

    public void setPropertiesValue(String propertiesValue) {
        PropertiesValue = propertiesValue;
    }
}
