package com.ecsolutions.service;

import com.ecsolutions.entity.monitoring_entity;

import java.util.List;

/**
 * Created by ecs on 2017/10/10.
 */
public interface Monitoring_Service {
    List<monitoring_entity> workflowInfoSearch();
    void deleteWorkflowinfo(String bpm_no);
}
