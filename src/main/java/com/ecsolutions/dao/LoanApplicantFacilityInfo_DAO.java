package com.ecsolutions.dao;

import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanApplicantFacilityInfo_DAO {
    @Update("Update LoanApplicantFacilityInfo SET APPFLAG ='Y' WHERE CUSTCOD=#{CustCode,jdbcType=VARCHAR}")
    @ResultType(Boolean.class)
    boolean updateAppFlag(String CustCode);

    //    @Update("Update LoanApplicantFacilityInfo SET APPFLAG ='Y' WHERE CUSTCOD=#{param1,jdbcType=VARCHAR} and BPM_NO=#{param2,jdbcType=VARCHAR}")
    @Update("Update LoanApplicantFacilityInfo SET APPFLAG ='Y' WHERE CUSTCOD='001' and BPM_NO='001'")
    @ResultType(Boolean.class)
    boolean updateAppFlagWithBPM_NO(String CustCode, String BPM_NO);

    @Update("Update LoanApplicantFacilityInfo SET osamt =#{param1,jdbcType=VARCHAR},holdamt =#{param2,jdbcType=VARCHAR},usedamt =#{param3,jdbcType=VARCHAR} WHERE CUSTCOD=#{param4,jdbcType=VARCHAR} and BPM_NO=#{param5,jdbcType=VARCHAR}")
    @ResultType(Boolean.class)
    boolean updateAmountWithBPM_NO(String osamt, String holdamt, String usedamt, String CustCode, String BPM_NO);
}
