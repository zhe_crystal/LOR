package com.ecsolutions.controller;

import com.ecsolutions.entity.monitoring_entity;
import com.ecsolutions.service.Monitoring_Service;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/31.
 */
@Controller
public class Monitoring_Controller {
    @Autowired
    private Monitoring_Service monitoring_service;

    public Monitoring_Service getMonitoring_service() {
        return monitoring_service;
    }

    @GetMapping("/monitoring")
    public String getApplyPage(Model model) {
        model.addAttribute("monitoring_entity", new monitoring_entity());
        return "monitoring";
    }

    @PostMapping("/monitoring")
    public String postApplyPage(@ModelAttribute monitoring_entity monitoring_entity,Model model) {
        model.addAttribute("monitoring_entity", monitoring_entity);
        return "monitoring";
    }

    //显示工作流信息
    @ResponseBody
    @RequestMapping(value = "/workflowInfo_Search",method = RequestMethod.POST)
    public List<monitoring_entity> findByBpm_no(){
        List<monitoring_entity> result = monitoring_service.workflowInfoSearch();
        return result;
    }

    //通过贷款编号bpm_no删除工作流记录
    @ResponseBody
    @RequestMapping(value = "/deleteWorkflowInfo",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public Map<String,String> deleteByBpm_no(@RequestBody List<String> list){
        Map<String, String> resultMap = new HashMap<>();
        if(list == null){
            resultMap.put("message","删除失败");
        }else {
            for(String bpm_no : list){
                monitoring_service.deleteWorkflowinfo(bpm_no);
            }
            resultMap.put("message","删除成功");
        }
        return resultMap;
    }
}
