package com.ecsolutions.controller;

import com.ecsolutions.entity.Apply_entity;
import com.ecsolutions.entity.Customer_entity;
import com.ecsolutions.service.ApplyCompany_Service;
import com.ecsolutions.service.Customer_Service;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class Apply_Controller {
    private String CustCode = "000001";

    @Autowired
    private Customer_Service customer_service;

    @Autowired
    private ApplyCompany_Service applyCompany_service;

    @GetMapping("/Apply")
    public String getApply() {
        return "ClientInformation/Apply";
    }

    @RequestMapping(value = "/Apply/Search", method = RequestMethod.GET)
    @ResponseBody
    public String SearchInfo(@RequestParam String PersonalOrEnterprise,
                             @RequestParam String startDate,
                             @RequestParam String endDate,
                             @RequestParam String chineseName,
                             @RequestParam String englishName,
                             @RequestParam String licenseNumber,
                             @RequestParam String creditCardNumber, int pageIndex, int pageSize) {
        if (PersonalOrEnterprise.trim().equals("N"))//企业
        {
            Gson gson = new Gson();
            int total = applyCompany_service.countSearchInfoList(PersonalOrEnterprise, startDate, endDate, chineseName, englishName, licenseNumber, creditCardNumber);
            PageHelper.startPage(pageIndex, pageSize);
            List<Apply_entity> list = applyCompany_service.getSearchInfoList(PersonalOrEnterprise, startDate, endDate, chineseName, englishName, licenseNumber, creditCardNumber);
            String historyList = gson.toJson(list);
            String errorCode = "0";
            return "{\"rowDatas\":" + historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}";
        } else if (PersonalOrEnterprise.trim().equals("Y"))//个人
        {
            Gson gson = new Gson();
            int total = customer_service.countSearchInfoList(PersonalOrEnterprise, startDate, endDate, chineseName, englishName, licenseNumber, creditCardNumber);
            PageHelper.startPage(pageIndex, pageSize);
            List<Customer_entity> list = customer_service.getSearchInfoList(PersonalOrEnterprise, startDate, endDate, chineseName, englishName, licenseNumber, creditCardNumber);
            String historyList = gson.toJson(list);
            String errorCode = "0";
            return "{\"rowDatas\":" + historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}";
        } else {
            int total = 0;
            String historyList = "";
            String errorCode = "获取记录失败";
            return "{\"rowDatas\":" + historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}";
        }
    }

}
