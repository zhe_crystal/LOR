package com.ecsolutions.service;

import com.ecsolutions.entity.TblRole_Entity;

import java.util.HashMap;
import java.util.List;

public interface TblRole_Service {
    boolean saveTblRole(TblRole_Entity entity);
    boolean saveTblUserRole(String UserId,String RoleId);
    List<TblRole_Entity> getHistoryList();
    boolean updateTblRole(TblRole_Entity entity);
    boolean deleteTblRole(String roleid);
    Integer countTblRole();
    boolean isRecord(String roleid);
    List<HashMap<String,String>> getAllUsers();
    List<HashMap<String,String>> getUserinOneRole(String roleid);
    List<String> getOneUsersRole(String userid);
    boolean deleteTblUser_Role(String userid, String roleid);
}
