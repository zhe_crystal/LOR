package com.ecsolutions.entity;

/**
 * Created by Administrator on 2017/8/25.
 */
public class Examination {
    private String userid;
    private String businesamoun;
    private String username;
    private String WorkItemID;
    private String custcod;
    private String LoanID;
    private String AuditNo;
    private String AuditTime;
    private String AuditUserBankCode;
    private String WorkItemTitle;
    private String AuditContext;
    private String ActionUserName;
    private String Result;

    public String getWorkItemID() {
        return WorkItemID;
    }

    public void setWorkItemID(String workItemID) {
        WorkItemID = workItemID;
    }

    public String getCustcod() {
        return custcod;
    }

    public void setCustcod(String custcod) {
        this.custcod = custcod;
    }

    public String getLoanID() {
        return LoanID;
    }

    public void setLoanID(String loanID) {
        LoanID = loanID;
    }

    public String getAuditNo() {
        return AuditNo;
    }

    public void setAuditNo(String auditNo) {
        AuditNo = auditNo;
    }

    public String getAuditTime() {
        return AuditTime;
    }

    public void setAuditTime(String auditTime) {
        AuditTime = auditTime;
    }

    public String getAuditUserBankCode() {
        return AuditUserBankCode;
    }

    public void setAuditUserBankCode(String auditUserBankCode) {
        AuditUserBankCode = auditUserBankCode;
    }

    public String getWorkItemTitle() {
        return WorkItemTitle;
    }

    public void setWorkItemTitle(String workItemTitle) {
        WorkItemTitle = workItemTitle;
    }

    public String getAuditContext() {
        return AuditContext;
    }

    public void setAuditContext(String auditContext) {
        AuditContext = auditContext;
    }

    public String getActionUserName() {
        return ActionUserName;
    }

    public void setActionUserName(String actionUserName) {
        ActionUserName = actionUserName;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }





    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBusinesamoun() {
        return businesamoun;
    }

    public void setBusinesamoun(String businesamoun) {
        this.businesamoun = businesamoun;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


}
