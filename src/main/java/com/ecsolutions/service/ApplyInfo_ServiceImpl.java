package com.ecsolutions.service;

import com.ecsolutions.dao.ApplyInfo_DAO;
import com.ecsolutions.entity.ApplyInfo_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/8/8.
 */
@Service("ApplyInfo_Service")
public class ApplyInfo_ServiceImpl implements ApplyInfo_Service {
    private ApplyInfo_DAO applyInfo_dao ;
    @Autowired
    public ApplyInfo_ServiceImpl(ApplyInfo_DAO applyInfo_dao) {
        this.applyInfo_dao= applyInfo_dao;
    }

    @Override
    public ApplyInfo_Entity getPopupInfo(String loanid) {
        ApplyInfo_Entity result = new ApplyInfo_Entity();
        List<HashMap<String,String>> Workflow = applyInfo_dao.getWorkflow(loanid);
        result.setWorkflow(Workflow);
        return result;
    }

    @Override
    public String getBranch (String userid){
        String branch = applyInfo_dao.getBranch(userid);
        return  branch;
    }
    @Override
    public String getWorkitemIdbyLoanid(String loanid){
        return applyInfo_dao.getWorkitemIdbyLoanid(loanid);
    };
    @Override
    public  void applyInfoSubmit(String userid,String loanid,String stepcode,String result){
        applyInfo_dao.applyInfoSubmit( userid, loanid, stepcode, result);
    }


}
