package com.ecsolutions.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface MicroCreditAuditLog_DAO {
    @Select("SELECT NVL(MAX(AuditNo),0)+1 FROM MicroCreditAuditLog")
    @ResultType(String.class)
    String getAuditNo();

    @Insert("Insert into MicroCreditAuditLog (WorkItemID,custcod,LoanID,AuditNo,AuditTime,AuditUserBankCode,WorkItemTitle,AuditContext,ActionUserName,Result) values" +
            "(#{param1,jdbcType=VARCHAR}," +
            "#{param2,jdbcType=VARCHAR}," +
            "#{param3,jdbcType=VARCHAR}," +
            "#{param4,jdbcType=VARCHAR}," +
            "to_date(#{param5,jdbcType=VARCHAR},'yyyy-mm-dd hh24:mi:ss')," +
            "#{param6,jdbcType=VARCHAR}," +
            "#{param7,jdbcType=VARCHAR}," +
            "#{param8,jdbcType=VARCHAR}," +
            "#{param9,jdbcType=VARCHAR}," +
            "#{param10,jdbcType=VARCHAR})")
    @ResultType(Boolean.class)
    boolean insertRecord(String WorkItemID, String custcod, String LoanID, String AuditNo, String AuditTime, String AuditUserBankCode, String WorkItemTitle, String AuditContext, String ActionUserName, String Result);
}
