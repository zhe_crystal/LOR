package com.ecsolutions.dao.SqlProvider;

import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

public class ApplyDAO_Provider {
    public String getSearchInfoList(Map<String, Object> map) {
        String PersonalOrEnterprise = (String) map.get("param1");
        String startDate = (String) map.get("param2");
        String endDate = (String) map.get("param3");
        String chineseName = (String) map.get("param4");
        String englishName = (String) map.get("param5");
        String licenseNumber = (String) map.get("param6");
        String creditCardNumber = (String) map.get("param7");
        return new SQL() {{
            SELECT("*");
            FROM("LoanApplicant");
            WHERE("personalflag=#{param1}");
            if (!startDate.trim().equals("") && startDate != null) {
                WHERE("to_date(applydate,'yyyy-mm-dd')>= to_date(#{param2},'yyyy-mm-dd')");
            }
            if (!endDate.trim().equals("") && endDate != null) {
                WHERE("to_date(applydate,'yyyy-mm-dd')<= to_date(#{param3},'yyyy-mm-dd')");
            }
            if (!chineseName.trim().equals("") && chineseName != null) {
                WHERE("lastname=#{param4}");
            }
            if (!englishName.trim().equals("") && englishName != null) {
                WHERE("firstname=#{param5}");
            }
            if (!licenseNumber.trim().equals("") && licenseNumber != null) {
                if (PersonalOrEnterprise.trim().equals("N"))
                    WHERE("lregno=#{param6}");
                else if (PersonalOrEnterprise.trim().equals("Y"))
                    WHERE("pidno=#{param6}");
            }
            if (!creditCardNumber.trim().equals("") && creditCardNumber != null) {
                WHERE("ethnic=#{param7}");
            }
            ORDER_BY("applydate");
        }}.toString();
    }

    public String countSearchInfoList(Map<String, Object> map) {
        String PersonalOrEnterprise = (String) map.get("param1");
        String startDate = (String) map.get("param2");
        String endDate = (String) map.get("param3");
        String chineseName = (String) map.get("param4");
        String englishName = (String) map.get("param5");
        String licenseNumber = (String) map.get("param6");
        String creditCardNumber = (String) map.get("param7");
        return new SQL() {{
            SELECT("COUNT(*)");
            FROM("LoanApplicant");
            WHERE("personalflag=#{param1}");
            if (!startDate.trim().equals("") && startDate != null) {
                WHERE("to_date(applydate,'yyyy-mm-dd')>= to_date(#{param2},'yyyy-mm-dd')");
            }
            if (!endDate.trim().equals("") && endDate != null) {
                WHERE("to_date(applydate,'yyyy-mm-dd')<= to_date(#{param3},'yyyy-mm-dd')");
            }
            if (!chineseName.trim().equals("") && chineseName != null) {
                WHERE("lastname=#{param4}");
            }
            if (!englishName.trim().equals("") && englishName != null) {
                WHERE("firstname=#{param5}");
            }
            if (!licenseNumber.trim().equals("") && licenseNumber != null) {
                if (PersonalOrEnterprise.trim().equals("N"))
                    WHERE("lregno=#{param6}");
                else if (PersonalOrEnterprise.trim().equals("Y"))
                    WHERE("pidno=#{param6}");
            }
            if (!creditCardNumber.trim().equals("") && creditCardNumber != null) {
                WHERE("ethnic=#{param7}");
            }
            ORDER_BY("applydate");
        }}.toString();
    }
}
