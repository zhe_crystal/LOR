package com.ecsolutions.dao.SqlProvider;

import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * Created by ecs on 2017/8/30.
 */
public class repaymentInput_Provider {
    public String getResultList(Map<String, Object> map) {
        String dwndate = (String) map.get("param1");
        String matdate = (String) map.get("param2");
        String custcod = (String) map.get("param3");
        String refno = (String) map.get("param4");
        return new SQL() {{
            SELECT("* ");
            FROM("hkbinmas ");
//            WHERE("status='P'");
            if (!dwndate.equals("") && dwndate != null) {
                WHERE("dwndate>= to_number(#{param1})");
            }
            if (!matdate.equals("") && matdate != null) {
                WHERE("matdate<= to_number(#{param2})");
            }
            if (!custcod.equals("") && custcod != null) {
                WHERE("custcod=#{param3}");
            }
            if (!refno.equals("") && refno != null) {
                WHERE("TRIM(refno)=#{param4}");
            }
            ORDER_BY("refno");
        }}.toString();
    }

    public String countResultList(Map<String, Object> map) {
        String dwndate = (String) map.get("param1");
        String matdate = (String) map.get("param2");
        String custcod = (String) map.get("param3");
        String refno = (String) map.get("param4");
        return new SQL() {{
            SELECT("COUNT(*) ");
            FROM("hkbinmas ");
//            WHERE("status='P'");
            if (!dwndate.equals("") && dwndate != null) {
                WHERE("dwndate>= to_number(#{param1})");
            }
            if (!matdate.equals("") && matdate != null) {
                WHERE("matdate<= to_number(#{param2})");
            }
            if (!custcod.equals("") && custcod != null) {
                WHERE("custcod=#{param3}");
            }
            if (!refno.equals("") && refno != null) {
                WHERE("TRIM(refno)=#{param4}");
            }
        }}.toString();
    }
}
