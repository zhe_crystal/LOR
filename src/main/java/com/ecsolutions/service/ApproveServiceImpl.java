package com.ecsolutions.service;

import com.ecsolutions.dao.ApproveDao;
import com.ecsolutions.soaClient.WorkFlowServiceManager;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017-10-13.
 */
@Service("Approve_Service")
public class ApproveServiceImpl implements ApproveService{
    @Autowired
    private WorkFlowServiceManager workFlowServiceManager;
    @Autowired
    private ApproveDao approveDao;

    @Override
    //比workflow接口多了一个参数，参数为步骤名称，取值有"提交贷款申请","支行审贷会秘书审批","合规"
    public List<String> getApproveList(String userName, String stepType, String stepState, String bActive,String step) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userName",userName);
        jsonObject.put("stepType",stepType);
        jsonObject.put("stepState",stepState);
        jsonObject.put("bActive",bActive);
        String listStr = workFlowServiceManager.GetWorkItemList(jsonObject.toString());
        JSONArray jsonArray=JSONArray.fromObject(listStr);//取到所有审批流程

        List<String> l = new ArrayList<String>();//返回类型
        for (int i=0;i<jsonArray.size();i++) {
            JSONObject jsObject = (JSONObject) jsonArray.get(i);
            if(jsObject.has("Subject")){
                if(jsObject.get("Subject").equals(step)){
                    String bpm_no = approveDao.getBpmNoByTaskId(jsObject.get("TaskID").toString());
                    if(bpm_no!=null){
                        l.add(bpm_no);
                    }
                }
            }
        }
        return l;
    };
}
