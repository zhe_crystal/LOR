package com.ecsolutions.dao.SqlProvider;

import com.ecsolutions.entity.CreditAmountRecheck_Entity;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

public class CreditAmountRecheck_Provider {
    public String getSearchInfoResultList(Map<String, Object> map) {
        String obkgid = (String) map.get("param1");
        String refnum = (String) map.get("param2");
        String startDate = (String) map.get("param3");
        String endDate = (String) map.get("param4");
        return new SQL() {{
            SELECT("* ");
            FROM("hkbcrmas ");
            WHERE("crsts='P'");
            if (!startDate.equals("") && startDate != null) {
                WHERE("bokdate>= to_number(#{param3})");
            }
            if (!endDate.equals("") && endDate != null) {
                WHERE("bokdate<= to_number(#{param4})");
            }
            if (!obkgid.equals("") && obkgid != null) {
                WHERE("obkgid=#{param1}");
            }
            if (!refnum.equals("") && refnum != null) {
                WHERE("TRIM(refnum)=#{param2}");
            }
            ORDER_BY("refnum");
        }}.toString();
    }

    public String countSearchInfoResultList(Map<String, Object> map) {
        String obkgid = (String) map.get("param1");
        String refnum = (String) map.get("param2");
        String startDate = (String) map.get("param3");
        String endDate = (String) map.get("param4");
        return new SQL() {{
            SELECT("COUNT(*) ");
            FROM("hkbcrmas ");
            WHERE("crsts='P'");
            if (!startDate.equals("") && startDate != null) {
                WHERE("bokdate>= to_number(#{param3})");
            }
            if (!endDate.equals("") && endDate != null) {
                WHERE("bokdate<= to_number(#{param4})");
            }
            if (!obkgid.equals("") && obkgid != null) {
                WHERE("obkgid=#{param1}");
            }
            if (!refnum.equals("") && refnum != null) {
                WHERE("TRIM(refnum)=#{param2}");
            }
        }}.toString();
    }
}
