package com.ecsolutions.dao;

import com.ecsolutions.entity.ApplySerial_Entity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplySerial_DAO {
    @Select("SELECT NVL(MAX(SUBSTR(BPM_NO,6)),'0000000') FROM applyserial")
    @ResultType(String.class)
    String getCustomerNum();

    //从Loanroutehistory表中获取工作流创建时间
    @Select("SELECT workflowstarttime FROM loanroutehistory where loanid = #{bpm_no}")
    @ResultType(String.class)
    String getCreatetimeInfo(String bpm_no);

    @Select("SELECT NVL(MAX(PROCESSID),'000000') FROM applyserial")
    @ResultType(String.class)
    String getPipelineNo();

    @Select("SELECT * FROM APPLYSERIAL WHERE custcode=#{CustCode,jdbcType=VARCHAR} and status='A'")
    @ResultType(ApplySerial_Entity.class)
    ApplySerial_Entity checkIsRecordUncompleted(String CustCode);

    @Insert("Insert into ApplySerial (custcode,bpm_no,startdate,lastsubmitdate,status,counteroffercount,oouser,bankcode,processid) " +
            "values(" +
            "#{custCode,jdbcType=VARCHAR}," +
            "#{BPM_NO,jdbcType=VARCHAR}," +
            "to_date(#{startDate,jdbcType=VARCHAR},'yyyy-MM-dd')," +
            "to_date(#{lastSubmitDate,jdbcType=VARCHAR},'yyyy-MM-dd')," +
            "#{status,jdbcType=VARCHAR}," +
            "#{counterOfferCount,jdbcType=VARCHAR}," +
            "#{OOUser,jdbcType=VARCHAR}," +
            "#{BankCode,jdbcType=VARCHAR}," +
            "#{ProcessId,jdbcType=VARCHAR}" +
            ")")
    @ResultType(Boolean.class)
    boolean insertRecord(ApplySerial_Entity entity);

    @Update("Update ApplySerial Set STATUS='C' WHERE CUSTCODE=#{param1,jdbcType=VARCHAR} and BPM_NO=#{param2,jdbcType=VARCHAR}")
    @ResultType(Boolean.class)
    boolean updateStatus(String CustCode, String BPM_NO);

    @Select("Select custcode from applyserial where BPM_NO =#{BPM_NO,jdbcType=VARCHAR}")
    @ResultType(String.class)
    String getCustcodebyBPM_NO(String BPM_NO);
}
