package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 2017/7/31.
 */
public class Loanpaymentinfo_det_Entity {
    private Integer id;
    private String custcode;
    private String loanref;
    private String duiflag;
    private String cityflag;
    private BigDecimal payamt;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date paydate;
    private String duicustcd;
    private String duibranch;
    private String duiaccno;
    private String duiname;
    private String waibranch;
    private String waibrname;
    private String waiaccno;
    private String wainame;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public String getLoanref() {
        return loanref;
    }

    public void setLoanref(String loanref) {
        this.loanref = loanref;
    }

    public String getDuiflag() {
        return duiflag;
    }

    public void setDuiflag(String duiflag) {
        this.duiflag = duiflag;
    }

    public String getCityflag() {
        return cityflag;
    }

    public void setCityflag(String cityflag) {
        this.cityflag = cityflag;
    }

    public BigDecimal getPayamt() {
        return payamt;
    }

    public void setPayamt(BigDecimal payamt) {
        this.payamt = payamt;
    }

    public Date getPaydate() {
        return paydate;
    }

    public void setPaydate(Date paydate) {
        this.paydate = paydate;
    }

    public String getDuicustcd() {
        return duicustcd;
    }

    public void setDuicustcd(String duicustcd) {
        this.duicustcd = duicustcd;
    }

    public String getDuibranch() {
        return duibranch;
    }

    public void setDuibranch(String duibranch) {
        this.duibranch = duibranch;
    }

    public String getDuiaccno() {
        return duiaccno;
    }

    public void setDuiaccno(String duiaccno) {
        this.duiaccno = duiaccno;
    }

    public String getDuiname() {
        return duiname;
    }

    public void setDuiname(String duiname) {
        this.duiname = duiname;
    }

    public String getWaibranch() {
        return waibranch;
    }

    public void setWaibranch(String waibranch) {
        this.waibranch = waibranch;
    }

    public String getWaibrname() {
        return waibrname;
    }

    public void setWaibrname(String waibrname) {
        this.waibrname = waibrname;
    }

    public String getWaiaccno() {
        return waiaccno;
    }

    public void setWaiaccno(String waiaccno) {
        this.waiaccno = waiaccno;
    }

    public String getWainame() {
        return wainame;
    }

    public void setWainame(String wainame) {
        this.wainame = wainame;
    }
}