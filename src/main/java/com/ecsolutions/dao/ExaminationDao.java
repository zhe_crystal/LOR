package com.ecsolutions.dao;

import com.ecsolutions.entity.Examination;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/8/25.
 */
@Repository
public interface ExaminationDao {
    @Select("SELECT userid,businesamoun,username FROM Tblusers")
    @ResultType(Examination.class)
    List<Examination> getListUsrsInfo();

    @Select("SELECT max(Puid)+1 FROM LoanRouteHistory where loanid =#{loanid}")
    String getPuid(String loanid);

    @Select("select custcode from applyserial where bpm_no=#{loanid} and rownum=1")
    String getCustcod(String loanid);



    @Insert("insert into MicroCreditAuditLog(WorkItemID,custcod,LoanID,AuditNo,AuditTime,AuditUserBankCode,WorkItemTitle,AuditContext,ActionUserName,Result) values(" +
            "(select workitemid from LoanRouteHistory where loanid =#{arg0} and rownum=1)," +
            "(select custcode from applyserial where bpm_no=#{arg0} and rownum=1)," +
            "#{arg0}," +
            "(SELECT max(Puid)+1 FROM LoanRouteHistory where loanid =#{arg0})," +
            "(select sysdate from dual)," +
            "(SELECT branch FROM Tblusers WHERE userid= #{arg1})," +
            "#{arg2.WorkItemTitle}," +
            "#{arg2.AuditContext}," +
            "#{arg2.ActionUserName}," +
            "#{arg2.Result})")
    void InsertMicroCreditAuditLog(String loanid,String userid,Examination examination);


}
