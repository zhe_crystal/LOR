package com.ecsolutions.dao;

import com.ecsolutions.entity.ExcelDownld_Entity;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017/8/17.
 */
@Repository
public interface ExcelDownld_DAO {
    @Select("select drawamt,drawccy,to_char(drawdate,'YYYYMMDD') as drawdate,schemarate,tenor from utilizationdetail WHERE LOANREF =#{LOANREF}")
    @ResultType(ExcelDownld_Entity.class)
    ExcelDownld_Entity getUtilizationdetailInfo(String LOANREF);

    @Select("select instno,to_char(enddate,'YYYYMMDD') as enddate,instamt,prnamt,intamt,osamt from paymentSchedule WHERE LOANREF =#{LOANREF} order by instno")
    @ResultType(ExcelDownld_Entity.class)
    List<ExcelDownld_Entity> getPaymentScheduleInfo(String LOANREF);

    @Select("SELECT Sum(instamt) as sum_instamt,Sum(prnamt) as sum_prnamt,Sum(intamt) as sum_intamt FROM paymentSchedule WHERE LOANREF =#{LOANREF}")
    @ResultType(ExcelDownld_Entity.class)
    ExcelDownld_Entity getSumInfo(String LOANREF);
}
