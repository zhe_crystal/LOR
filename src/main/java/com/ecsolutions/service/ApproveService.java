package com.ecsolutions.service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017-10-13.
 */
public interface ApproveService {
     List<String> getApproveList(String userName,String stepType,String stepState,String bActive,String step);//比workflow接口多了一个参数，参数为步骤名称，取值有"提交贷款申请","支行审贷会秘书审批","合规"
}
