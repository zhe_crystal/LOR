package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Random;

/**
 * Created by ecs on 2017/8/16.
 */
public class liability_entity {
    private Integer id; //new Random().nextInt(100)%100 + 1
    private String custcode = "20000";
    private String loanamount;
    private String balance;
    private String creditorname;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date loandate;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date duedate;
    private String paymentmethod;
    private String installmentamount;
    private String isoverdue;
    private String overdueamount;
    private String memo1;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getLoanamount() {
        return loanamount;
    }

    public void setLoanamount(String loanamount) {
        this.loanamount = loanamount;
    }

    public String getCreditorname() {
        return creditorname;
    }

    public void setCreditorname(String creditorname) {
        this.creditorname = creditorname;
    }

    public Date getLoandate() {
        return loandate;
    }

    public void setLoandate(Date loandate) {
        this.loandate = loandate;
    }

    public Date getDuedate() {
        return duedate;
    }

    public void setDuedate(Date duedate) {
        this.duedate = duedate;
    }

    public String getPaymentmethod() {
        return paymentmethod;
    }

    public void setPaymentmethod(String paymentmethod) {
        this.paymentmethod = paymentmethod;
    }

    public String getInstallmentamount() {
        return installmentamount;
    }

    public void setInstallmentamount(String installmentamount) {
        this.installmentamount = installmentamount;
    }

    public String getIsoverdue() {
        return isoverdue;
    }

    public void setIsoverdue(String isoverdue) {
        this.isoverdue = isoverdue;
    }

    public String getOverdueamount() {
        return overdueamount;
    }

    public void setOverdueamount(String overdueamount) {
        this.overdueamount = overdueamount;
    }

    public String getMemo1() {
        return memo1;
    }

    public void setMemo1(String memo1) {
        this.memo1 = memo1;
    }
}
