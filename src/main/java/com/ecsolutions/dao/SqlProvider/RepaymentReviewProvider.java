package com.ecsolutions.dao.SqlProvider;

import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * Created by Administrator on 2017-09-05.
 */
public class RepaymentReviewProvider {
    public String getResultList(Map<String, Object> map) {
        String dwndate = (String) map.get("param1");
        String matdate = (String) map.get("param2");
        String custcod = (String) map.get("param3");
        String refno = (String) map.get("param4");
        return new SQL() {{
            SELECT("*");
            FROM("paymenttemp ");
            WHERE("status='P'");
            if (!dwndate.equals("") && dwndate != null) {
                WHERE("inpdate>= to_number(#{param1})");
            }
            if (!matdate.equals("") && matdate != null) {
                WHERE("inpdate<= to_number(#{param2})");
            }
            if (!custcod.equals("") && custcod != null) {
                WHERE("custcod=#{param3}");
            }
            if (!refno.equals("") && refno != null) {
                WHERE("TRIM(refno)=#{param4}");
            }
            ORDER_BY("refno");
        }}.toString();
    }
}
