package com.ecsolutions.service;

import com.ecsolutions.entity.Paymenttemp;
import com.ecsolutions.entity.repaymentInput_entity;

import java.util.List;

/**
 * Created by Administrator on 2017-09-05.
 */
public interface RepaymentReviewService {
    List<Paymenttemp> getResultList(String dwndate, String matdate, String custcod, String refno);
    void saveRepaymentReviewInfo(Paymenttemp paymenttemp);
}
