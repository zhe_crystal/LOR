function Format(value){
    if (value != null) {
        var date = new Date(parseInt(value));
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        return date.getFullYear()  + month  + currentDate;
    }
}

$(function() {

    $('#a_saveOne').click(function () {
        var formOne = $('#formOne').serialize();
        var params = decodeURIComponent(formOne,true);
        $.ajax({
            type: "POST",
            url: "/saveLoanapplicationPayInfo",
            data: params,
            async: false,
            success: function(result){
                alert(result);
            },
        });
    });

    $('#a_delete').click(function () {
        var id = $('#id').val();
            $.ajax({
                type: "POST",
                url: "/deleteLoanpaymentInfo",
                data: {id : id},
                async: false,
                success: function(result){
                    $('#LoanpaymentinfoTable').bootstrapTable("refresh",{
                        silent : true
                    });
                    alert(result);
                    $('#a_add').trigger("click");
                }
            });

    });
    //还款计划表导出Excel
    $('#a_derivar').click(function () {
        var loanref = $('#loanref').val();
        var custcod = $('#custcod').val();
        var lineno = $('#lineno').val();
        $.ajax({
            type: "POST",
            url: "/derivarLoanpaymentInfo",
            data: {loanref: loanref, custcod: custcod,lineno: lineno},
            async: false,
            success: function (result) {
                alert(result);
            },
            error: function (result) {
                alert(result);
            }
        });
    });

    //当其采取后台分页的时候，在这里把所需要的参数传给后台
    function queryParams(params) { // 配置参数
        var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            // limit : params.limit, // 页面大小
            // offset : params.offset, // 页码
            // sortname : params.sort, // 排序列名
            // sortorder : params.order// 排序规则
            custcod : $("#custcod").val(),
            loanref : $('#loanref').val()
        };
        return temp;
    }

    $("#payTable").bootstrapTable({
        method : "post",// 请求方式（*）
        url: "/payList",// 请求后台的URL（*）
        queryParams : queryParams,// 传递参数（*)
        uniqueId : "instno",// 唯一标识
        sidePagination : 'client',// 分页方式：client客户端分页，server服务端分页（*）
        sortName : "instno",// 排序字段
        sortOrder : "asc",// 排序方式
        pageList : [ 5, 10, 15, 20, 25, 30 ],// 可供选择的每页的行数（*）
        pageNumber : 1,// 初始化加载第一页，默认第一页
        pagination : true,// 是否显示分页（*）
        pageSize : 12,// 每页的记录行数（*）
        showColumns : true, // 是否显示所有的列
        toolbar : "#toolbar",// 工具按钮用哪个容器
        clickToSelect : true,// 是否启用点击选中行
        cache : false,// 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped : false,// 是否显示行间隔色
        showRefresh : true,// 是否显示刷新按钮
        showToggle : true,// 是否显示详细视图和列表视图的切换按钮
        cardView : false,// 是否显示详细视图
        detailView : false,// 是否显示父子表
        paginationLoop : false,// 是否允许循环分页
        paginationPreText : "上一页",// 指定上一页按钮文字，不配置默认<
        paginationNextText : "下一页",
        columns : [{
            field: 'instno',
            title: '号码'
        }, {
            field: 'enddate',
            title: '日期',
            formatter:function(value,row,index){
                if (value != null) {
                    var date = new Date(parseInt(value));
                    var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
                    var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                    var dateNew = date.getFullYear()  + month  + currentDate;
                    return "<input name='enddate' id='enddate' value='"+dateNew+"'>"
                }
            }
        }, {
            field : 'daycount',
            title : '计息天数',
            formatter:function(value,row,index){
                var startdate = row.startdate;
                var enddate = row.enddate;
                var haomiaocount = enddate - startdate;
                var daycount = haomiaocount/86400000;
                return daycount;
            }
        }, {
            field : 'instamt',
            title : '每期还款额',
            formatter:function(value,row,index){
                return "<input name='instamt' id='instamt' value='"+value+"'>"
            }
        }, {
            field : 'begprinamt',
            title : '还款前本金'
        }, {
            field : 'intamt',
            title : '应还利息'
        }, {
            field : 'prnamt',
            title : '应还本金',
            formatter:function(value,row,index){
                return "<input name='prnamt' id='prnamt' value='"+value+"'>"
            }
        },{
            field : 'osamt',
            title : '还款后剩余本金'
        }],

        onClickRow : function(row) {

        }
    });

    var drawamt = $('#drawamt').val();
    var json={
        custcod : $("#custcod").val(),
        loanref : $("#loanref").val()
    };
    $.ajax({
        type: "POST",
        url: "/payList",
        contentType: "application/json",
        dataType : "json",
        data: JSON.stringify(json),
        async: false,
        success: function(result){
            var intamtcount = 0;
            for(var i=0;i<result.length;i++){
                intamtcount += result[i].intamt;
            }
            //总利息
            $('#amtcount2').val(intamtcount);
            //总还款额
            $('#amtcount1').val(intamtcount+parseInt(drawamt));
        },
        error: function (result) {
            alert(result);
        }
    });




});