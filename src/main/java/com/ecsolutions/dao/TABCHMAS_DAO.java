package com.ecsolutions.dao;

import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/8/18.
 */
@Repository
public interface TABCHMAS_DAO {
    @Select("SELECT REFNON FROM TABCHMAS ")
    @ResultType(HashMap.class)
    List<HashMap<String,String>> getBranchCodeList();
}
