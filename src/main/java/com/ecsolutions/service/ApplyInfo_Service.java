package com.ecsolutions.service;

import com.ecsolutions.entity.ApplyInfo_Entity;

/**
 * Created by Administrator on 2017/8/8.
 */
public interface ApplyInfo_Service {
    ApplyInfo_Entity getPopupInfo(String loanid);
    String getBranch (String userid);
    void applyInfoSubmit(String userid,String loanid,String stepcode,String result);
    String getWorkitemIdbyLoanid(String loanid);

//    boolean applyInfoFollow(String userid, String loanid, String stepcode, String result);

//    List<ApplyInfo_Entity> Search(String startDate, String endDate, String personalFlag, String status, String manager, String custCod, String lastName, String lregno);
}
