package com.ecsolutions.service;

import com.ecsolutions.dao.CreditAmountRecheck_DAO;
import com.ecsolutions.dao.Hkbinmas_DAO;
import com.ecsolutions.dao.SqlProvider.CreditAmountRecheck_Provider;
import com.ecsolutions.dao.User_DAO;
import com.ecsolutions.entity.CreditAmountRecheck_Entity;
import com.ecsolutions.entity.User_Entity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("CreditAmountRecheck_Service")
public class CreditAmountRecheck_ServiceImpl implements CreditAmountRecheck_Service {
    private CreditAmountRecheck_DAO creditAmountRecheck_dao;
    private User_DAO user_dao;
    private Hkbinmas_DAO hkbinmas_dao;

    public CreditAmountRecheck_ServiceImpl(Hkbinmas_DAO hkbinmas_dao,CreditAmountRecheck_DAO creditAmountRecheck_dao, User_DAO user_dao) {
        this.creditAmountRecheck_dao = creditAmountRecheck_dao;
        this.user_dao = user_dao;
        this.hkbinmas_dao=hkbinmas_dao;
    }

    @Override
    public List<CreditAmountRecheck_Entity> getSearchInfoResultList(String obkgid, String refnum, String startDate, String endDate) {

        return creditAmountRecheck_dao.getSearchInfoResultList(obkgid, refnum, startDate, endDate);
    }

    @Override
    public Integer countSearchInfoResultList(String obkgid, String refnum, String startDate, String endDate) {
        return creditAmountRecheck_dao.countSearchInfoResultList(obkgid, refnum, startDate, endDate);
    }

    @Override
    public String getFirstApproverName(String userId) {
        User_Entity user_entity = user_dao.getByUserId(userId);
        if (user_entity!=null) {
            return user_entity.getUsername();
        } else return "fail";
    }

    @Override
    public String getCustnm(String refnum) {
        return hkbinmas_dao.getCustnm(refnum);
    }

    @Override
    public boolean saveRecord(CreditAmountRecheck_Entity entity) {
        return creditAmountRecheck_dao.saveRecord(entity);
    }
}
