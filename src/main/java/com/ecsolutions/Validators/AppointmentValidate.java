package com.ecsolutions.Validators;

import com.ecsolutions.entity.Appointment_Entity;
import com.ecsolutions.entity.Contact_Entity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by Administrator on 2017/7/26.
 * 预约信息校验
 */
@Component
public class AppointmentValidate implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Appointment_Entity.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Appointment_Entity appointment_entity = (Appointment_Entity) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "appointclient", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "appointdate", "amountrequired", "不能为空");
    }
}
