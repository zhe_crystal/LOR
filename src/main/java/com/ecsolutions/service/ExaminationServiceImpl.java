package com.ecsolutions.service;

import com.ecsolutions.dao.ApplyCompany_dao;
import com.ecsolutions.dao.Customer_DAO;
import com.ecsolutions.dao.ExaminationDao;
import com.ecsolutions.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/8/25.
 */
@Service("ExaminationService")
public class ExaminationServiceImpl implements ExaminationService{
    private ExaminationDao examinationDao;
    private Customer_DAO customer_dao;
    private ApplyCompany_dao applyCompany_dao;
    @Autowired
    public ExaminationServiceImpl(ExaminationDao examinationDao, Customer_DAO customer_dao, ApplyCompany_dao applyCompany_dao) {
        this.examinationDao= examinationDao;
        this.customer_dao = customer_dao;
        this.applyCompany_dao = applyCompany_dao;
    }


    @Override
    public List<Examination> getUsersInfo(){
        List<Examination> Listuser = examinationDao.getListUsrsInfo();
        return Listuser;
    };
    @Override
    public String getPuid(String loanid){
        String puId=examinationDao.getPuid(loanid);
        return puId;
    };

    @Override
    public void savaExamination(String loanid,String userid,Examination examination){
        examinationDao.InsertMicroCreditAuditLog(loanid,userid,examination);
    };

    @Override
    public String getCustcod(String loanid){
        return examinationDao.getCustcod(loanid);
    }

    @Override
    public String getCustomerType(String custcod) {
        return customer_dao.getCustomerType(custcod);
    }

    @Override
    public Acpy_Customer_Entity getPersonal(String custcode) {
        return customer_dao.searchCustomer(custcode);
    }

    @Override
    public Acpy_Enterprise_Entity getEnterprise(String custcode) {
        return applyCompany_dao.searchCustomer(custcode);
    }
}
