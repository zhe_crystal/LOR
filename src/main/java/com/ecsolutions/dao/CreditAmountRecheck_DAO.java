package com.ecsolutions.dao;

import com.ecsolutions.dao.SqlProvider.CreditAmountRecheck_Provider;
import com.ecsolutions.entity.CreditAmountRecheck_Entity;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreditAmountRecheck_DAO {
    @SelectProvider(type = CreditAmountRecheck_Provider.class, method = "getSearchInfoResultList")
    @ResultType(CreditAmountRecheck_Entity.class)
    List<CreditAmountRecheck_Entity> getSearchInfoResultList(String obkgid, String refnum, String startDate, String endDate);

    @SelectProvider(type = CreditAmountRecheck_Provider.class, method = "countSearchInfoResultList")
    @ResultType(Integer.class)
    Integer countSearchInfoResultList(String obkgid, String refnum, String startDate, String endDate);

    @Update("Update Hkbcrmas set actcode=#{actcode,jdbcType=VARCHAR}," +
            "apprmk1=#{apprmk1,jdbcType=VARCHAR}," +
            "appid1=#{appid1,jdbcType=VARCHAR}," +
            "appdat1=to_number(#{appdat1,jdbcType=VARCHAR})," +
            "apptim1=to_number(#{apptim1,jdbcType=VARCHAR})" +
            "Where refnum=#{refnum}")
    @ResultType(boolean.class)
    boolean saveRecord(CreditAmountRecheck_Entity entity);
}
