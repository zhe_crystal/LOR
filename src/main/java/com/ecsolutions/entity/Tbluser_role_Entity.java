package com.ecsolutions.entity;

/**
 * Created by ecs on 2017/8/18.
 */
public class Tbluser_role_Entity {
    private String userid;
    private String roleid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }
}
