package com.ecsolutions.dao;

import com.ecsolutions.entity.CompDepApproval_entity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * Created by ecs on 2017/8/29.
 */
@Repository
public interface CompDepApproval_DAO {

    @Select("SELECT bankcode FROM ApplySerial where bpm_no =#{loanid}")
    String getBankcode(String loanid);

    @Select("SELECT branch FROM Tblusers where userid =#{userid}")
    String getBranch(String userid);

    @Select("SELECT drawamt FROM utilizationDetail where bpm_no =#{loanid}")
    String getDrawamt(String loanid);

    @Select("SELECT drawccy FROM utilizationDetail where bpm_no =#{loanid}")
    String getDrawccy(String loanid);

    @Select("SELECT max(Puid)+1 FROM LoanRouteHistory where loanid =#{loanid}")
    String getPuid(String loanid);

    @Insert("insert into MicroCreditAuditLog(WorkItemID,custcod,LoanID,AuditNo,AuditTime,AuditUserBankCode,WorkItemTitle,AuditContext,ActionUserName,Result) values(" +
            "(select workitemid from LoanRouteHistory where loanid =#{arg0} and rownum=1)," +
            "(select custcode from applyserial where bpm_no=#{arg0} and rownum=1)," +
            "#{arg0}," +
            "(SELECT max(Puid)+1 FROM LoanRouteHistory where loanid =#{arg0})," +
            "(select sysdate from dual)," +
            "(SELECT branch FROM Tblusers WHERE userid= #{arg1})," +
            "#{arg2.WorkItemTitle}," +
            "#{arg2.AuditContext}," +
            "#{arg2.ActionUserName}," +
            "#{arg2.Result})")
    void insert(String loanid,String userid,CompDepApproval_entity approval_entity);

    @Insert("insert into LoanRouteHistory(puid,loanid,actiondate,actionuser,stepcode,result,workitemid) values(" +
            "(select (max(puid))+1 from LoanRouteHistory where loanid =#{arg1})," +
            "#{arg1}," +
            "(select sysdate from dual)," +
            "#{arg0}," +
            "#{arg2}," +
            "#{arg3}," +
            "(select workitemid from LoanRouteHistory where loanid =#{arg1} and rownum=1)" +
            ")")
    void approvalInfoSubmit(String userid,String loanid,String stepcode,String result);
}
