package com.ecsolutions.controller;

import com.ecsolutions.entity.repaymentScheModify_entity;
import com.ecsolutions.entity.repaymentSchedule_entity;
import com.ecsolutions.service.repaymentScheModify_Service;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class repaymentScheModify_Controller {

    @Autowired
    private repaymentScheModify_Service repaymentschemodify_service;

    public repaymentScheModify_Service getRepaymentschemodify_service() {
        return repaymentschemodify_service;
    }

    @GetMapping("/repaymentScheModify")
    public String getLoanInfo(Model model,@RequestParam("userid") String userid) {
        repaymentScheModify_entity repaymentschemodify_entity1 = new repaymentScheModify_entity();
        repaymentSchedule_entity repaymentschedule_entity = new repaymentSchedule_entity();
        repaymentScheModify_entity repaymentschemodify_entity2 = new repaymentScheModify_entity();
        model.addAttribute("repaymentScheModify_entity1", repaymentschemodify_entity1);
        model.addAttribute("repaymentSchedule_entity", repaymentschedule_entity);
        model.addAttribute("repaymentScheModify_entity2", repaymentschemodify_entity2);
        model.addAttribute("userid", userid);
        return "repaymentScheModify";
    }

    @PostMapping("/repaymentScheModify")
    public String postLoanInfo(@ModelAttribute("repaymentScheModify_entity1") repaymentScheModify_entity repaymentschemodify_entity1,@ModelAttribute("repaymentSchedule_entity") repaymentSchedule_entity repaymentschedule_entity,@ModelAttribute("repaymentScheModify_entity2") repaymentScheModify_entity repaymentschemodify_entity2, BindingResult result, Model model,@RequestParam("userid") String userid) {
        model.addAttribute("repaymentScheModify_entity1", repaymentschemodify_entity1);
        model.addAttribute("repaymentSchedule_entity", repaymentschedule_entity);
        model.addAttribute("repaymentScheModify_entity2", repaymentschemodify_entity2);
        model.addAttribute("userid", userid);
        String refno = repaymentschedule_entity.getRefno();
        List<repaymentSchedule_entity> repaymentScheduleList = repaymentschemodify_service.getRepaymentScheduleList(refno);
        repaymentschedule_entity = repaymentScheduleList.get(0);
        repaymentschemodify_service.saveToHkbppmas(repaymentschemodify_entity1);
        for (repaymentSchedule_entity repaysche_entity: repaymentScheduleList) {
            repaymentschemodify_service.saveToAdsinmf4(repaysche_entity);
        }
        repaymentschemodify_service.saveToHkbpptmp(repaymentschemodify_entity2, repaymentschedule_entity);
        repaymentschemodify_service.saveToHkbindtl(repaymentschemodify_entity2, repaymentschedule_entity);
        return "repaymentScheduleInfo";
    }

    @RequestMapping(value = "/repaymentScheModify/viewLoanDetails",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    @ResponseBody
    public repaymentScheModify_entity findByRefno(@RequestBody Map<String, Object> map) {
        String refno = map.get("refno").toString();
        repaymentScheModify_entity result = repaymentschemodify_service.getLoanInfo(refno);
        result.setIntrate(repaymentschemodify_service.getIntrate(result.getRefno()));
        result.setStrdate(repaymentschemodify_service.getStrdate(result.getRefno()));
        return result;
    }

    @RequestMapping(value = "/repaymentScheModify_getIntrate",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    @ResponseBody
    public String getIntrateInfo(@RequestBody Map<String, Object> map) {
        String refno = map.get("refno").toString();
        String result = repaymentschemodify_service.getIntrate(refno);
        return result;
    }

//    @RequestMapping(value = "/repaymentScheModify_getStrdate",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
//    @ResponseBody
//    public String getStrdateInfo(@RequestBody Map<String, Object> map) {
//        String refno = map.get("refno").toString();
//        String result = repaymentschemodify_service.getStrdate(refno);
//        return result;
//    }

    /*查询出信息跳转到还款计划表页面*/
    @RequestMapping(value = "/repaymentScheduleInfo",method = RequestMethod.GET)
    public String getPay(String refno,String dwndate,String intrate,String instno,String prymth,String paymth,String matdate,String rpyfeq,String rpyper,String strdate,String fpaydat,Model model,@RequestParam("userid") String userid){
        repaymentSchedule_entity repaymentschedule_entity = new repaymentSchedule_entity();
        repaymentScheModify_entity repaymentschemodify_entity1 = repaymentschemodify_service.getRepaymentSchedulePage(refno);
        repaymentScheModify_entity repaymentschemodify_entity2 = repaymentschemodify_service.getRepaymentSchedulePage(refno);
        repaymentschemodify_entity1.setIntrate(repaymentschemodify_service.getIntrate(repaymentschemodify_entity1.getRefno()));
        repaymentschemodify_entity1.setDwndate(dwndate);
        repaymentschemodify_entity1.setIntrate(intrate);
        repaymentschemodify_entity1.setInstno(instno);
        repaymentschemodify_entity1.setPrymth(prymth);
        repaymentschemodify_entity1.setPaymth(paymth);
        repaymentschemodify_entity1.setMatdate(matdate);
        repaymentschemodify_entity1.setRpyfeq(rpyfeq);
        repaymentschemodify_entity1.setRpyper(rpyper);
        repaymentschemodify_entity1.setStrdate(strdate);
        repaymentschemodify_entity1.setFpaydat(fpaydat);
        repaymentschemodify_entity2.setPaymth(paymth);
        repaymentschemodify_entity2.setMatdate(matdate);
        model.addAttribute("repaymentScheModify_entity1",repaymentschemodify_entity1);
        model.addAttribute("repaymentSchedule_entity", repaymentschedule_entity);
        model.addAttribute("repaymentScheModify_entity2", repaymentschemodify_entity2);
        model.addAttribute("userid", userid);
        return "repaymentScheduleInfo";
    }

    /*通过refno查询信息到还款计划表格中*/
    @ResponseBody
    @RequestMapping(value = "/getRepaymentScheduleInfo",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public List<repaymentSchedule_entity> getRepaymentScheduleInfo(@RequestBody Map<String, Object> map ){
        String refno = map.get("refno").toString();
        List<repaymentSchedule_entity> repaymentScheduleList = repaymentschemodify_service.getRepaymentScheduleList(refno);
//        for (repaymentSchedule_entity repaymentschedule_entity: repaymentScheduleList) {
//            repaymentschemodify_service.saveToAdsinmf4(repaymentschedule_entity);
//        }
        return repaymentScheduleList;
    }

    /*通过refno导出还款计划表*/
    @ResponseBody
    @RequestMapping(value = "/exportRepaymentSchedule",method = RequestMethod.POST)
    public String exportRepaymentScheduleInfo(String refno){
        List<repaymentSchedule_entity> repaymentScheduleList = repaymentschemodify_service.getRepaymentScheduleList(refno);//还款计划表信息
        repaymentScheModify_entity repaymentschemodify_entity = repaymentschemodify_service.getRepaymentSchedulePage(refno);;//贷款信息
        String result = repaymentschemodify_service.exportExcel(repaymentScheduleList,repaymentschemodify_entity);
        return result;
    }

    @RequestMapping(value = "/repaymentScheModify/Search", method = RequestMethod.GET)
    @ResponseBody
    public String searchResult(@RequestParam String refno, int limit, int offset) {
        Gson gson = new Gson();
        int count_result = repaymentschemodify_service.countResultList(refno);
        PageHelper.startPage(offset, limit);
        List<repaymentScheModify_entity> reuslt_list = repaymentschemodify_service.getResultList(refno);
        for(repaymentScheModify_entity repaymentschemodify_entity : reuslt_list){
            repaymentschemodify_entity.setIntrate(repaymentschemodify_service.getIntrate(repaymentschemodify_entity.getRefno()));
        }
        String searchResultList = gson.toJson(reuslt_list);
        String errorCode = "0";
        return "{\"rowDatas\":" + searchResultList + ",\"dataLength\":" + count_result + ",\"errCode\":" + errorCode + "}";
    }
}
