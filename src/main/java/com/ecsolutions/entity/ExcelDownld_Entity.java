package com.ecsolutions.entity;

/**
 * Created by Administrator on 2017/8/17.
 */

public class ExcelDownld_Entity {
    private String LOANREF;

    //UtilizationdetailInfo
    private String drawamt;
    private String drawccy;
    private String drawdate;
    private String schemarate;
    private String tenor;

    //PaymentScheduleInfo
    private String instno;
    private String enddate;
    private String instamt;
    private String prnamt;
    private String intamt;
    private String osamt;

    //SumInfo
    private String sum_instamt;
    private String sum_prnamt;
    private String sum_intamt;

    public String getDrawamt() {
        return drawamt;
    }

    public String getDrawccy() {
        return drawccy;
    }

    public void setDrawccy(String drawccy) {
        this.drawccy = drawccy;
    }

    public String getDrawdate() {
        return drawdate;
    }

    public void setDrawdate(String drawdate) {
        this.drawdate = drawdate;
    }

    public String getSchemarate() {
        return schemarate;
    }

    public void setSchemarate(String schemarate) {
        this.schemarate = schemarate;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getInstno() {
        return instno;
    }

    public void setInstno(String instno) {
        this.instno = instno;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getInstamt() {
        return instamt;
    }

    public void setInstamt(String instamt) {
        this.instamt = instamt;
    }

    public String getPrnamt() {
        return prnamt;
    }

    public void setPrnamt(String prnamt) {
        this.prnamt = prnamt;
    }

    public String getIntamt() {
        return intamt;
    }

    public void setIntamt(String intamt) {
        this.intamt = intamt;
    }

    public String getOsamt() {
        return osamt;
    }

    public void setOsamt(String osamt) {
        this.osamt = osamt;
    }

    public String getSum_instamt() {
        return sum_instamt;
    }

    public void setSum_instamt(String sum_instamt) {
        this.sum_instamt = sum_instamt;
    }

    public String getSum_prnamt() {
        return sum_prnamt;
    }

    public void setSum_prnamt(String sum_prnamt) {
        this.sum_prnamt = sum_prnamt;
    }

    public String getSum_intamt() {
        return sum_intamt;
    }

    public void setSum_intamt(String sum_intamt) {
        this.sum_intamt = sum_intamt;
    }

    public void setDrawamt(String drawamt) {
        this.drawamt = drawamt;
    }




    public String getLOANREF() {
        return LOANREF;
    }

    public void setLOANREF(String LOANREF) {
        this.LOANREF = LOANREF;
    }
}
