package com.ecsolutions.controller;

import com.ecsolutions.common.ObjectHelp;
import com.ecsolutions.entity.Apply_entity;
import com.ecsolutions.service.ApplyCompany_Service;
import com.ecsolutions.soaClient.TransferClient;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/6/27.
 */
@Controller
public class ApplyCompany {

    @Autowired
    private ApplyCompany_Service applyCompany_service;
    public ApplyCompany_Service getApplyCompany_service() {
        return applyCompany_service;
    }

    @Autowired
    private TransferClient client;

    @Autowired
    @Qualifier("companyValidate")
    private Validator validator;

    @InitBinder
    public void initBinder(DataBinder binder){
        binder.setValidator(validator);
    }

    //新建企业用户申请界面
    @GetMapping("/apply")
    public String getApplyPage(Model model) {
        model.addAttribute("apply_entity", new Apply_entity());
        model.addAttribute("apply_info", applyCompany_service.getPopupInfo());
        return "applyCompany";
    }

    //企业用户信息查看修改界面
    @GetMapping("/EnterpriseUserInfo")
    public String getApplyPage(@RequestParam String CustCode, Model model) {
        //填充数据
        Apply_entity apply_entity = applyCompany_service.getApplyEntity(CustCode);
        System.out.println(JSONObject.fromObject(apply_entity));
        model.addAttribute("apply_entity", apply_entity);
        model.addAttribute("apply_info", applyCompany_service.getPopupInfo());
        return "applyCompany";
    }

    @PostMapping("/apply")
    public String saveApplyInfo(@Valid @ModelAttribute Apply_entity apply_entity, BindingResult result, Model model) {
        if(!result.hasErrors()) {
    //调用soa
        try {
            System.out.println("call TransferClient.transfer");
            String mess = ObjectHelp.InitTransferData("GenerateCustcodTx", null);
            // TransferClient client=new TransferClient();
            String recieveMsg = client.transfer(mess);
            if (recieveMsg != null || recieveMsg.length() > 0) {
                JSONObject receiveJson= JSONObject.fromObject(recieveMsg);
                String pcustcd = receiveJson.getString("PCUSTCD");
                String prspcod = receiveJson.getString("PRSPCOD");

                if (prspcod.equals("0000000")) {
                    System.out.println("get CustomerCode success");
                    System.out.println("pcustcd: " + pcustcd);
                    System.out.println("prspcod: " + prspcod);
                } else {
                    System.out.println("Cannot get CustomerCode From Soa");
                }
                apply_entity.setSoaCustcode(pcustcd);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
       //soa 调用结束

        applyCompany_service.saveApplyEntity(apply_entity);

        model.addAttribute("apply_entity", apply_entity);
        model.addAttribute("apply_info", applyCompany_service.getPopupInfo());
        }
        return "applyCompany";
    }

    @ResponseBody
    @RequestMapping(value = "/applyCompany_ethnicCheck",method = RequestMethod.POST)
    public String findByEthnic(String ethnic){
        //Pledge_Entity pledge_entity = pledgeNew_service.getByEthnic(ethnic);
        //return pledge_entity;
        String result = applyCompany_service.ethnicCheck(ethnic);
        return result;
    }

    /*通过门类值查询信息到大类下拉框*/
    @ResponseBody
    @RequestMapping(value = "/applyCompany_getDaleiInfo",method = RequestMethod.POST)
    public List<HashMap<String,String>> getDaleiList(String industrialClass){
        List<HashMap<String,String>> daleiList = applyCompany_service.getByMenleiInfo(industrialClass);
        return daleiList;
    }

    /*通过门类和大类查询信息到中类下拉框*/
    @ResponseBody
    @RequestMapping(value = "/applyCompany_getZhongleiInfo",method = RequestMethod.POST)
    public List<HashMap<String,String>> getZhongleiList(String industrialClass,String industrialPrimaryCategory){
        List<HashMap<String,String>> zhongleiList = applyCompany_service.getByMenleiAndDaleiInfo(industrialClass,industrialPrimaryCategory);
        return zhongleiList;
    }
}
