package com.ecsolutions.service;

import com.ecsolutions.dao.Monitoring_DAO;
import com.ecsolutions.entity.monitoring_entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ecs on 2017/10/10.
 */
@Service("Monitoring_Service")
public class Monitoring_ServiceImpl implements Monitoring_Service{
    private Monitoring_DAO monitoring_dao;

    @Autowired
    public Monitoring_ServiceImpl(Monitoring_DAO monitoring_dao) {
        this.monitoring_dao = monitoring_dao;
    }

    @Override
    public List<monitoring_entity> workflowInfoSearch(){
        List<monitoring_entity> result = monitoring_dao.getWorkflowInfo();
        return result;
    }

    @Override
    public void deleteWorkflowinfo(String bpm_no){
        monitoring_dao.delete(bpm_no);
    }
}
