package com.ecsolutions.dao.SqlProvider;

import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * Created by ecs on 2017/9/4.
 */
public class repaymentScheModify_Provider {
    public String getResultList(Map<String, Object> map) {
        String refno = (String) map.get("param1");
        return new SQL() {{
//            SELECT("hkbinmas.refno,hkbinmas.custnm,hkbinmf4.intrate,hkbinmas.instno,hkbinmas.lnamt ");
//            FROM("hkbinmas left join hkbinmf4 on TRIM(hkbinmas.instno)=TRIM(hkbinmf4.instno)");
            SELECT("* ");
            FROM("hkbinmas ");
            if (!refno.equals("") && refno != null) {
                WHERE("TRIM(hkbinmas.refno)=#{param1}");
            }
            ORDER_BY("refno");
        }}.toString();
    }

    public String countResultList(Map<String, Object> map) {
        String refno = (String) map.get("param1");
        return new SQL() {{
            SELECT("COUNT(*) ");
            FROM("hkbinmas ");
            if (!refno.equals("") && refno != null) {
                WHERE("TRIM(refno)=#{param1}");
            }
        }}.toString();
    }
}
