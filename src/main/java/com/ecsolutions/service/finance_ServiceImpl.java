package com.ecsolutions.service;

import com.ecsolutions.dao.finance_DAO;
import com.ecsolutions.entity.finance_body_entity;
import com.ecsolutions.entity.finance_header_entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ecs on 2017/8/18.
 */
@Service("finance_Service")
public class finance_ServiceImpl implements finance_Service{
    private finance_DAO finance_dao;

    @Autowired
    public finance_ServiceImpl(finance_DAO finance_dao) {
        this.finance_dao = finance_dao;
    }

    @Override
    public finance_header_entity getPopupInfo(){
        finance_header_entity result = new finance_header_entity();
        System.out.println("begin qian");
        System.out.println("getid success");
        System.out.println("okkkk");
        return result;
    }

    @Override
    public void saveFinanceHeaderEntity(finance_header_entity finance_header_entity){
        finance_dao.insert_header(finance_header_entity);
    }

    @Override
    public void saveFinanceBodyEntity(finance_body_entity finance_body_entity){
        finance_dao.insert_body(finance_body_entity);
    }

    @Override
    public String insertCheck(String custcode){
        return finance_dao.checkinsert(custcode);
    }

    @Override
    public List<finance_header_entity> financeHeaderInfoSearch(String custcode){
        List<finance_header_entity> result = finance_dao.getHeaderInfo(custcode);
        return result;
    }

    @Override
    public List<String> getReportno(String custcode){
        return finance_dao.getReportnoList(custcode);
    }

    @Override
    public List<finance_body_entity> financeBodyInfoSearch(String reportno){
        List<finance_body_entity> result = finance_dao.getBodyInfo(reportno);
        return result;
    }
}
