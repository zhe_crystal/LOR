package com.ecsolutions.dao;

import com.ecsolutions.entity.monitoring_entity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ecs on 2017/10/10.
 */
@Repository
public interface Monitoring_DAO {

    // select from tbl_workflow
    @Select("SELECT * FROM tbl_workflow ")
    @ResultType(monitoring_entity.class)
    List<monitoring_entity> getWorkflowInfo();

    //根据贷款编号bpm_no删除工作流记录
    @Delete("delete from tbl_workflow where bpm_no=#{bpm_no}")
    void delete(String bpm_no);
}
