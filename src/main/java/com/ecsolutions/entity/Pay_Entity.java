package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/6/29.
 */
public class Pay_Entity {
    private String loanref;
    private String custcod;
    private Integer instno;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date startdate;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date enddate;
    private BigDecimal intrate;
    private BigDecimal begprinamt;
    private BigDecimal instamt;
    private BigDecimal intamt;
    private BigDecimal prnamt;
    private BigDecimal osamt;
    //计息天数
    private Integer daycount;

    public String getLoanref() {
        return loanref;
    }

    public void setLoanref(String loanref) {
        this.loanref = loanref;
    }

    public String getCustcod() {
        return custcod;
    }

    public void setCustcod(String custcod) {
        this.custcod = custcod;
    }

    public Integer getInstno() {
        return instno;
    }

    public void setInstno(Integer instno) {
        this.instno = instno;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public BigDecimal getIntrate() {
        return intrate;
    }

    public void setIntrate(BigDecimal intrate) {
        this.intrate = intrate;
    }

    public BigDecimal getBegprinamt() {
        return begprinamt;
    }

    public void setBegprinamt(BigDecimal begprinamt) {
        this.begprinamt = begprinamt;
    }

    public BigDecimal getInstamt() {
        return instamt;
    }

    public void setInstamt(BigDecimal instamt) {
        this.instamt = instamt;
    }

    public BigDecimal getIntamt() {
        return intamt;
    }

    public void setIntamt(BigDecimal intamt) {
        this.intamt = intamt;
    }

    public BigDecimal getPrnamt() {
        return prnamt;
    }

    public void setPrnamt(BigDecimal prnamt) {
        this.prnamt = prnamt;
    }

    public BigDecimal getOsamt() {
        return osamt;
    }

    public void setOsamt(BigDecimal osamt) {
        this.osamt = osamt;
    }

    public Integer getDaycount() {
        return daycount;
    }

    public void setDaycount(Integer daycount) {
        this.daycount = daycount;
    }
}