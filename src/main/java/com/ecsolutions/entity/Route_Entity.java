package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Administrator on 2017/6/29.
 */
public class Route_Entity {
    private Integer puid;
    private String stepcode;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date audittime;
    private String workitemtitle;
    private String audituserbankcode;
    private String actionusername;
    private Integer result;

    public Integer getPuid() {
        return puid;
    }

    public void setPuid(Integer puid) {
        this.puid = puid;
    }

    public String getStepcode() {
        return stepcode;
    }

    public void setStepcode(String stepcode) {
        this.stepcode = stepcode;
    }

    public Date getAudittime() {
        return audittime;
    }

    public void setAudittime(Date audittime) {
        this.audittime = audittime;
    }

    public String getWorkitemtitle() {
        return workitemtitle;
    }

    public void setWorkitemtitle(String workitemtitle) {
        this.workitemtitle = workitemtitle;
    }

    public String getAudituserbankcode() {
        return audituserbankcode;
    }

    public void setAudituserbankcode(String audituserbankcode) {
        this.audituserbankcode = audituserbankcode;
    }

    public String getActionusername() {
        return actionusername;
    }

    public void setActionusername(String actionusername) {
        this.actionusername = actionusername;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
}