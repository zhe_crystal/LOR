package com.ecsolutions.service;

import com.ecsolutions.entity.Tbluser_Properties_Entity;

import java.util.List;

public interface Tbluser_Properties_Service {
    public List<Tbluser_Properties_Entity> getAllByUserId(String UserId);

    public boolean updateOneByAll(Tbluser_Properties_Entity entity);

    public boolean insertUserProperty(Tbluser_Properties_Entity entity);

    public boolean initNewUserProperty(String UserId);

    public boolean deleteOneUserProperty(String UserId);

    public boolean deleteOneProperty(String PropertiesId);

    public boolean insertOneProperty(String PropertiesId);
}
