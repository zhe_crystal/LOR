package com.ecsolutions.service;

import com.ecsolutions.dao.Contact_DAO;
import com.ecsolutions.entity.Contact_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/7/24.
 */
@Service("Contact_Service")
public class Contact_ServiceImpl implements Contact_Service {
    private Contact_DAO contact_dao;

    @Autowired
    public Contact_ServiceImpl(Contact_DAO contact_dao) {
        this.contact_dao = contact_dao;
    }


    @Override
    public List<Contact_Entity> getContactInfo(String custcode, String bpm_no) {
        return contact_dao.getByCustcodeAndBpm_no(custcode,bpm_no);
    }

    @Override
    public Integer getContactId(String custcode, String bpm_no) {
        return contact_dao.getIdByCustcodeAndBpm_no(custcode,bpm_no);
    }

    @Override
    public void saveContactInfo(Contact_Entity contact_entity) {
        contact_dao.saveContactInfo(contact_entity);
    }

    @Override
    public void updateContactInfo(Contact_Entity contact_entity) {
        contact_dao.updateContactInfo(contact_entity);
    }

    @Override
    public void deleteContactInfo(Integer id) {
        contact_dao.deleteContactInfo(id);
    }
}

