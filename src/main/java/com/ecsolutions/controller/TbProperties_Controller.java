package com.ecsolutions.controller;

import com.ecsolutions.Validators.TbProperties_Validate;
import com.ecsolutions.entity.TbProperties_Entity;
import com.ecsolutions.service.TbProperties_Service;
import com.ecsolutions.service.Tbluser_Properties_Service;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class TbProperties_Controller {
    @Autowired
    private TbProperties_Service tbProperties_service;

    @Autowired
    private Tbluser_Properties_Service tbluser_properties_service;

    public Tbluser_Properties_Service getTbluser_properties_service() {
        return tbluser_properties_service;
    }

    public TbProperties_Service getTbProperties_service() {
        return tbProperties_service;
    }

    @InitBinder("tbProperties_entity")
    public void initBinder(WebDataBinder binder) {
        binder.setValidator(new TbProperties_Validate());
    }

    @GetMapping("/TbProperties")
    public String getTbProperties(Model model) {
        model.addAttribute("tbProperties_entity", new TbProperties_Entity());
        return "/ClientInformation/TbProperties";
    }

    @PostMapping("/TbProperties")
    public String postTbProperties(Model model, @Valid @ModelAttribute("tbProperties_entity") TbProperties_Entity tbProperties_entity, BindingResult result) {
        model.addAttribute("tbProperties_entity", tbProperties_entity);
        if (!result.hasErrors()) {
            if (tbProperties_service.countOne(tbProperties_entity.getPropertiesId()) > 0) {
                tbProperties_service.update(tbProperties_entity);
            } else {
                tbProperties_service.save(tbProperties_entity);
                tbluser_properties_service.insertOneProperty(tbProperties_entity.getPropertiesId());
            }
        }
        return "/ClientInformation/TbProperties";
    }

    @ResponseBody
    @RequestMapping(value = "/TbProperties/getHistoryList", method = RequestMethod.GET)
    public String getHistoryList(int pageSize, int pageIndex) {
        Gson gson = new Gson();
        int total = tbProperties_service.countAll();
        PageHelper.startPage(pageIndex, pageSize);
        List<TbProperties_Entity> list = tbProperties_service.findAll();
        String historyList = gson.toJson(list);
        String errorCode = "0";
        return "{\"rowDatas\":" + historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}";
    }

    @ResponseBody
    @RequestMapping(value = "/TbProperties/deleteTblRole", method = RequestMethod.GET)
    public String deleteTbProperty(@RequestParam String PropertiesId) {
        boolean state = tbProperties_service.delete(PropertiesId) & tbluser_properties_service.deleteOneProperty(PropertiesId);
        if (state)
            return "success";
        else
            return "fail";
    }

}
