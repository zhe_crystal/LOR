package com.ecsolutions.dao;

import com.ecsolutions.entity.TblRole_Entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ecs on 2017/8/21.
 */
@Repository
public interface TblRole_DAO extends Repository {
    @Select("Select roleid from tblrole")
    @ResultType(String.class)
    List<String> getRoleId();

    @Select("Select * from tblrole order by roleid ASC")
    @ResultType(TblRole_Entity.class)
    List<TblRole_Entity> getHistoryList();

    @Select("Select COUNT(*) from tblrole")
    @ResultType(Integer.class)
    Integer countHistoryList();

    @Select("SELECT COUNT(*) from tblrole where roleid=#{roleid,jdbcType=VARCHAR}")
    @ResultType(Integer.class)
    Integer getOne(String roleid);

    @Insert("Insert into tblrole (roleid,roledesc) values (#{roleid,jdbcType=VARCHAR},#{roledesc,jdbcType=VARCHAR})")
    @ResultType(boolean.class)
    boolean saveTblRole(TblRole_Entity entity);

    @Update("Update tblrole Set roledesc=#{roledesc} Where roleid= #{roleid}")
    @ResultType(boolean.class)
    boolean updateTblRole(TblRole_Entity entity);

    @Delete("Delete from tblrole Where roleid=#{roleid}")
    @ResultType(boolean.class)
    boolean deleteTblRole(String roleid);
}
