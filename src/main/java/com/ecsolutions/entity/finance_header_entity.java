package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.File;
import java.util.Date;
import java.util.Random;

/**
 * Created by ecs on 2017/8/18.
 */
public class finance_header_entity {
    private File finance_file;
    private String custcode = "100501900961";
    private Integer typeid;
    private String reportno;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date reportdate;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date reportenddate;
    private String auditoffice;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date audittime;
    private String auditofficer;
    private Integer seqno;
    private String detail_typeid;
    private double origvalue;
    private double endvalue;

    public File getFinance_file() {
        return finance_file;
    }

    public void setFinance_file(File finance_file) {
        this.finance_file = finance_file;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public String getReportno() {
        return reportno;
    }

    public void setReportno(String reportno) {
        this.reportno = reportno;
    }

    public Date getReportdate() {
        return reportdate;
    }

    public void setReportdate(Date reportdate) {
        this.reportdate = reportdate;
    }

    public Date getReportenddate() {
        return reportenddate;
    }

    public void setReportenddate(Date reportenddate) {
        this.reportenddate = reportenddate;
    }

    public String getAuditoffice() {
        return auditoffice;
    }

    public void setAuditoffice(String auditoffice) {
        this.auditoffice = auditoffice;
    }

    public Date getAudittime() {
        return audittime;
    }

    public void setAudittime(Date audittime) {
        this.audittime = audittime;
    }

    public String getAuditofficer() {
        return auditofficer;
    }

    public void setAuditofficer(String auditofficer) {
        this.auditofficer = auditofficer;
    }

    public Integer getSeqno() {
        return seqno;
    }

    public void setSeqno(Integer seqno) {
        this.seqno = seqno;
    }

    public String getDetail_typeid() {
        return detail_typeid;
    }

    public void setDetail_typeid(String detail_typeid) {
        this.detail_typeid = detail_typeid;
    }

    public double getOrigvalue() {
        return origvalue;
    }

    public void setOrigvalue(double origvalue) {
        this.origvalue = origvalue;
    }

    public double getEndvalue() {
        return endvalue;
    }

    public void setEndvalue(double endvalue) {
        this.endvalue = endvalue;
    }
}
