package com.ecsolutions.service;

import com.ecsolutions.entity.repaymentScheModify_entity;
import com.ecsolutions.entity.repaymentSchedule_entity;

import java.util.List;

/**
 * Created by Administrator on 2017-09-07.
 */
public interface RepayScheModReviewService {
    List<repaymentScheModify_entity> getResultList(String refno);
    String getIntrate(String refno);
    String getStrdate(String refno,String curinno);
    repaymentScheModify_entity getLoanInfo(String refno);
    List<repaymentSchedule_entity> getRepaymentScheduleList(String refno);
}
