package com.ecsolutions.controller;

import com.ecsolutions.Validators.RepaymentInputValidate;
import com.ecsolutions.common.ObjectHelp;
import com.ecsolutions.entity.repaymentInput_entity;
import com.ecsolutions.service.repaymentInput_Service;
import com.ecsolutions.soaClient.TransferClient;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class repaymentInput {



    @Autowired
    private repaymentInput_Service repaymentinput_service;

    public repaymentInput_Service getRepaymentinput_service() {
        return repaymentinput_service;
    }

    @Autowired
    private TransferClient client;

    @Autowired
    @Qualifier("repaymentInputValidate")
    private Validator validator;

    @InitBinder("repaymentInput_entity")
    public void initBinder(DataBinder binder){
        binder.setValidator(validator);
    }

    public repaymentInput(){

    }

    @GetMapping("/repaymentInput")
    public String getRepaymentInputPage(Model model, @RequestParam String refno) {
        List<repaymentInput_entity> result_list = repaymentinput_service.getResultList("", "", "", refno);
        repaymentInput_entity repaymentinput_entity = result_list.get(0);


        //调用soa
        try {
            System.out.println("call TransferClient.transfer");
            //需要三个参数refno 实体中已经有了，PRODATE不知是什么，QOLFLAG 授权标志赋的是status的值
            String mess = ObjectHelp.InitTransferData("RepaymentSearchTx", repaymentinput_entity);
            String recieveMsg = client.transfer(mess);
            System.out.println(recieveMsg);
            if (recieveMsg != null || recieveMsg.length() > 0) {
                JSONObject receiveJson= JSONObject.fromObject(recieveMsg);
                String prtncod = receiveJson.getString("PRTNCOD");
                String plamtx = receiveJson.getString("PLAMTX"); //本金金额
                String pitintx = receiveJson.getString("PITINTX");//分期利息 授权 = "Y" OR "P"
                String podintx = receiveJson.getString("PODINTX");//逾期利息 授权 = "Y" OR "P"
                String plchgx = receiveJson.getString("PLCHGX");//逾期支付 授权 = "Y" OR "P"
                String pitinty = receiveJson.getString("PITINTY");//分期利息 授权 = "N"
                String podinty = receiveJson.getString("PODINTY");//逾期利息 授权 = "N"
                String plchgy = receiveJson.getString("PLCHGY");//逾期支付 授权 = "N"
                String pstpdx = receiveJson.getString("PSTPDX");//税  授权 = "Y" OR "P"
                String pstpdy = receiveJson.getString("PSTPDY");//税  授权 = "N"
                String pcmint = receiveJson.getString("PCMINT");//复合利息
                String pinstno = receiveJson.getString("PINSTNO");//起始期数
                String payto = receiveJson.getString("PAYTO");//结束期数


                if (prtncod.equals("0000")) {
                    System.out.println("get RepaymentSearchTx success");
                    repaymentinput_entity.setInstnof(pinstno);
                    repaymentinput_entity.setInstnot(payto);
                    repaymentinput_entity.setPrinamt(plamtx);
                    repaymentinput_entity.setCompint(pcmint);

                    if(repaymentinput_entity.getOverid().equals("N")){
                        repaymentinput_entity.setIntamt(pitinty);
                        repaymentinput_entity.setOdintamt(podinty);
                        repaymentinput_entity.setLatechg(plchgy);

                    }
                    if(repaymentinput_entity.getOverid().equals("Y")||repaymentinput_entity.getOverid().equals("P")){
                        repaymentinput_entity.setIntamt(pitintx);
                        repaymentinput_entity.setOdintamt(podintx);
                        repaymentinput_entity.setLatechg(plchgx);
                    }

                } else {
                    System.out.println("Cannot get RepaymentSearchTx From Soa");
                }

            }

        }catch(Exception e){
            e.printStackTrace();
        }
        //soa 调用结束

        model.addAttribute("repaymentInput_entity", repaymentinput_entity);
        return "repaymentInput";
    }

//    @ResponseBody
//    @RequestMapping(value = "/repaymentInput_insertCheck",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
//    public String findAll(@RequestBody Map<String, Object> map){;
//        String refno = map.get("refno").toString();
//        String custcod = map.get("custcod").toString();
//        String instnof = map.get("instnof").toString();
//        String instnot = map.get("instnot").toString();
//        String status = map.get("status").toString();
//        insert_result = repaymentinput_service.insertCheck(refno,custcod,instnof,instnot,status);
//        return insert_result;
//    }

    @PostMapping("/repaymentInput")
    public String postRepaymentInputPage(@Valid @ModelAttribute("repaymentInput_entity") repaymentInput_entity repaymentinput_entity, BindingResult result,@RequestParam String refno,@RequestParam String custcod,@RequestParam String instnof,@RequestParam String instnot,@RequestParam String status, Model model) {
        model.addAttribute("repaymentInput_entity", repaymentinput_entity);
        String insert_result;
        insert_result = repaymentinput_service.insertCheck(refno,custcod,instnof,instnot,status);
        if (!(result.hasErrors())) {
            if (insert_result.equals("0")){
                repaymentinput_service.saveRepaymentInfo(repaymentinput_entity);
            }else {
                repaymentinput_service.updateRepaymentInfo(repaymentinput_entity);
            }
        }
        return "repaymentInput";
    }

    @RequestMapping("/repaymentInput/getSearchInfo")
    @ResponseBody
    public repaymentInput_entity findByRefno(@RequestParam String refno) {
        repaymentInput_entity result = repaymentinput_service.getRepaymentInfo(refno);
        return result;
    }

    //根据schetyp查询出schedes
    @ResponseBody
    @RequestMapping(value = "/repaymentInput_QueryBySchetyp",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public String findBySchetyp(@RequestBody Map<String, Object> map){
        String schetyp = map.get("schetyp").toString();
        String result = repaymentinput_service.QueryBySchetyp(schetyp);
        return result;
    }

    //根据refno查询出Payamt
    @ResponseBody
    @RequestMapping(value = "/repaymentInput_getPayamt",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public String getPayamtInfo(@RequestBody Map<String, Object> map){
        String refno = map.get("refno").toString();
        String result = repaymentinput_service.getPayamt(refno);
        return result;
    }

}

