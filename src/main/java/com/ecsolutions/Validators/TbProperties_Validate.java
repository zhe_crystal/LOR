package com.ecsolutions.Validators;

import com.ecsolutions.entity.TbProperties_Entity;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class TbProperties_Validate implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return TbProperties_Entity.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        TbProperties_Entity entity = (TbProperties_Entity) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "PropertiesId", "ValueRequired", "属性名不能为空");
        if (!entity.getPropertiesId().isEmpty()) {
            if (!entity.getPropertiesId().matches("^[A-Za-z0-9]+$")) {
                errors.rejectValue("PropertiesId", "Format", "请输入正确的属性Id(英文或者数字)");
            }
            if (entity.getPropertiesId().length() > 50) {
                errors.rejectValue("PropertiesId", "Length", "请输入50位以内的字符");
            }
            if (!entity.getPropertiesDesc().isEmpty()) {
                if (entity.getPropertiesDesc().length() > 75) {
                    errors.rejectValue("PropertiesDesc", "Length", "请输入75位以内的字符");
                }
            }
        }
    }
}
