package com.ecsolutions.entity;

public class TbProperties_Entity {

    private String PropertiesId;
    private String PropertiesDesc;

    public String getPropertiesId() {
        return PropertiesId;
    }

    public void setPropertiesId(String propertiesId) {
        PropertiesId = propertiesId;
    }

    public String getPropertiesDesc() {
        return PropertiesDesc;
    }

    public void setPropertiesDesc(String propertiesDesc) {
        PropertiesDesc = propertiesDesc;
    }
}
