package com.ecsolutions.controller;

import com.ecsolutions.entity.Paymenttemp;
import com.ecsolutions.entity.repaymentInput_entity;
import com.ecsolutions.service.RepaymentReviewService;
import com.ecsolutions.service.repaymentInput_Service;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 2017-09-05.
 */
@Controller
public class repaymentReviewSearchController {
    @Autowired
    private RepaymentReviewService repaymentReviewService;

    @GetMapping("/repaymentReviewSearch")
    public String getRepaymentInfo() {
        return "repaymentReviewSearch";
    }

    @PostMapping("/repaymentReviewSearch")
    public String postRepaymentInfo() {
        return "repaymentReviewSearch";
    }

    @RequestMapping(value = "/repaymentReviewSearch/Search", method = RequestMethod.GET)
    @ResponseBody
    public String searchResult(@RequestParam String dwndate, @RequestParam String matdate, @RequestParam String custcod, @RequestParam String refno, int limit, int offset) {
        Gson gson = new Gson();

        PageHelper.startPage(offset, limit);
        List<Paymenttemp> reuslt_list = repaymentReviewService.getResultList(dwndate, matdate, custcod, refno);
        int count_result = reuslt_list.size();
        String searchResultList = gson.toJson(reuslt_list);
        String errorCode = "0";
        return "{\"rowDatas\":" + searchResultList + ",\"dataLength\":" + count_result + ",\"errCode\":" + errorCode + "}";
    }
}
