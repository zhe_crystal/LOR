package com.ecsolutions.service;

import com.ecsolutions.entity.Customer_entity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/6/28.
 */
public interface Customer_Service {
    Customer_entity getPopupInfo();
    void saveCustomerEntity(Customer_entity customer_entity);
    String pidnoCheck(String pidno);
    List<HashMap<String,String>> getByMenleiInfo(String industrialClass);
    List<HashMap<String,String>> getByMenleiAndDaleiInfo(String industrialClass,String industrialPrimaryCategory);

    List<Customer_entity> getSearchInfoList(String PersonalOrEnterprise,
                                            String startDate,
                                            String endDate,
                                            String chineseName,
                                            String englishName,
                                            String licenseNumber,
                                            String creditCardNumber);

    Integer countSearchInfoList(String PersonalOrEnterprise,
                                String startDate,
                                String endDate,
                                String chineseName,
                                String englishName,
                                String licenseNumber,
                                String creditCardNumber);
}
