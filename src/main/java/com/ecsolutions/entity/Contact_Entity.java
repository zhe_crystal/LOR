package com.ecsolutions.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/7/31.
 */
public class Contact_Entity {
    private Integer id;
    private String bpm_no;
    private String custcode;
    private String clientname;
    private String contactperson;
    private String contactreason;
    private String remarks;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date contactdate;
    private String contacttime;
    private String contactuser;
    private String appointbooked;
    private String createdby;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date createdon;
    private String modifiedby;
    @DateTimeFormat(pattern = "yyyyMMdd")
    private Date modifiedon;
    private Integer clientname_state;
    private Integer contactperson_state;
    private Integer contactreason_state;
    private Integer remarks_state;
    private Integer contactdate_state;
    private Integer contacttime_state;
    private Integer contactuser_state;
    private Integer appointbooked_state;
    private String contactlocation;
    private Integer contactlocation_state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBpm_no() {
        return bpm_no;
    }

    public void setBpm_no(String bpm_no) {
        this.bpm_no = bpm_no;
    }

    public String getCustcode() {
        return custcode;
    }

    public void setCustcode(String custcode) {
        this.custcode = custcode;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getContactperson() {
        return contactperson;
    }

    public void setContactperson(String contactperson) {
        this.contactperson = contactperson;
    }

    public String getContactreason() {
        return contactreason;
    }

    public void setContactreason(String contactreason) {
        this.contactreason = contactreason;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getContactdate() {
        return contactdate;
    }

    public void setContactdate(Date contactdate) {
        this.contactdate = contactdate;
    }

    public String getContacttime() {
        return contacttime;
    }

    public void setContacttime(String contacttime) {
        this.contacttime = contacttime;
    }

    public String getContactuser() {
        return contactuser;
    }

    public void setContactuser(String contactuser) {
        this.contactuser = contactuser;
    }

    public String getAppointbooked() {
        return appointbooked;
    }

    public void setAppointbooked(String appointbooked) {
        this.appointbooked = appointbooked;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public String getModifiedby() {
        return modifiedby;
    }

    public void setModifiedby(String modifiedby) {
        this.modifiedby = modifiedby;
    }

    public Date getModifiedon() {
        return modifiedon;
    }

    public void setModifiedon(Date modifiedon) {
        this.modifiedon = modifiedon;
    }

    public Integer getClientname_state() {
        return clientname_state;
    }

    public void setClientname_state(Integer clientname_state) {
        this.clientname_state = clientname_state;
    }

    public Integer getContactperson_state() {
        return contactperson_state;
    }

    public void setContactperson_state(Integer contactperson_state) {
        this.contactperson_state = contactperson_state;
    }

    public Integer getContactreason_state() {
        return contactreason_state;
    }

    public void setContactreason_state(Integer contactreason_state) {
        this.contactreason_state = contactreason_state;
    }

    public Integer getRemarks_state() {
        return remarks_state;
    }

    public void setRemarks_state(Integer remarks_state) {
        this.remarks_state = remarks_state;
    }

    public Integer getContactdate_state() {
        return contactdate_state;
    }

    public void setContactdate_state(Integer contactdate_state) {
        this.contactdate_state = contactdate_state;
    }

    public Integer getContacttime_state() {
        return contacttime_state;
    }

    public void setContacttime_state(Integer contacttime_state) {
        this.contacttime_state = contacttime_state;
    }

    public Integer getContactuser_state() {
        return contactuser_state;
    }

    public void setContactuser_state(Integer contactuser_state) {
        this.contactuser_state = contactuser_state;
    }

    public Integer getAppointbooked_state() {
        return appointbooked_state;
    }

    public void setAppointbooked_state(Integer appointbooked_state) {
        this.appointbooked_state = appointbooked_state;
    }

    public String getContactlocation() {
        return contactlocation;
    }

    public void setContactlocation(String contactlocation) {
        this.contactlocation = contactlocation;
    }

    public Integer getContactlocation_state() {
        return contactlocation_state;
    }

    public void setContactlocation_state(Integer contactlocation_state) {
        this.contactlocation_state = contactlocation_state;
    }
}