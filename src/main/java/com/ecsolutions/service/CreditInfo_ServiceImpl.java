package com.ecsolutions.service;

import com.ecsolutions.dao.CreditInfo_DAO;
import com.ecsolutions.entity.CreditInfo_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/7/28.
 */
@Service("CreditInfo_Service")
public class CreditInfo_ServiceImpl implements  CreditInfo_Service{
    private CreditInfo_DAO creditInfo_dao ;
    @Autowired
    public CreditInfo_ServiceImpl(CreditInfo_DAO creditInfo_dao) {
        this.creditInfo_dao= creditInfo_dao;
    }

    @Override
    public CreditInfo_Entity getPopupInfo(String custcode){
        CreditInfo_Entity result = new CreditInfo_Entity();
        List<HashMap<String,String>> creditInfoList = creditInfo_dao.getCreditInfoList(custcode);
        List<HashMap<String,String>> CCYList = creditInfo_dao.getCCY();
        result.setCriditInfoList(creditInfoList);
        result.setCCYList(CCYList);
        return result;
    }
    @Override
    public void saveCreditInfoEntity(String custcode,CreditInfo_Entity creditInfo_entity){

        creditInfo_dao.insert(custcode,creditInfo_entity);
    }

    @Override
    public void updateCreditInfoEntity(CreditInfo_Entity updateCreditInfo_entity){

        creditInfo_dao.update(updateCreditInfo_entity);
    }
    @Override
    public void deleteCreditInfoEntity(String deleteId){
        creditInfo_dao.delete(deleteId);
    };

}
