package com.ecsolutions.dao;

import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface Hkbinmas_DAO {
    @Select("SELECT cusnm FROM Hkbinmas Where TRIM(refno) = #{refnum,jdbcType=VARCHAR}")
    @ResultType(String.class)
    public String getCustnm(String refnum);

}
