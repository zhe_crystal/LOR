package com.ecsolutions.controller;

import com.ecsolutions.entity.CreditAmountRecheck_Entity;
import com.ecsolutions.service.CreditAmountRecheck_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
public class CreditAmountRecheckPage_Controller {
    @Autowired
    private CreditAmountRecheck_Service creditAmountRecheck_service;

    @GetMapping("/CreditAmountRecheckPage")
    public String getCreditAmountRecheckPage(Model model, @RequestParam String refnum) {
        List<CreditAmountRecheck_Entity> list = creditAmountRecheck_service.getSearchInfoResultList("", refnum, "", "");
        CreditAmountRecheck_Entity entity = list.get(0);
        String reason = entity.getPdgrsn().substring(0, 8);
        String replaceText = "";
        char[] deal = reason.toCharArray();
        boolean state = false;
        replaceText += "复核原因列表:\n";
        for (int i = 0; i < deal.length; i++) {
            if (deal[i] == "Y".charAt(0)) {
                state = true;
                if (i == 0) {
                    replaceText += i + 1 + ":额度超额;\n";
                } else if (i == 1) {
                    replaceText += i + 1 + ":期数超过最大期数;\n";
                } else if (i == 2) {
                    replaceText += i + 1 + ":国家风险限制;\n";
                } else if (i == 3) {
                    replaceText += i + 1 + ":额度超过期限;\n";
                } else if (i == 4) {
                    replaceText += i + 1 + ":RESTRICT/REFER TX;\n";
                } else if (i == 5) {
                    replaceText += i + 1 + ":O/DUE ITEM;\n";
                } else if (i == 6) {
                    replaceText += i + 1 + ":CR SPEC. COND;\n";
                } else if (i == 7) {
                    replaceText += i + 1 + ":EXP DATE EXCESS;\n";
                }
            }
        }
        if (!state)
            replaceText += "无";
        entity.setPdgrsn(replaceText);
        model.addAttribute("creditAmountRecheck_entity", entity);
        return "ClientInformation/CreditAmountRecheckPage";
    }

    @PostMapping("/CreditAmountRecheckPage")
    public String postCreditAmountRecheckPage(Model model, @ModelAttribute CreditAmountRecheck_Entity creditAmountRecheck_entity) {
        model.addAttribute("creditAmountRecheck_entity", creditAmountRecheck_entity);
        creditAmountRecheck_service.saveRecord(creditAmountRecheck_entity);
        return "ClientInformation/CreditAmountRecheckPage";
    }

    @RequestMapping("/CreditAmountRecheckPage/getFirstApproverName")
    @ResponseBody
    public String getFirstApproverName(@RequestParam String UserIdCode) {
        return creditAmountRecheck_service.getFirstApproverName(UserIdCode);
    }

    @RequestMapping("/CreditAmountRecheckPage/getCustnm")
    @ResponseBody
    public String getCustnm(@RequestParam String refnum) {
        return creditAmountRecheck_service.getCustnm(refnum);
    }

}
