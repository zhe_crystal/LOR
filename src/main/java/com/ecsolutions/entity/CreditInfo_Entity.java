package com.ecsolutions.entity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/7/28.
 */
public class CreditInfo_Entity {
    private String ID;
    private String line_no;
    private String Rating;
    private String OtherOrgName;
    private String OtherOrgeRating;
    private String HaveBadRecord;
    private String TotalSecurityCCY;
    private String TotalSecurity;
    private List<HashMap<String,String>> CCYList;
    private List<HashMap<String,String>> criditInfoList;

    public List<HashMap<String, String>> getCCYList() {
        return CCYList;
    }

    public void setCCYList(List<HashMap<String, String>> CCYList) {
        this.CCYList = CCYList;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public List<HashMap<String, String>> getCriditInfoList() {
        return criditInfoList;
    }

    public void setCriditInfoList(List<HashMap<String, String>> criditInfoList) {
        this.criditInfoList = criditInfoList;
    }

    public String getLine_no() {
        return line_no;
    }

    public void setLine_no(String line_no) {
        this.line_no = line_no;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getOtherOrgName() {
        return OtherOrgName;
    }

    public void setOtherOrgName(String otherOrgName) {
        OtherOrgName = otherOrgName;
    }

    public String getOtherOrgeRating() {
        return OtherOrgeRating;
    }

    public void setOtherOrgeRating(String otherOrgeRating) {
        OtherOrgeRating = otherOrgeRating;
    }

    public String getHaveBadRecord() {
        return HaveBadRecord;
    }

    public void setHaveBadRecord(String haveBadRecord) {
        HaveBadRecord = haveBadRecord;
    }

    public String getTotalSecurityCCY() {
        return TotalSecurityCCY;
    }

    public void setTotalSecurityCCY(String totalSecurityCCY) {
        TotalSecurityCCY = totalSecurityCCY;
    }

    public String getTotalSecurity() {
        return TotalSecurity;
    }

    public void setTotalSecurity(String totalSecurity) {
        TotalSecurity = totalSecurity;
    }



}
