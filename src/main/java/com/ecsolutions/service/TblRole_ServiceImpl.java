package com.ecsolutions.service;

import com.ecsolutions.dao.TblRole_DAO;
import com.ecsolutions.dao.User_DAO;
import com.ecsolutions.dao.tbluser_role_DAO;
import com.ecsolutions.entity.TblRole_Entity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service("TblRole_Service")
public class TblRole_ServiceImpl implements TblRole_Service {
    private TblRole_DAO tblRole_dao;
    private User_DAO user_dao;
    private tbluser_role_DAO tbluser_role_dao;


    public TblRole_ServiceImpl(TblRole_DAO tblRole_dao, User_DAO user_dao, tbluser_role_DAO tbluser_role_dao) {
        this.user_dao=user_dao;
        this.tblRole_dao = tblRole_dao;
        this.tbluser_role_dao=tbluser_role_dao;
    }

    @Override
    public List<TblRole_Entity> getHistoryList() {
        return tblRole_dao.getHistoryList();
    }

    @Override
    public boolean saveTblRole(TblRole_Entity entity) {
        return tblRole_dao.saveTblRole(entity);
    }

    @Override
    public boolean saveTblUserRole(String UserId,String RoleId) {
        return tbluser_role_dao.saveUserRole( UserId, RoleId);
    }

    @Override
    public boolean updateTblRole(TblRole_Entity entity) {
        return tblRole_dao.updateTblRole(entity);
    }

    @Override
    public boolean deleteTblRole(String roleid) {
        return tblRole_dao.deleteTblRole(roleid);
    }

    @Override
    public Integer countTblRole() {
        return tblRole_dao.countHistoryList();
    }

    @Override
    public boolean isRecord(String roleid) {
        if (tblRole_dao.getOne(roleid) > 0)
            return true;
        else
            return false;
    }

    @Override
    public List<HashMap<String,String>> getAllUsers() {
        return user_dao.getAllUserId();
    }

    @Override
    public List<HashMap<String, String>> getUserinOneRole(String roleid) {
        return tbluser_role_dao.getUserinOneRole(roleid);
    }

    @Override
    public List<String> getOneUsersRole(String userid) {
        return null;
    }

    @Override
    public boolean deleteTblUser_Role(String userid, String roleid) {
        return tbluser_role_dao.deleteUserRole(userid,roleid);
    }
}
