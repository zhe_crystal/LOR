package com.ecsolutions.controller;

import com.ecsolutions.entity.ApplySearchResult;
import com.ecsolutions.entity.ApplySearch_Entity;
import com.ecsolutions.service.ApplySearch_Service;
import com.github.pagehelper.PageHelper;
import com.google.gson.Gson;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Administrator on 2017/7/31.
 */
@Controller
public class ApplySearch_Controller {
    @Autowired
    private ApplySearch_Service applysearch_service;
    public ApplySearch_Service getApplySearch_Service() {
        return applysearch_service;
    }

    @GetMapping("/applySearch")
    public String getApplyPage(Model model) {
        model.addAttribute("applysearch_info", applysearch_service.getPopupInfo());
        return "applySearch";
    }


    @GetMapping("/applySearchResult")
    public String getApplySearchResultPage(Model model,   @RequestParam("custcode") String custcode) {
        System.out.println(custcode);
        model.addAttribute("custcode",custcode);
        return "applySearchResult";
    }

    @ResponseBody
    @RequestMapping(value = "/applySearchSearch",method = RequestMethod.GET)
    public String findByEthnic(@RequestParam String startDate,@RequestParam String endDate,@RequestParam String personalFlag,@RequestParam String status,@RequestParam String manager,@RequestParam String custCod,@RequestParam String lastname,@RequestParam String lregno,int pageSize,int pageIndex){
        //Pledge_Entity pledge_entity = pledgeNew_service.getByEthnic(ethnic);
        //return pledge_entity;
//        System.out.println(pageIndex+""+pageSize);
        PageHelper.startPage(pageIndex, pageSize);
        List<ApplySearchResult> listResult = applysearch_service.Search( startDate, endDate, personalFlag, status, manager, custCod, lastname, lregno);

        Integer total = listResult.size();
        String errorCode = "0";
        Gson gson = new Gson();

        String historyList = gson.toJson(listResult);

//        String historyList =JSONArray.fromObject(listResult).toString();
        System.out.println( "{\"rowDatas\":" +JSONArray.fromObject(listResult).toString() + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}");
        System.out.println( "{\"rowDatas\":" +historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}");

//        return JSONArray.fromObject(listResult).toString();
        return "{\"rowDatas\":" +historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}";
//        return "{\"rowDatas\":" + historyList + ",\"dataLength\":" + total + ",\"errCode\":" + errorCode + "}";


    }

}
