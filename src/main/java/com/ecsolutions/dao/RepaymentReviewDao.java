package com.ecsolutions.dao;

import com.ecsolutions.dao.SqlProvider.RepaymentReviewProvider;
import com.ecsolutions.entity.Paymenttemp;
import com.ecsolutions.entity.repaymentInput_entity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Administrator on 2017-09-05.
 */
@Repository
public interface RepaymentReviewDao {
    @SelectProvider(type = RepaymentReviewProvider.class, method = "getResultList")
    @ResultType(repaymentInput_entity.class)
    List<Paymenttemp> getResultList(String dwndate, String matdate, String custcod, String refno);

    @Update("update paymenttemp set status ='F' where refno =#{refno}")
    void savaRepaymentInputInfo(Paymenttemp paymenttemp);

    @Update("update hkbmamas " +
            "set Rtamt=(case when(rtamt!=0) then (#{payamt}+rtamt) else #{payamt}+0 end) ," +
            "Osbamt=(case when(osbamt -#{payamt}<0)then 0 else (osbamt -#{payamt}) end)," +
            "Txccy=#{lnccy}," +
            "Txamt=#{payamt} where prodref=#{refno} ")
    void udpateHkbmamas(Paymenttemp paymenttemp);

    @Insert("insert into HKBINPYH(refno,paydate,paytime,paytrev,custcod,custref,payeffd,payerev,paydued,payccy,payamt ,newosam,lnosccy,lnosamt,lnccy,lnamt,regprn,regint,dlqprn,dlqint,odint,latchg,penchg,instfr,instto,branch,prounit,func,flidate,fliuser,adate,auser,lapppu,dracno1,dracno2,status1,status2,stppaid)" +
            "values(" +
            "#{arg0.refno}," +
            "(select to_char(sysdate,'yyyyMMdd') from dual)," +
            "to_number((select to_char(sysdate,'hh24miss') from dual),'999999')," +
            "0," +
            "#{arg0.custcod}," +
            "' '," +
            "0," +
            "0," +
            "0," +
            "#{arg0.lnccy}," +
            "#{arg0.payamt,jdbcType=NUMERIC}," +
            "0," +
            "0," +
            "0," +
            "' '," +
            "0," +
            "#{arg0.prinamt,jdbcType=NUMERIC}," +
            "#{arg0.intamt,jdbcType=NUMERIC}," +
            "0," +
            "0," +
            "#{arg0.odintamt,jdbcType=NUMERIC}," +
            "#{arg0.odadjamt,jdbcType=NUMERIC}," +
            "0," +
            "#{arg0.instnof,jdbcType=INTEGER}," +
            "#{arg0.instnot,jdbcType=INTEGER}," +
            "(select branch FROM tblusers where userid=#{arg1})," +
            "' '," +
            "' '," +
            "to_char(to_date(#{arg0.inpdate},'yyyy-mm-dd hh24:mi:ss'),'yyyyMMdd')," +
            "#{arg0.inpuser,jdbcType=VARCHAR}," +
            "(select to_char(sysdate,'yyyyMMdd') from dual)," +
            "#{arg1}," +
            "' '," +
            "' '," +
            "' '," +
            "(case when (#{arg0.margflag}='Y') then '2' else '1' end)," +
            "' '," +
            "0" +
            ")")
    void insertHkbinpyh(Paymenttemp paymenttemp,String userid);
    //arg0.inpdate的格式有变会报，无效数字的错
}
