package com.ecsolutions.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class ApplyPagePersonal_Controller {

    private String CustCode="000000";

    @GetMapping("/ApplyPagePersonal")
    public String getApplyPagePersonal(@RequestParam("CustCode") String CustCode) {
        this.CustCode=CustCode;
        return "ClientInformation/ApplyPagePersonal";
    }

    @RequestMapping(value="/ApplyPagePersonal/getCustCode",method = RequestMethod.GET)
    @ResponseBody
    public String getCustCode(){
        return CustCode;
    }
}
