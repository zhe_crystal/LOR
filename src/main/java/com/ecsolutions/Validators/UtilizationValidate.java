package com.ecsolutions.Validators;

import com.ecsolutions.dao.Utilization_Dao;
import com.ecsolutions.entity.Facility_Entity;
import com.ecsolutions.entity.Utilization_Entity;
import com.ecsolutions.service.Utilization_Service;
import com.ecsolutions.service.Utilization_ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2017/7/26.
 * 使用信息校验
 */
@Component
public class UtilizationValidate implements Validator {

    private Utilization_Service utilization_service;

    @Autowired
    public UtilizationValidate(Utilization_Service utilization_service){
        this.utilization_service = utilization_service;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Utilization_Entity.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Utilization_Entity utilization_entity = (Utilization_Entity) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lineno", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "drawdate", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "schetype", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "drawccy", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "drawamt", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "ratetype", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "overdurate", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "paymentmethod", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tenor", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fixflag", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "payfreq", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "payper", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstpaydate", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "collflag", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "docflag", "amountrequired", "不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accoffic", "amountrequired", "不能为空");
        //信贷员操作日期不能大于放贷日期
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
        Date drawdate = utilization_entity.getDrawdate();//放款日
        Date anndate = utilization_entity.getAnndate();//信贷员分析日
        if(drawdate!=null && anndate!=null){
            Integer drawdatenew=Integer.valueOf(sdf.format(drawdate));
            Integer anndatenew=Integer.valueOf(sdf.format(anndate));
            if(anndatenew>drawdatenew){
                errors.rejectValue("anndate","ddd","日期不能大于放款日");
            }
        }
        //放款币种必须和额度币种相同
        String custcod = utilization_entity.getCustcod();
        String lineno = utilization_entity.getLineno();
        String drawccy = utilization_entity.getDrawccy();
        if(drawccy!="" && lineno!=""){
            Facility_Entity facility_entity = utilization_service.getFacilityInfoByCustcodAndLineno(custcod,lineno);
            if(!drawccy.equals(facility_entity.getLineccy())){
                errors.rejectValue("drawccy","ddd","放款币种必须和额度币种相同");
            }
        }
        //放款金额不能大于额度可用金额
        Integer drawamt = utilization_entity.getDrawamt();
        if(drawamt!=null && lineno!=""){
            if(drawamt>utilization_entity.getAvliamt()){
                errors.rejectValue("drawamt","ddd","放款金额不能大于额度可用金额");
            }
        }
        //还款方式为：一次性还本付息，则首次还款日为到期日；其他方式时，首次还款日大于等于放款日期。
        String paymentmethod = utilization_entity.getPaymentmethod();
        Date firstpaydate = utilization_entity.getFirstpaydate();//首次还款日
        if(paymentmethod!="" && firstpaydate!=null && drawdate!=null){
            Integer firstpaydatenew=Integer.valueOf(sdf.format(firstpaydate));
            Integer drawdatenew=Integer.valueOf(sdf.format(drawdate));//放款日
            if(firstpaydatenew<drawdatenew){
                errors.rejectValue("firstpaydate","ddd","首次还款日不能小于放款日");
            }
        }
        //顾问费支付日如果有值需大于等于操作日
        Date chgdate = utilization_entity.getChgdate();//顾问费支付日
        if(chgdate!=null && anndate!=null){
            Integer chgdatenew=Integer.valueOf(sdf.format(chgdate));
            Integer anndatenew=Integer.valueOf(sdf.format(anndate));
            if(chgdatenew<anndatenew){
                errors.rejectValue("chgdate","ddd","日期不能小于信贷员操作日");
            }
        }
        //借款合同签订日如果有值需大于等于操作日
        Date  signeddate = utilization_entity.getSigneddate();
        if(signeddate!=null && anndate!=null){
            Integer signeddatenew=Integer.valueOf(sdf.format(signeddate));
            Integer anndatenew=Integer.valueOf(sdf.format(anndate));
            if(signeddatenew<anndatenew){
                errors.rejectValue("signeddate","ddd","日期不能小于信贷员操作日");
            }
        }

    }
}
