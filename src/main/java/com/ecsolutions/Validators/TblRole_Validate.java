package com.ecsolutions.Validators;

import com.ecsolutions.entity.TblRole_Entity;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class TblRole_Validate implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return TblRole_Entity.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        TblRole_Entity entity = (TblRole_Entity) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "roleid", "ValueRequired", "组名不能为空");
        if (!entity.getRoleid().isEmpty()) {
            if (!entity.getRoleid().matches("^[\\u4E00-\\u9FA5A-Za-z0-9_]+$"))
                errors.rejectValue("roleid", "Format", "请输入正确的组名（中文英文或者数字,可含下划线）");
            if (entity.getRoleid().length() > 15)
                errors.rejectValue("roleid", "Length", "请输入15位以内的字符");
            if (!entity.getRoledesc().isEmpty()) {
                if (entity.getRoledesc().length() > 25)
                    errors.rejectValue("roledesc", "Length", "请输入25位以内的字符");
            }
        }
    }
}
