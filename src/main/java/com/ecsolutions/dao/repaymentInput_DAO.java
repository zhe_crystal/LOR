package com.ecsolutions.dao;

import com.ecsolutions.dao.SqlProvider.repaymentInput_Provider;
import com.ecsolutions.entity.repaymentInput_entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/8/30.
 */
@Repository
public interface repaymentInput_DAO {
    @SelectProvider(type = repaymentInput_Provider.class, method = "getResultList")
    @ResultType(repaymentInput_entity.class)
    List<repaymentInput_entity> getResultList(String dwndate, String matdate, String custcod, String refno);

    @SelectProvider(type = repaymentInput_Provider.class, method = "countResultList")
    @ResultType(Integer.class)
    Integer countResultList(String dwndate, String matdate, String custcod, String refno);

    @Select("SELECT (prinamt+intamt+odintamt+odadjamt+compint+latechg+lchgadj) FROM paymenttemp Where refno = #{refno}")
    @ResultType(String.class)
    String countPayment(String refno);

    @Select("SELECT refno,custcod,cusnm FROM hkbinmas Where TRIM(refno) = #{refno,jdbcType=VARCHAR}")
    @ResultType(repaymentInput_entity.class)
    repaymentInput_entity getInfoByRefno(String refno);

    @Select("SELECT schedes FROM hkbinsch Where schetyp = #{schetyp}")
    @ResultType(String.class)
    String getSchedes(String schetyp);

    @Select("SELECT osbamt FROM hkbmamas Where TRIM(refno) = TRIM(#{refno})")
    @ResultType(String.class)
    String getOsbamt(String refno);

    @Select("SELECT count(*) FROM paymenttemp WHERE refno = #{refno} and custcod = #{custcod} and instnof = #{instnof} and instnot = #{instnot} and status = #{status}")
    @ResultType(String.class)
    String checkinsert(@Param("refno")String refno, @Param("custcod")String custcod, @Param("instnof")String instnof, @Param("instnot")String instnot, @Param("status")String status);

    @Insert("insert into paymenttemp(refno,custcod,custnm,custref,schetyp,schedes,lnccy,lnamt,overid,instnof,instnot,prinamt,setord1,intamt,setord2,odintamt,setord3,odadjamt,compint,latechg,setord4,lchgadj,margflag,payamt,inpdate,inpuser,status) "
            +"values(#{refno,jdbcType=VARCHAR},#{custcod,jdbcType=VARCHAR},#{cusnm,jdbcType=VARCHAR},#{custref,jdbcType=VARCHAR},#{schetyp,jdbcType=VARCHAR},#{schedes,jdbcType=VARCHAR},#{lnccy,jdbcType=VARCHAR},to_number(#{lnamt,jdbcType=VARCHAR}),#{overid,jdbcType=VARCHAR},"
            + "to_number(#{instnof,jdbcType=VARCHAR}),to_number(#{instnot,jdbcType=VARCHAR}),to_number(#{prinamt,jdbcType=VARCHAR}),#{setord1,jdbcType=VARCHAR},to_number(#{intamt,jdbcType=VARCHAR}),#{setord2,jdbcType=VARCHAR},"
            + "to_number(#{odintamt,jdbcType=VARCHAR}),#{setord3,jdbcType=VARCHAR},to_number(#{odadjamt,jdbcType=VARCHAR}),to_number(#{compint,jdbcType=VARCHAR}),to_number(#{latechg,jdbcType=VARCHAR}),#{setord4,jdbcType=VARCHAR},"
            +"to_number(#{latechg,jdbcType=VARCHAR}),#{margflagString,jdbcType=VARCHAR},to_number(#{payamt,jdbcType=VARCHAR}),(select sysdate from dual),#{inpuser,jdbcType=VARCHAR},#{status,jdbcType=VARCHAR})")
    void insert(repaymentInput_entity repaymentinput_entity);

    @Update("Update paymenttemp set refno=#{refno,jdbcType=VARCHAR}," + "custcod=#{custcod,jdbcType=VARCHAR}," + "custnm=#{cusnm,jdbcType=VARCHAR}," + "custref=#{custref,jdbcType=VARCHAR}," +
            "schetyp=#{schetyp,jdbcType=VARCHAR}," + "schedes=#{schedes,jdbcType=VARCHAR}," + "lnccy=#{lnccy,jdbcType=VARCHAR}," + "lnamt=to_number(#{lnamt,jdbcType=VARCHAR})," +
            "lnosccy=#{lnosccy,jdbcType=VARCHAR}," + "lnosamt=to_number(#{lnosamt,jdbcType=VARCHAR})," + "instnof=to_number(#{instnof,jdbcType=VARCHAR})," + "instnot=to_number(#{instnot,jdbcType=VARCHAR})," +
            "prinamt=to_number(#{prinamt,jdbcType=VARCHAR})," + "setord1=#{setord1,jdbcType=VARCHAR}," + "intamt=to_number(#{intamt,jdbcType=VARCHAR})," + "setord2=#{setord2,jdbcType=VARCHAR}," +
            "odintamt=to_number(#{odintamt,jdbcType=VARCHAR})," + "setord3=#{setord3,jdbcType=VARCHAR}," + "odadjamt=to_number(#{odadjamt,jdbcType=VARCHAR})," + "compint=to_number(#{compint,jdbcType=VARCHAR})," +
            "latechg=to_number(#{latechg,jdbcType=VARCHAR})," + "setord4=#{setord4,jdbcType=VARCHAR}," + "lchgadj=to_number(#{lchgadj,jdbcType=VARCHAR})," + "margflag=#{margflagString,jdbcType=VARCHAR}," +
            "payamt=to_number(#{payamt,jdbcType=VARCHAR})," + "inpdate=(select sysdate from dual)," + "inpuser=#{inpuser,jdbcType=VARCHAR}," + "status=#{status,jdbcType=VARCHAR}" +
            "Where refno=#{refno} and custcod=#{custcod} and instnof=to_number(#{instnof,jdbcType=VARCHAR}) and instnot=to_number(#{instnot,jdbcType=VARCHAR}) and status=#{status}")
    void update(repaymentInput_entity repaymentinput_entity);
}
