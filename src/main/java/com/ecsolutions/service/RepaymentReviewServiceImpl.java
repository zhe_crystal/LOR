package com.ecsolutions.service;

import com.ecsolutions.dao.RepaymentReviewDao;
import com.ecsolutions.dao.repaymentInput_DAO;
import com.ecsolutions.entity.Paymenttemp;
import com.ecsolutions.entity.repaymentInput_entity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017-09-05.
 */
@Service("RepaymentReviewService")
public class RepaymentReviewServiceImpl implements RepaymentReviewService{
    private RepaymentReviewDao repaymentReviewDao;

    public RepaymentReviewServiceImpl(RepaymentReviewDao repaymentReviewDao) {
        this.repaymentReviewDao = repaymentReviewDao;
    }

    @Override
    public List<Paymenttemp> getResultList(String dwndate, String matdate, String custcod, String refno){
        List<Paymenttemp> result = repaymentReviewDao.getResultList(dwndate,matdate,custcod,refno);
        return result;
    }

    @Override
    public void saveRepaymentReviewInfo(Paymenttemp paymenttemp){

        repaymentReviewDao.savaRepaymentInputInfo(paymenttemp);
        repaymentReviewDao.insertHkbinpyh(paymenttemp,"1");
        if(paymenttemp.getMargflag().equals("Y")){
            repaymentReviewDao.udpateHkbmamas(paymenttemp);
        }



    };
}
