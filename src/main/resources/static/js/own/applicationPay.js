function Format(value){
    if (value != null) {
        var date = new Date(parseInt(value));
        var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
        var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        return date.getFullYear()  + month  + currentDate;
    }
}

$(function() {

    $(":radio[name='paytype'][value='2']").click(function () {
        $('#divTable').show();
    });

    $(":radio[name='paytype'][value='1']").click(function () {
        $('#divTable').hide();
    });

    $(":radio[name='paytype'][value='3']").click(function () {
        $('#divTable').hide();
    });


    $('#btn_select').click(function () {
        $('#duiname').val("");
        $('#duibranch').val("");
        var custcode = $('#duicustcd').val();
        if(custcode==""){
            $('#span1').attr("hidden", false);
        }else{
            $('#span1').attr("hidden", true);
            $.ajax({
                type: "POST",
                url: "/selectDuicustcd",
                dataType: "json",
                data: {custcode : custcode},
                async: false,
                success: function(result){
                    $('#duiname').val(result.LASTNAME);
                    $('#duibranch').val(result.BANKCODE);
                },
            });
        }
    });

    $('#a_saveOne').click(function () {
        var loantype = $('input:radio[name="loantype"]:checked').val();
        var loancatg = $('input:radio[name="loancatg"]:checked').val();
        var purpdesc = $('#purpdesc').val();
        var paytype = $('input:radio[name="paytype"]:checked').val();
        if(loantype==null || loancatg==null || purpdesc=="" || paytype==null){
            alert("信息未填写完整");
        }else {
            var formOne = $('#formOne').serialize();
            var params = decodeURIComponent(formOne, true);
            $.ajax({
                type: "POST",
                url: "/saveLoanapplicationPayInfo",
                data: params,
                async: false,
                success: function (result) {
                    alert(result);
                },
            });
        }
    });

    $('#a_saveTwo').click(function () {
        var duiflag = $('input:radio[name="duiflag"]:checked').val();
        var payamt = $('#payamt').val();
        var paydate = $('#paydate').val();
        if(duiflag!=null && payamt!="" && paydate!=""){
            if(duiflag=='1'){
                var duicustcd = $('#duicustcd').val();
                var duibranch = $('#duibranch').val();
                var duiaccno = $('#duiaccno').val();
                var duiname = $('#duiname').val();
                if(duicustcd=="" || duibranch=="" || duiaccno=="" || duiname==""){
                    alert("行内信息未填写完整");
                }else{
                    var formTwo = $('#formTwo').serialize();
                    var params = decodeURIComponent(formTwo, true);
                    $.ajax({
                        type: "POST",
                        url: "/saveLoanapplicationPayInfodet",
                        data: params,
                        async: false,
                        success: function (result) {
                            $('#LoanpaymentinfoTable').bootstrapTable("refresh", {
                                silent: true
                            });
                            $('#id').val(result);
                            alert("保存成功");
                        },
                    });
                }
            }
            if(duiflag=='2'){
                var waibranch = $('#waibranch').val();
                var waibrname = $('#waibrname').val();
                var waiaccno = $('#waiaccno').val();
                var wainame = $('#wainame').val();
                if(waibranch=="" || waibrname=="" || waiaccno=="" || wainame==""){
                    alert("行外信息未填写完整");
                }else{
                    var formTwo = $('#formTwo').serialize();
                    var params = decodeURIComponent(formTwo, true);
                    $.ajax({
                        type: "POST",
                        url: "/saveLoanapplicationPayInfodet",
                        data: params,
                        async: false,
                        success: function (result) {
                            $('#LoanpaymentinfoTable').bootstrapTable("refresh", {
                                silent: true
                            });
                            $('#id').val(result);
                            alert("保存成功");
                        },
                    });
                }
            }
        }else {
            alert("请填写完交易对手标识、支付时间、支付金额");
        }
    });

    $('#a_delete').click(function () {
        if(confirm("确定要删除记录?")) {
            var id = $('#id').val();
            $.ajax({
                type: "POST",
                url: "/deleteLoanpaymentInfo",
                data: {id: id},
                async: false,
                success: function (result) {
                    $('#LoanpaymentinfoTable').bootstrapTable("refresh", {
                        silent: true
                    });
                    alert(result);
                    $('#a_add').trigger("click");
                }
            });
        }
    });

    //当其采取后台分页的时候，在这里把所需要的参数传给后台
    function queryParams(params) { // 配置参数
        var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
            // limit : params.limit, // 页面大小
            // offset : params.offset, // 页码
            // sortname : params.sort, // 排序列名
            // sortorder : params.order// 排序规则
            custcode : $("#custcode").val(),
            loanref : $('#loanref').val()
        };
        return temp;
    }

    $("#LoanpaymentinfoTable").bootstrapTable({
        method : "post",// 请求方式（*）
        url: "/LoanpaymentinfoList",// 请求后台的URL（*）
        queryParams : queryParams,// 传递参数（*)
        uniqueId : "id",// 唯一标识
        sidePagination : 'client',// 分页方式：client客户端分页，server服务端分页（*）
        sortName : "id",// 排序字段
        sortOrder : "desc",// 排序方式
        pageList : [ 5, 10, 15, 20, 25, 30 ],// 可供选择的每页的行数（*）
        pageNumber : 1,// 初始化加载第一页，默认第一页
        pagination : true,// 是否显示分页（*）
        pageSize : 5,// 每页的记录行数（*）
        showColumns : true, // 是否显示所有的列
        toolbar : "#toolbar",// 工具按钮用哪个容器
        clickToSelect : true,// 是否启用点击选中行
        cache : false,// 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        striped : false,// 是否显示行间隔色
        showRefresh : true,// 是否显示刷新按钮
        showToggle : true,// 是否显示详细视图和列表视图的切换按钮
        cardView : false,// 是否显示详细视图
        detailView : false,// 是否显示父子表
        paginationLoop : false,// 是否允许循环分页
        paginationPreText : "上一页",// 指定上一页按钮文字，不配置默认<
        paginationNextText : "下一页",
        columns : [{
            field: 'id',
            title: '序号'
        }, {
            field: 'payamt',
            title: '支付金额'
        }, {
            field : 'paydate',
            title : '支付时间',
            formatter:function(value,row,index){
                if (value != null) {
                    var date = new Date(parseInt(value));
                    var month = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
                    var currentDate = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
                    return date.getFullYear()  + month  + currentDate;
                }
            }
        }, {
            field : 'duiflag',
            title : '行内/行外',
            formatter:function(value,row,index){
                if(value=='1'){
                    return "行内";
                }else if(value=='2'){
                    return "行外";
                }
            }
        }, {
            field : 'duibranch',
            title : '分/支行(行内)'
        }, {
            field : 'waibrname',
            title : '交易对手开户行名称（行外）'
        }, {
            field : 'duicustcd',
            title : '客户号(行内)'
        },{
            field : 'cityflag',
            title : '是否同城',
            formatter:function(value,row,index){
                if(value=='Y'){
                    return "是";
                }else if(value=='N'){
                    return "否";
                }
            }
        }],

        onClickRow : function(row) {
            $(":radio[name='duiflag'][value='1']").attr("checked",false);
            $(":radio[name='duiflag'][value='2']").attr("checked",false);
            //通用信息
            $("#id").val(row.id);
            if(row.duiflag=='1'){
                $(":radio[name='duiflag'][value='1']").attr("checked",true);
            }
            if(row.duiflag=='2'){
                $(":radio[name='duiflag'][value='2']").attr("checked",true);
            }
            $("#cityflag").val(row.cityflag);
            $("#payamt").val(row.payamt);
            var paydate = Format(row.paydate);
            $("#paydate").val(paydate);
            $("#duicustcd").val(row.duicustcd);
            $("#duibranch").val(row.duibranch);
            $("#duiaccno").val(row.duiaccno);
            $("#duiname").val(row.duiname);
            $("#waibranch").val(row.waibranch);
            $("#waibrname").val(row.waibrname);
            $("#waiaccno").val(row.waiaccno);
            $("#wainame").val(row.wainame);
        }
    });

    $("#a_add").click(function () {
        $("#id").val("");
        $(":radio[name='duiflag'][value='1']").attr("checked",false);
        $(":radio[name='duiflag'][value='2']").attr("checked",false);
        $("#payamt").val("");
        $("#paydate").val("");
        $("#duicustcd").val("");
        $("#duibranch").val("");
        $("#duiaccno").val("");
        $("#duiname").val("");
        $("#waibranch").val("");
        $("#waibrname").val("");
        $("#waiaccno").val("");
        $("#wainame").val("");
    });

    $('#paydate').datepicker({
        format: 'yyyymmdd',
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked',
        language: 'zh-CN'
    }).on('changeDate',function(ev){

    });

});