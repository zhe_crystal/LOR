package com.ecsolutions.service;

import com.ecsolutions.dao.Appointment_DAO;
import com.ecsolutions.dao.Contact_DAO;
import com.ecsolutions.entity.Appointment_Entity;
import com.ecsolutions.entity.Contact_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/7/24.
 */
@Service("Appointment_Service")
public class Appointment_ServiceImpl implements Appointment_Service {
    private Appointment_DAO appointment_dao;

    @Autowired
    public Appointment_ServiceImpl(Appointment_DAO appointment_dao) {
        this.appointment_dao = appointment_dao;
    }


    @Override
    public List<Appointment_Entity> getAppointmentInfo(String custcode, String bpm_no) {
        return appointment_dao.getByCustcodeAndBpm_no(custcode,bpm_no);
    }

    @Override
    public Integer getContactId(String custcode, String bpm_no) {
        return appointment_dao.getIdByCustcodeAndBpm_no(custcode,bpm_no);
    }

    @Override
    public void saveAppointmentInfo(Appointment_Entity appointment_entity) {
        appointment_dao.saveAppointmentInfo(appointment_entity);
    }

    @Override
    public void updateAppointmentInfo(Appointment_Entity appointment_entity) {
        appointment_dao.updateAppointmentInfo(appointment_entity);
    }

    @Override
    public void deleteAppointmentInfo(Integer id) {
        appointment_dao.deleteAppointmentInfo(id);
    }
}

