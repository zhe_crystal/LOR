package com.ecsolutions.service;

import com.ecsolutions.entity.Apply_entity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2017/7/4.
 */
public interface ApplyCompany_Service {
    Apply_entity getApplyEntity(String custcode);
    Apply_entity getPopupInfo();
    void saveApplyEntity(Apply_entity apply_entity);
    String ethnicCheck(String ethnic);
    List<HashMap<String,String>> getByMenleiInfo(String industrialClass);
    List<HashMap<String,String>> getByMenleiAndDaleiInfo(String industrialClass,String industrialPrimaryCategory);

    List<Apply_entity> getSearchInfoList(String PersonalOrEnterprise,
                                         String startDate,
                                         String endDate,
                                         String chineseName,
                                         String englishName,
                                         String licenseNumber,
                                         String creditCardNumber);

    Integer countSearchInfoList(String PersonalOrEnterprise,
                                String startDate,
                                String endDate,
                                String chineseName,
                                String englishName,
                                String licenseNumber,
                                String creditCardNumber);
}
