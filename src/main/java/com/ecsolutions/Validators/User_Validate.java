package com.ecsolutions.Validators;

import com.ecsolutions.entity.User_Entity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by ecs on 2017/8/18.
 */
@Component
public class User_Validate implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return User_Entity.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User_Entity entity = (User_Entity) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userid", "ValueRequired", "用户Id不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "branch", "ValueRequired", "分/支行不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwrd", "ValueRequired", "密码不能为空");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "ValueRequired", "确认密码不能为空");
        if (!(entity.getUserid().isEmpty() || entity.getBranch().isEmpty() || entity.getPasswrd().isEmpty() || entity.getConfirmPassword().isEmpty())) {
            if (!entity.getUserid().matches("^[A-Za-z0-9]+$")) {
                errors.rejectValue("userid", "Format", "请输入正确的用户Id(英文或者数字)");
            }
            if (entity.getUserid().length() > 10) {
                errors.rejectValue("userid", "Length", "请输入10位以内的字符");
            }
            if (!entity.getBranch().matches("^\\d{6}$")) {
                errors.rejectValue("branch", "Format", "请输入正确的分/支行(6位数字)");
            }
            if (entity.getBranch().length() > 6) {
                errors.rejectValue("branch", "Length", "请输入6位以内的字符");
            }
            if (!entity.getPasswrd().matches("^[A-Za-z0-9]+$")) {
                errors.rejectValue("passwrd", "Format", "请输入正确的密码(英文或者数字)");
            }
            if (entity.getPasswrd().length() > 50) {
                errors.rejectValue("passwrd", "Length", "请输入50位以内的字符");
            }
            if (!entity.getConfirmPassword().matches("^[A-Za-z0-9]+$")) {
                errors.rejectValue("confirmPassword", "Format", "请输入正确的确认密码(英文或者数字)");
            }
            if (entity.getConfirmPassword().length() > 50) {
                errors.rejectValue("confirmPassword", "Length", "请输入50位以内的字符");
            }
            if (!entity.getConfirmPassword().equals(entity.getPasswrd())) {
                errors.rejectValue("confirmPassword", "Require", "请输入与密码相同的确认密码");
            }
            if (!entity.getUsername().isEmpty()) {
                if (!entity.getUsername().matches("^[\\u4E00-\\u9FA5A-Za-z0-9]+$")) {
                    errors.rejectValue("username", "Format", "请输入正确的用户名称(中文英文或者数字)");
                }
                if (entity.getUsername().length() > 15) {
                    errors.rejectValue("username", "Length", "请输入15位以内的字符");
                }
            }
            if (!entity.getEmail().isEmpty()) {
                if (!entity.getEmail().matches("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$")) {
                    errors.rejectValue("email", "Format", "请输入正确的电子邮箱");
                }
                if (entity.getEmail().length() > 100) {
                    errors.rejectValue("email", "Length", "请输入100位以内的字符");
                }
            }
        }
    }
}
