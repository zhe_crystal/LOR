package com.ecsolutions.service;

import com.ecsolutions.entity.ApplySerial_Entity;

public interface ApplySerial_Service {
    String getCustomerNum();

    boolean insertRecord(ApplySerial_Entity entity);

    boolean updateRecord(ApplySerial_Entity entity);

    ApplySerial_Entity checkIsRecordUncompleted(String custcode);

    String getPipelineNo();

    boolean startOrUpdateWorkFlow(String userid, String loanid, String stepcode, String result, String taskid );

    String getTaskId(String BPM_NO, String userid);

    boolean insertMicroCreditAuditLogRecord(String WorkItemID, String custcod, String LoanID, String AuditNo, String AuditTime, String AuditUserBankCode, String WorkItemTitle, String AuditContext, String ActionUserName, String Result);

    String getCustcodebyBPM_NO(String BPM_NO);

    String getAuditNo();

    boolean updateAppFlag(String CustCode);

    void updateAppFlagWithBPM_NO(String CustCode, String BPM_NO);

    boolean updateStatus(String CustCode, String BPM_NO);

    boolean updateAmountWithBPM_NO(String osamt,String holdamt,String usedamt,String CustCode, String BPM_NO);


    String getCreatetime(String bpm_no); //获取工作流创建时间

    void saveWorkflowListInfo(String bpm_no,String processid,String taskname,String initiatorname,String templatename,String state,String createtime); //保存工作流列表信息

    String insertCheck(String bpm_no); //check insert

    void updateWorkflowListInfo(String bpm_no,String processid,String taskname,String initiatorname,String templatename,String state,String createtime); //更新工作流列表信息
}
