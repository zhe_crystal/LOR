package com.ecsolutions.entity;
/**
 * Created by Administrator on 2017/7/31.
 */
public class Loanpaymentinfo_Entity {
    private String custCode;
    private String lineno;
    private String loanRef;
    private String nmgflag;
    private String loantype;
    private String loancatg;
    private String zcxflag;
    private String duiFlag;
    private String purpflag;
    private String purpdesc;
    private String paytype;

    private String fenzhihang;
    private String lastname;
    private String drawccy;
    private String drawamt;
    private String drawdate;

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getLineno() {
        return lineno;
    }

    public void setLineno(String lineno) {
        this.lineno = lineno;
    }

    public String getLoanRef() {
        return loanRef;
    }

    public void setLoanRef(String loanRef) {
        this.loanRef = loanRef;
    }

    public String getNmgflag() {
        return nmgflag;
    }

    public void setNmgflag(String nmgflag) {
        this.nmgflag = nmgflag;
    }

    public String getLoantype() {
        return loantype;
    }

    public void setLoantype(String loantype) {
        this.loantype = loantype;
    }

    public String getLoancatg() {
        return loancatg;
    }

    public void setLoancatg(String loancatg) {
        this.loancatg = loancatg;
    }

    public String getZcxflag() {
        return zcxflag;
    }

    public void setZcxflag(String zcxflag) {
        this.zcxflag = zcxflag;
    }

    public String getDuiFlag() {
        return duiFlag;
    }

    public void setDuiFlag(String duiFlag) {
        this.duiFlag = duiFlag;
    }

    public String getPurpflag() {
        return purpflag;
    }

    public void setPurpflag(String purpflag) {
        this.purpflag = purpflag;
    }

    public String getPurpdesc() {
        return purpdesc;
    }

    public void setPurpdesc(String purpdesc) {
        this.purpdesc = purpdesc;
    }

    public String getPaytype() {
        return paytype;
    }

    public void setPaytype(String paytype) {
        this.paytype = paytype;
    }

    public String getFenzhihang() {
        return fenzhihang;
    }

    public void setFenzhihang(String fenzhihang) {
        this.fenzhihang = fenzhihang;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDrawccy() {
        return drawccy;
    }

    public void setDrawccy(String drawccy) {
        this.drawccy = drawccy;
    }

    public String getDrawamt() {
        return drawamt;
    }

    public void setDrawamt(String drawamt) {
        this.drawamt = drawamt;
    }

    public String getDrawdate() {
        return drawdate;
    }

    public void setDrawdate(String drawdate) {
        this.drawdate = drawdate;
    }
}