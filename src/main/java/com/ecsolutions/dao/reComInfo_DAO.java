package com.ecsolutions.dao;

import com.ecsolutions.entity.reComInfo_entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ecs on 2017/7/28.
 */
@Repository
public interface reComInfo_DAO {
    // get custcod and lastname
    @Select("SELECT custcod as custNo,lastname FROM loanapplicant WHERE ethnic = #{ethnic}")
    @ResultType(reComInfo_entity.class)
    reComInfo_entity getCustCodAndName(String ethnic);

    //check insert
    @Select("SELECT count(*) FROM loanApplicantRelationCorpInfo WHERE custcode = #{custcode} and custNo = #{custNo}")
    @ResultType(String.class)
    String checkinsert(@Param("custcode") String custcode,@Param("custNo") String custNo);

    //check ethnic
    @Select("SELECT count(*) FROM loanapplicant WHERE custcod = #{custcode} and ethnic = #{ethnic}")
    @ResultType(String.class)
    String checkethnic(@Param("ethnic") String ethnic,@Param("custcode") String custcode);

    // select by custcode
    @Select("SELECT * FROM loanApplicantRelationCorpInfo WHERE TRIM(custcode) = TRIM(#{custcode})")
    @ResultType(reComInfo_entity.class)
    List<reComInfo_entity> getReComInfo(String custcode);

    //根据关系类型ethnic删除关联方企业信息
    @Delete("delete from loanApplicantRelationCorpInfo where ethnic=#{ethnic}")
    void delete(String ethnic);

    @Insert("insert into loanApplicantRelationCorpInfo(custcode,lastName,hierarchy,custNo,ethnic,holdcustomershareratio,customerholdshareratio) values(#{custcode,jdbcType=VARCHAR},#{lastName,jdbcType=VARCHAR},#{hierarchy,jdbcType=VARCHAR},"
            +"#{custNo,jdbcType=VARCHAR},#{ethnic,jdbcType=VARCHAR},#{holdcustomershareratio,jdbcType=VARCHAR},#{customerholdshareratio,jdbcType=VARCHAR})")
    void insert(reComInfo_entity recominfo_entity);

    @Update("update loanApplicantRelationCorpInfo set custcode=#{custcode,jdbcType=VARCHAR},lastName=#{lastName,jdbcType=VARCHAR},hierarchy=#{hierarchy,jdbcType=VARCHAR},custNo=#{custNo,jdbcType=VARCHAR},ethnic=#{ethnic,jdbcType=VARCHAR},"
            +"holdcustomershareratio=#{holdcustomershareratio,jdbcType=VARCHAR},customerholdshareratio=#{customerholdshareratio,jdbcType=VARCHAR} where custcode=#{custcode} and custNo=#{custNo}")
    void update(reComInfo_entity recominfo_entity);
}
