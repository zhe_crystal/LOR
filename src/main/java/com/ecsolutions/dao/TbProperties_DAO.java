package com.ecsolutions.dao;

import com.ecsolutions.entity.TbProperties_Entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TbProperties_DAO {
    @Insert("insert into TbProperties(" +
            "propertiesid," +
            "propertiesdesc) " +
            "values (" +
            "#{PropertiesId,jdbcType=VARCHAR}," +
            "#{PropertiesDesc,jdbcType=VARCHAR}" +
            ")")
    @ResultType(Boolean.class)
    boolean saveNewProperty(TbProperties_Entity entity);

    @Select("SELECT COUNT(*) FROM TbProperties Where propertiesid=" + "#{PropertiesId,jdbcType=VARCHAR}")
    @ResultType(Integer.class)
    Integer countOne(String PropertiesId);

    @Select("SELECT * FROM TbProperties Where PropertiesId=" + "#{PropertiesId,jdbcType=VARCHAR}" + " Order By PropertiesId Asc")
    @ResultType(TbProperties_Entity.class)
    List<TbProperties_Entity> findOne(String PropertiesId);

    @Select("SELECT COUNT(*) FROM TbProperties")
    @ResultType(Integer.class)
    Integer countAll();

    @Select("SELECT * FROM TbProperties Order By PropertiesId Asc")
    @ResultType(TbProperties_Entity.class)
    List<TbProperties_Entity> findAll();

    @Update("Update TbProperties " +
            "SET " +
            "PropertiesDesc=" + "#{PropertiesDesc,jdbcType=VARCHAR}" +
            " Where PropertiesId=" + "#{PropertiesId,jdbcType=VARCHAR}"
    )
    @ResultType(Boolean.class)
    boolean update(TbProperties_Entity entity);

    @Delete("DELETE FROM TbProperties WHERE TRIM(PropertiesId)=#{PropertiesId,jdbcType=VARCHAR}")
    @ResultType(Boolean.class)
    boolean delete(String propertiesid);
}
