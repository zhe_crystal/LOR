package com.ecsolutions.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class ApplyPageEnterprise_Controller {

    @GetMapping("/ApplyPageEnterprise")
    public String getApplyPageEnterprise(Model model, @RequestParam("CustCode") String CustCode) {
        model.addAttribute("CustCode",CustCode);
        return "ClientInformation/ApplyPageEnterprise";
    }

}
