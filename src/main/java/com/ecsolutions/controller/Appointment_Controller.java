package com.ecsolutions.controller;

import com.ecsolutions.Validators.AppointmentValidate;
import com.ecsolutions.Validators.ContactValidate;
import com.ecsolutions.entity.Appointment_Entity;
import com.ecsolutions.entity.Contact_Entity;
import com.ecsolutions.service.Appointment_Service;
import com.ecsolutions.service.Contact_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/24.
 * 预约记录
 */
@SuppressWarnings("all")
@Controller
public class Appointment_Controller {

    public Appointment_Controller(){}

    @Autowired
    private Appointment_Service appointment_service;

    @InitBinder("appointment_entity")
    public void initBinder(WebDataBinder binder){
        binder.setValidator(new AppointmentValidate());
    }

    /*进入预约信息页面*/
    @GetMapping("/AppointmentList")
    public String getPledgeNewList(Model model){
        String custcode = "190101000162";
        String bpm_no = "BP1Y10000128";
        Appointment_Entity appointment_entity = new Appointment_Entity();
        appointment_entity.setCustcode(custcode);
        appointment_entity.setBpm_no(bpm_no);
        model.addAttribute("appointment_entity", appointment_entity);
        return "Appointment/appointment";
    }

    /*通过custcode和bpm_no查询信息到Table表格中*/
    @ResponseBody
    @RequestMapping(value = "/AppointmentListInfo",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public List<Appointment_Entity> getPrendaInfoList(@RequestBody Map<String, Object> map ){
        Map<String, Object> resultMap = new HashMap<>();
        String custcode = map.get("custcode").toString();
        String bpm_no = map.get("bpm_no").toString();
        List<Appointment_Entity> appointmentInfoList = appointment_service.getAppointmentInfo(custcode,bpm_no);

        return appointmentInfoList;
    }

    /*通过id删除联系记录信息*/
    @ResponseBody
    @RequestMapping(value = "/deleteAppointmentInfo",method = RequestMethod.POST)
    public Map<String,String> deleteUtilizationInfo(String id){
        Map<String, String> resultMap = new HashMap<>();
        appointment_service.deleteAppointmentInfo(Integer.parseInt(id));
        resultMap.put("message","删除成功");
        return resultMap;
    }

    /*点击新增按钮传递一个空对象到form表单*/
    @ResponseBody
    @RequestMapping(value = "/NewAppointmentInfo",method = RequestMethod.POST)
    public Contact_Entity getNewContactInfo(){
        Contact_Entity contact_entity= new Contact_Entity();
        return contact_entity;
    }

    /*保存或者更新预约记录*/
    @PostMapping("/AppointmentList")
    public String saveContactInfo(@Valid @ModelAttribute("appointment_entity") Appointment_Entity appointment_entity, BindingResult result, Model model){
        if(!result.hasErrors()){
            if(appointment_entity.getId()==null){
                String custcode = appointment_entity.getCustcode();
                String bpm_no = appointment_entity.getBpm_no();
                Integer maxid = appointment_service.getContactId(custcode,bpm_no);
                if(maxid==null){
                    appointment_entity.setId(1);
                    appointment_service.saveAppointmentInfo(appointment_entity);
                }else{
                    appointment_entity.setId(maxid+1);
                    appointment_service.saveAppointmentInfo(appointment_entity);
                }
            }else{
                appointment_service.updateAppointmentInfo(appointment_entity);
            }
            return "Appointment/appointment";
        }
        model.addAttribute("appointment_entity",appointment_entity);
        return "Appointment/appointment";
    }

}