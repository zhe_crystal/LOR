package com.ecsolutions.service;

import com.ecsolutions.dao.CompDepApproval_DAO;
import com.ecsolutions.entity.CompDepApproval_entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by ecs on 2017/8/29.
 */
@Service("CompDepAprroval_Service")
public class CompDepApproval_ServiceImpl implements CompDepApproval_Service{
    private CompDepApproval_DAO approval_dao ;
    @Autowired
    public CompDepApproval_ServiceImpl(CompDepApproval_DAO approval_dao) {
        this.approval_dao= approval_dao;
    }

    @Override
    public CompDepApproval_entity getPopupInfo(String userid,String loanid){
        CompDepApproval_entity result = new CompDepApproval_entity();
        String bankcode = approval_dao.getBankcode(loanid);
        String branch = approval_dao.getBranch(userid);
        double drawamt = Double.valueOf(approval_dao.getDrawamt(loanid));
        String drawccy = approval_dao.getDrawccy(loanid);
        result.setBankcode(bankcode);
        result.setBranch(branch);
        result.setDrawamt(drawamt);
        result.setDrawccy(drawccy);
        return result;
    }

    @Override
    public String getPuid(String loanid){
        String puId=approval_dao.getPuid(loanid);
        return puId;
    }

    @Override
    public void saveApprovalInfo(String loanid,String userid,CompDepApproval_entity approval_entity){
        approval_dao.insert(loanid,userid,approval_entity);
    }

    @Override
    public void approvalInfoSubmit(String userid,String loanid,String stepcode,String result){
        approval_dao.approvalInfoSubmit( userid, loanid, stepcode, result);
    }
}
