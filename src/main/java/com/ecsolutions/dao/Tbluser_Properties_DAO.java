package com.ecsolutions.dao;

import com.ecsolutions.entity.Tbluser_Properties_Entity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Tbluser_Properties_DAO {

    @Select("SELECT * FROM tbluser_properties Where UserId= #{UserId} order by PropertiesId")
    @ResultType(Tbluser_Properties_Entity.class)
    public List<Tbluser_Properties_Entity> getAllByUserId(String UserId);

    @Update("Update tbluser_properties Set PropertiesValue =#{PropertiesValue} Where UserId =#{UserId} And PropertiesId= #{PropertiesId}")
    @ResultType(Boolean.class)
    public boolean updateOneByAll(Tbluser_Properties_Entity entity);

    @Insert("Insert into tbluser_properties (UserId,PropertiesId,PropertiesValue) Values (#{UserId,jdbcType=VARCHAR},#{PropertiesId,jdbcType=VARCHAR},#{PropertiesValue,jdbcType=VARCHAR})")
    @ResultType(Boolean.class)
    public boolean insertUserProperty(Tbluser_Properties_Entity entity);

    @Delete("Delete from tbluser_properties Where UserId=#{UserId}")
    @ResultType(Boolean.class)
    public boolean deleteUserProperty(String UserId);

    @Delete("Delete from tbluser_properties Where PropertiesId=#{PropertiesId}")
    @ResultType(Boolean.class)
    public boolean deleteOneProperty(String PropertiesId);

}
