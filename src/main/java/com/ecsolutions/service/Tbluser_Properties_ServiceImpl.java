package com.ecsolutions.service;

import com.ecsolutions.dao.TbProperties_DAO;
import com.ecsolutions.dao.Tbluser_Properties_DAO;
import com.ecsolutions.dao.User_DAO;
import com.ecsolutions.entity.TbProperties_Entity;
import com.ecsolutions.entity.Tbluser_Properties_Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service("Tbluser_Properties_Service")
public class Tbluser_Properties_ServiceImpl implements Tbluser_Properties_Service {
    private Tbluser_Properties_DAO tbluser_properties_dao;
    private TbProperties_DAO tbProperties_dao;
    private User_DAO user_dao;
    @Autowired
    public Tbluser_Properties_ServiceImpl(Tbluser_Properties_DAO tbluser_properties_dao, TbProperties_DAO tbProperties_dao,User_DAO user_dao) {
        this.tbluser_properties_dao = tbluser_properties_dao;
        this.tbProperties_dao = tbProperties_dao;
        this.user_dao=user_dao;
    }

    @Override
    public List<Tbluser_Properties_Entity> getAllByUserId(String UserId) {
        return tbluser_properties_dao.getAllByUserId(UserId);
    }

    @Override
    public boolean updateOneByAll(Tbluser_Properties_Entity entity) {
        return tbluser_properties_dao.updateOneByAll(entity);
    }

    @Override
    public boolean insertUserProperty(Tbluser_Properties_Entity entity) {
        return tbluser_properties_dao.insertUserProperty(entity);
    }

    @Override
    public boolean initNewUserProperty(String UserId) {
        List<TbProperties_Entity> propertyList = tbProperties_dao.findAll();
        boolean state = true;
        for (TbProperties_Entity propertyEntity : propertyList) {
            Tbluser_Properties_Entity tbluser_properties_entity = new Tbluser_Properties_Entity();
            tbluser_properties_entity.setPropertiesId(propertyEntity.getPropertiesId());
            tbluser_properties_entity.setUserId(UserId);
            tbluser_properties_entity.setPropertiesValue("");
            state = tbluser_properties_dao.insertUserProperty(tbluser_properties_entity);
        }
        return state;
    }


    @Override
    public boolean deleteOneUserProperty(String UserId) {
        return tbluser_properties_dao.deleteUserProperty(UserId);
    }

    @Override
    public boolean deleteOneProperty(String PropertiesId) {
        return tbluser_properties_dao.deleteOneProperty(PropertiesId);
    }

    @Override
    public boolean insertOneProperty(String PropertiesId) {
        List<HashMap<String,String>> UserIdList=user_dao.getAllUserId();
        boolean state=true;
        for(HashMap<String,String> item:UserIdList) {
            Tbluser_Properties_Entity tbluser_properties_entity = new Tbluser_Properties_Entity();
            tbluser_properties_entity.setPropertiesId(PropertiesId);
            tbluser_properties_entity.setUserId(item.get("USERID"));
            tbluser_properties_entity.setPropertiesValue("");
            state = tbluser_properties_dao.insertUserProperty(tbluser_properties_entity);
        }
        return state;
    }
}
