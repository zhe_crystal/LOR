package com.ecsolutions.dao;

import com.ecsolutions.entity.ChangePwd_Entity;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/8/11.
 */
@Repository
public interface ChangePwd_DAO {
    @Select("SELECT PASSWRD FROM tblusers where USERID =#{id}")
    @ResultType(String.class)
    String getPassword(String id);

    @Update("UPDATE TBLUSERS SET PASSWRD =#{NewPassword} WHERE USERID=#{userID}")
    void update(ChangePwd_Entity changePwd_entity);

}
