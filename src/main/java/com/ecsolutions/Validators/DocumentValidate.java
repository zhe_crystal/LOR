package com.ecsolutions.Validators;

import com.ecsolutions.entity.document_entity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by tim on 2017/7/4.
 */

@Component
public class DocumentValidate implements Validator {

    @Override
    public void validate(Object target, Errors errors) {
        document_entity documentForm = (document_entity) target;
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Date effectiveDate = documentForm.getEffectivedate();
        Date expiryDate = documentForm.getExpirydate();
        if ((effectiveDate != null) || (expiryDate != null)){
            Integer efdate = Integer.valueOf(format.format(effectiveDate));
            Integer exdate = Integer.valueOf(format.format(expiryDate));
            if (efdate > exdate) {
                errors.rejectValue("effectivedate", "ddd", "生效日期不应晚于到期日期.");
                errors.rejectValue("expirydate", "ddd", "到期日期不应早于生效日期.");
            }
        }

    }

    @Override
    public boolean supports(Class<?> clazz) {
        return document_entity.class.isAssignableFrom(clazz);
    }

}
