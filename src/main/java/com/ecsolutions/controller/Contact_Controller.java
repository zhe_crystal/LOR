package com.ecsolutions.controller;

import com.ecsolutions.Validators.ContactValidate;
import com.ecsolutions.Validators.PrendaValidate;
import com.ecsolutions.entity.Contact_Entity;
import com.ecsolutions.entity.Prenda_Entity;
import com.ecsolutions.service.Contact_Service;
import com.ecsolutions.service.Prenda_Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/24.
 */
@SuppressWarnings("all")
@Controller
public class Contact_Controller {

    public Contact_Controller(){}

    @Autowired
    private Contact_Service contact_service;

    @InitBinder("contact_entity")
    public void initBinder(WebDataBinder binder){
        binder.setValidator(new ContactValidate());
    }

    /*进入联系记录页面*/
    @GetMapping("/ContactList")
    public String getPledgeNewList(Model model){
        String custcode = "190101000162";
        String bpm_no = "BP1Y10000128";
        Contact_Entity contact_entity = new Contact_Entity();
        contact_entity.setCustcode(custcode);
        contact_entity.setBpm_no(bpm_no);
        model.addAttribute("contact_entity", contact_entity);
        return "Contact/contact";
    }

    /*通过custcode和bpm_no查询信息到Table表格中*/
    @ResponseBody
    @RequestMapping(value = "/ContactListInfo",method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public List<Contact_Entity> getPrendaInfoList(@RequestBody Map<String, Object> map ){
        Map<String, Object> resultMap = new HashMap<>();
        String custcode = map.get("custcode").toString();
        String bpm_no = map.get("bpm_no").toString();
        List<Contact_Entity> contactInfoList = contact_service.getContactInfo(custcode,bpm_no);

        return contactInfoList;
    }

    /*通过id删除联系记录信息*/
    @ResponseBody
    @RequestMapping(value = "/deleteContactInfo",method = RequestMethod.POST)
    public Map<String,String> deleteUtilizationInfo(String id){
        Map<String, String> resultMap = new HashMap<>();
        contact_service.deleteContactInfo(Integer.parseInt(id));
        resultMap.put("message","删除成功");
        return resultMap;
    }

    /*点击新增按钮传递一个空对象到form表单*/
    @ResponseBody
    @RequestMapping(value = "/NewContactInfo",method = RequestMethod.POST)
    public Contact_Entity getNewContactInfo(){
        Contact_Entity contact_entity= new Contact_Entity();
        return contact_entity;
    }

    /*保存或者更新联系记录*/
    @PostMapping("/ContactList")
    public String saveContactInfo(@Valid @ModelAttribute("contact_entity") Contact_Entity contact_entity, BindingResult result, Model model){
        if(!result.hasErrors()){
            if(contact_entity.getId()==null){
                String custcode = contact_entity.getCustcode();
                String bpm_no = contact_entity.getBpm_no();
                Integer maxid = contact_service.getContactId(custcode,bpm_no);
                if(maxid==null){
                    contact_entity.setId(1);
                    contact_service.saveContactInfo(contact_entity);
                }else{
                    contact_entity.setId(maxid+1);
                    contact_service.saveContactInfo(contact_entity);
                }
            }else{
                contact_service.updateContactInfo(contact_entity);
            }
            return "Contact/contact";
        }
        model.addAttribute("contact_entity",contact_entity);
        return "Contact/contact";
    }

}